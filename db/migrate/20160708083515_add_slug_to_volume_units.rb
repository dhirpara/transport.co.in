class AddSlugToVolumeUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :volume_units, :slug, :string
    add_index :volume_units, :slug, unique: true
  end
end
