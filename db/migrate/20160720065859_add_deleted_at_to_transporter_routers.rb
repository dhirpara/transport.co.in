class AddDeletedAtToTransporterRouters < ActiveRecord::Migration[5.0]
  def change
    add_column :transporter_routers, :deleted_at, :datetime
    add_index :transporter_routers, :deleted_at
  end
end
