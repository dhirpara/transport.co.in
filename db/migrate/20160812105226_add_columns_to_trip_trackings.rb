class AddColumnsToTripTrackings < ActiveRecord::Migration[5.0]
  def change
  	add_column :trip_trackings, :packet_type, :string
  	add_column :trip_trackings, :tracker_id, :integer
  	add_column :trip_trackings, :information_serial, :string
  	add_column :trip_trackings, :gpsinfolength, :integer
  	add_column :trip_trackings, :gpssatellites, :integer
  	add_column :trip_trackings, :status, :string
  	add_column :trip_trackings, :gps_real_time, :boolean
  	add_column :trip_trackings, :gps_positioned, :boolean
  	add_column :trip_trackings, :course, :integer
  	add_column :trip_trackings, :mcc, :integer
  	add_column :trip_trackings, :mnc, :integer
  	add_column :trip_trackings, :lac, :integer
  	add_column :trip_trackings, :cell_id, :integer
  	add_column :trip_trackings, :lbslength, :string
  	add_column :trip_trackings, :voltage, :string
  	add_column :trip_trackings, :gsmsignal, :string
  	add_column :trip_trackings, :gas_electricity, :string
  	add_column :trip_trackings, :gps_tracking, :string
  	add_column :trip_trackings, :terminal_type, :string
  	add_column :trip_trackings, :charging, :string
  	add_column :trip_trackings, :acc, :string
  	add_column :trip_trackings, :defence, :string
  	add_column :trip_trackings, :alarm_type, :string
  	add_column :trip_trackings, :language, :string
  	add_column :trip_trackings, :dataString, :text
  	add_column :trip_trackings, :in_poi, :string
  end
end
