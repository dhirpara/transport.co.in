class AddSlugToVehicleLoadTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :vehicle_load_types, :slug, :string
    add_index :vehicle_load_types, :slug, unique: true
  end
end
