class AddColumnsToOrders < ActiveRecord::Migration[5.0]
  def change
    add_column :orders, :canceled_by, :integer
    add_column :orders, :action_as, :string
    add_column :orders, :canceled_at, :datetime
  end
end
