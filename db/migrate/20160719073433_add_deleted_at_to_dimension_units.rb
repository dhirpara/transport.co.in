class AddDeletedAtToDimensionUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :dimension_units, :deleted_at, :datetime
    add_index :dimension_units, :deleted_at
  end
end
