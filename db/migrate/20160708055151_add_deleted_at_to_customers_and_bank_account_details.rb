class AddDeletedAtToCustomersAndBankAccountDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :deleted_at, :datetime
    add_column :bank_account_details, :deleted_at, :datetime
    add_index :customers, :deleted_at
    add_index :bank_account_details, :deleted_at
  end
end
