class AddColumnsToLoads < ActiveRecord::Migration[5.0]
  def change
    add_column :loads, :step_status, :integer
    add_column :loads, :posted_on, :datetime
    add_column :loads, :load_canceled_at, :datetime
    add_column :loads, :load_canceled_by, :integer
    add_column :loads, :slug, :string
    add_index :loads, :slug, unique: true
  end
end
