class AddTypeToAdmins < ActiveRecord::Migration[5.0]
  def change
    add_column :admins, :type, :string, default: "SubAdmin"
  end
end
