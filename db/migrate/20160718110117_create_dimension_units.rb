class CreateDimensionUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :dimension_units do |t|
      t.string :name
      t.string :short_name

      t.timestamps
    end
  end
end
