class ChangeColumnsNameFromLoads < ActiveRecord::Migration[5.0]
  def change
  	rename_column :loads, :destination_address, :to_address
  	add_column :loads, :loading_address, :string
  	add_column :loads, :destination_address, :string
  end
end
