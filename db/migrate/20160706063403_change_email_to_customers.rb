class ChangeEmailToCustomers < ActiveRecord::Migration[5.0]
  def up
  	# Remove unique constraint
    remove_index :customers, :email
    add_index :customers, :email
  end

  def down
  	# Add unique constraint
    remove_index :customers, :email
    add_index :customers, :email, unique: true
  end
end
