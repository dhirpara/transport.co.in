class AddTransporterToTransporterRoutes < ActiveRecord::Migration[5.0]
  def change
    add_reference :transporter_routers, :transporter, foreign_key: true
  end
end
