class CreateFavoriteLoads < ActiveRecord::Migration[5.0]
  def change
    create_table :favorite_loads do |t|
      t.references :transporter, foreign_key: true
      t.references :load, foreign_key: true

      t.timestamps
    end
  end
end
