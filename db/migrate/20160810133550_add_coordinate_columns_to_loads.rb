class AddCoordinateColumnsToLoads < ActiveRecord::Migration[5.0]
  def change
    add_column :loads, :from_coordinate, :point
    add_column :loads, :to_coordinate, :point
  end
end
