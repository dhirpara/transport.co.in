class CreateLoads < ActiveRecord::Migration[5.0]
  def change
    create_table :loads do |t|
      t.references :customer, foreign_key: true
      t.references :load_type, foreign_key: true
      t.references :vehicle_load_type, foreign_key: true
      t.references :vehicle_type, foreign_key: true
      t.integer :vehicle_quantity
      t.hstore :weight, default: {value: nil, unit: nil}
      t.hstore :volume, default: {value: nil, unit: nil}
      t.hstore :dimension, default: {length: nil, height: nil, dept: nil, unit: nil}
      t.datetime :loading_date
      t.boolean :strict_date
      t.string :comment
      t.boolean :display_comment
      t.integer :load_status
      t.string :trip_distance
      t.string :from_address
      t.string :destination_address
      t.string :load_docket_id
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
