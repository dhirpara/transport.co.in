class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.references :load, foreign_key: true
      t.references :quote, foreign_key: true
      t.references :transporter, foreign_key: true
      t.datetime :dispatched_at
      t.datetime :completed_at
      t.integer :status

      t.timestamps
    end
  end
end
