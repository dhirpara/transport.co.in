class CreateTransporterRouters < ActiveRecord::Migration[5.0]
  def change
    create_table :transporter_routers do |t|
      t.integer :source_city_id
      t.integer :destination_city_id

      t.timestamps
    end
  end
end
