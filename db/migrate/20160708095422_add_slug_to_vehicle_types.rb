class AddSlugToVehicleTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :vehicle_types, :slug, :string
    add_index :vehicle_types, :slug, unique: true
  end
end
