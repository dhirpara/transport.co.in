class AddDeletedAtToVehicleTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :vehicle_types, :deleted_at, :datetime
    add_index :vehicle_types, :deleted_at
  end
end
