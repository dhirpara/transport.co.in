class AddOtpSecretKeyToCustomers < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :ph_no_verified, :boolean, default: false
    add_column :customers, :otp_secret_code, :string
    add_column :customers, :otp_sent_at, :datetime
    add_column :customers, :otp_verified_at, :datetime
  end
end
