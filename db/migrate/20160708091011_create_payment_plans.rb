class CreatePaymentPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_plans do |t|
      t.integer :payment_type
      t.integer :pay_within_days
      t.hstore :payment_fees
      t.boolean :display_active

      t.timestamps
    end
  end
end
