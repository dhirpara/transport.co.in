class AddSlugToWeightUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :weight_units, :slug, :string
    add_index :weight_units, :slug, unique: true
  end
end
