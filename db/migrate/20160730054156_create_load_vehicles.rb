class CreateLoadVehicles < ActiveRecord::Migration[5.0]
  def change
    create_table :load_vehicles do |t|
      t.references :load, foreign_key: true
      t.references :vehicle, foreign_key: true

      t.timestamps
    end
  end
end
