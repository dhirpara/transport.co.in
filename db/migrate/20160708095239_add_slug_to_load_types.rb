class AddSlugToLoadTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :load_types, :slug, :string
    add_index :load_types, :slug, unique: true
  end
end
