class CreateWebNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :web_notifications do |t|
      t.string :title
      t.text :body
      t.boolean :is_read, default: false

      t.timestamps
    end
  end
end
