class AddDeletedAtToLoadTypes < ActiveRecord::Migration[5.0]
  def change
    add_column :load_types, :deleted_at, :datetime
    add_index :load_types, :deleted_at
  end
end
