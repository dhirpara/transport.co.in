class AddColumnsToWebNotifications < ActiveRecord::Migration[5.0]
  def change
    add_reference :web_notifications, :notifiable, polymorphic: true, index: true
    add_column :web_notifications, :event_type, :string
		reversible do |mod|
			mod.up { change_column :web_notifications, :body, 'json USING CAST("body" AS json)' }
			mod.down { change_column :web_notifications, :body, :text }
    end
  end
end
