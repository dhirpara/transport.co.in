class CreateCities < ActiveRecord::Migration[5.0]
  def change
    create_table :cities do |t|
      t.string :name
      t.string :short_name
      t.point :coordinate

      t.timestamps
    end
  end
end
