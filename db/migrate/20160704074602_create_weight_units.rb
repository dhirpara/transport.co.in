class CreateWeightUnits < ActiveRecord::Migration[5.0]
  def change
    create_table :weight_units do |t|
      t.string :name
      t.string :short_name

      t.timestamps
    end
  end
end
