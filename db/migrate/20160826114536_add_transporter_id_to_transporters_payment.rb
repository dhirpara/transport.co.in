class AddTransporterIdToTransportersPayment < ActiveRecord::Migration[5.0]
  def change
    add_reference :transporters_payments, :transporter, foreign_key: true
  end
end
