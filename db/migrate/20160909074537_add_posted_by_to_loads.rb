class AddPostedByToLoads < ActiveRecord::Migration[5.0]
  def change
    add_column :loads, :posted_by, :string
  end
end
