class CreateUserNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :user_notifications do |t|
      t.references :web_notification, foreign_key: true
      t.references :notifiable_user, polymorphic: true, index: { name: 'index_u_ntf_on_notifiable_user_type_and_notifiable_user_id' }

      t.timestamps
    end
  end
end
