class AddDeletedAtToWeightUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :weight_units, :deleted_at, :datetime
    add_index :weight_units, :deleted_at
  end
end
