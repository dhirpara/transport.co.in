class AddSlugToBankAccountDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :bank_account_details, :slug, :string
    add_index :bank_account_details, :slug, unique: true
  end
end
