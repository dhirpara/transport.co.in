class CreateRouteboxerHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :routeboxer_histories do |t|
      t.references :transporter, foreign_key: true
      t.point :load_origin_point
      t.point :load_destination_point
      t.point :matched_origin_point
      t.point :matched_destination_point

      t.timestamps
    end
  end
end
