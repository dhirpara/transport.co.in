class AddDeletedAtToVolumeUnits < ActiveRecord::Migration[5.0]
  def change
    add_column :volume_units, :deleted_at, :datetime
    add_index :volume_units, :deleted_at
  end
end
