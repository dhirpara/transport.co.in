class AddColumnsToTransporters < ActiveRecord::Migration[5.0]
  def change
  	add_column :transporters, :name, :string, after: :id
    add_column :transporters, :ph_no, :string, after: :email
    add_column :transporters, :company_name, :string, after: :password
    add_column :transporters, :address_detail, :string, after: :company_name
    add_column :transporters, :rating_score, :decimal, after: :address_detail
    add_column :transporters, :is_blocked, :boolean
    add_column :transporters, :approved, :boolean, :default => false, :null => false
    add_column :transporters, :slug, :string
    add_column :transporters, :deleted_at, :datetime
    add_column :transporters, :ph_no_verified, :boolean, default: false
    add_column :transporters, :otp_secret_code, :string
    add_column :transporters, :otp_sent_at, :datetime
    add_column :transporters, :otp_verified_at, :datetime
    add_index :transporters, :deleted_at
    add_index :transporters, :ph_no, unique: true
    add_index  :transporters, :approved
    add_index :transporters, :slug, unique: true
  end
end
