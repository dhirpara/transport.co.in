class CreateCustomersPayments < ActiveRecord::Migration[5.0]
  def change
    create_table :customers_payments do |t|
    	t.references :quote, foreign_key: true
    	t.references :payment_plan, foreign_key: true
      t.decimal :amount
      t.datetime :paying_date
      t.string :pay_via
      t.string :ref_no
      t.integer :status
      t.timestamps
    end
  end
end
