class CreateBankAccountDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :bank_account_details do |t|
      t.string :account_holder_name
      t.string :account_number
      t.string :bank_name
      t.string :ifsc_code
      t.string :branch_name
      t.references :accountable, polymorphic: true, index: { name: 'index_bank_acc_details_on_acc_type_and_acc_id' }

      t.timestamps
    end
  end
end
