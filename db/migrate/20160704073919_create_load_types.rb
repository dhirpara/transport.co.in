class CreateLoadTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :load_types do |t|
      t.string :name
      t.boolean :display_active, defaul: true

      t.timestamps
    end
  end
end
