class CreateTripTrackings < ActiveRecord::Migration[5.0]
  def change
    create_table :trip_trackings do |t|
      t.references :trip, foreign_key: true
      t.string :location
      t.point :coordinate
      t.datetime :location_time_stamp
      t.decimal :speed

      t.timestamps
    end
  end
end
