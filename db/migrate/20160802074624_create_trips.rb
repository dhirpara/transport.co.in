class CreateTrips < ActiveRecord::Migration[5.0]
  def change
    create_table :trips do |t|
      t.references :load
      t.references :vehicle
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
