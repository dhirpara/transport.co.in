class AddSlugToDimension < ActiveRecord::Migration[5.0]
  def change
    add_column :dimension_units, :slug, :string
    add_index :dimension_units, :slug, unique: true
  end
end
