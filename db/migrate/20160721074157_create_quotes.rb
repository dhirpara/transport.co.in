class CreateQuotes < ActiveRecord::Migration[5.0]
  def change
    create_table :quotes do |t|
      t.references :transporter, foreign_key: true
      t.references :load, foreign_key: true
      t.integer :offered_vehicle_load_type_id, index: true
      t.integer :offered_vehicle_type_id, index: true
      t.integer :offered_vehicle_quantity
      t.datetime :offered_date
      t.decimal :offered_price
      t.integer :status
      t.datetime :quotes_on
      t.integer :quotes_by
      t.datetime :deleted_at
      t.string :slug

      t.timestamps
    end
    add_index :quotes, :deleted_at
    add_index :quotes, :slug, unique: true
    add_foreign_key :quotes, :vehicle_load_types, column: :offered_vehicle_load_type_id
    add_foreign_key :quotes, :vehicle_types, column: :offered_vehicle_type_id
  end
end
