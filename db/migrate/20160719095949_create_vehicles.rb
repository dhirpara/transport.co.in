class CreateVehicles < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicles do |t|
      t.references :transporter, foreign_key: true
      t.references :vehicle_type, foreign_key: true
      t.string :number_plate
      t.integer :vehicle_status
      t.string :chassis_number
      t.integer :fitness

      t.timestamps
    end
  end
end
