class AddDefaultValueToDisplayActive < ActiveRecord::Migration[5.0]
  def change
  	change_column :load_types, :display_active, :boolean, :default => true
  end
end
