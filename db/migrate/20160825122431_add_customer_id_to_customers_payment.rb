class AddCustomerIdToCustomersPayment < ActiveRecord::Migration[5.0]
  def change
    add_reference :customers_payments, :customer, foreign_key: true
  end
end
