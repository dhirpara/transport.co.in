class AddColumnsToCustomers < ActiveRecord::Migration[5.0]
  def change
    add_column :customers, :name, :string, after: :id
    add_column :customers, :ph_no, :string, after: :email
    add_column :customers, :company_name, :string, after: :password
    add_column :customers, :address_detail, :string, after: :company_name
    add_column :customers, :rating_score, :decimal, after: :address_detail
    add_column :customers, :is_blocked, :boolean
    add_column :customers, :approved, :boolean, :default => false, :null => false
    add_column :customers, :slug, :string
    add_index :customers, :ph_no, unique: true
    add_index  :customers, :approved
    add_index :customers, :slug, unique: true
  end
end
