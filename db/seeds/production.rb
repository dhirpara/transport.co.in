def create_admin
	yield
	Admin.create!(email: APP_CONFIG["SUPER_ADMIN_EMAIL"], password: APP_CONFIG["SUPER_ADMIN_PASSWORD"], type: "Admin")
end

def load_type
	yield
	LoadType.create!([
		{name: "Container cargo"},
		{name: "Liquid bulk"},
		{name: "Dry bulk"},
		{name: "Breakbulk"},
		{name: "Milk"}
	])
end

def weight_unit
	yield
	WeightUnit.create!([
		{name: "tonne", short_name: "t"},
		{name: "kilogram", short_name: "kg"},
		{name: "hectogram", short_name: "hg"},
		{name: "gram", short_name: "g"},
		{name: "decigram", short_name: "dg"},
		{name: "centigram", short_name: "cg"},
		{name: "milligram", short_name: "mg"},
		{name: "microgram", short_name: "µg"},
		{name: "carat", short_name: "carat"},
		{name: "grain", short_name: "grain"}
	])
end

def volume_unit
	yield
	VolumeUnit.create!([
		{name: "cubic meter", short_name: "m³"},
		{name: "cubic decimeter", short_name: "dm³"},
		{name: "cubic centimeter", short_name: "cm³"},
		{name: "liter", short_name: "l"},
		{name: "deciliter", short_name: "dl"},
		{name: "centiliter", short_name: "cl"},
		{name: "milliliter", short_name: "ml"},
		{name: "cubic inch", short_name: "in³"},
		{name: "cubic foot", short_name: "ft³"},
		{name: "petroleum barrel", short_name: "bbl"}
	])
end

def dimention_unit
	yield
	DimensionUnit.create!([
		{name: "cubic meter", short_name: "m³"},
		{name: "cubic decimeter", short_name: "dm³"},
		{name: "cubic centimeter", short_name: "cm³"},
		{name: "liter", short_name: "l"},
		{name: "deciliter", short_name: "dl"},
		{name: "centiliter", short_name: "cl"},
		{name: "milliliter", short_name: "ml"},
		{name: "cubic inch", short_name: "in³"},
		{name: "cubic foot", short_name: "ft³"},
		{name: "petroleum barrel", short_name: "bbl"}
	])
end

def vehicle_load_type
	yield
	VehicleLoadType.create!([
		{name: "Full vehicle load"},
		{name: "Partial vehicle load"}
	])
end

def vehicle_type
	yield
	VehicleType.create!([
		{name: "Don't know"},
		{name: "4 wheeels/10ft/1 ton"},
		{name: "6 wheeels/12ft/5 ton"},
		{name: "18 wheeels/19ft/25 ton"}
	])
end

def payment_plan
	yield
	PaymentPlan.create!([
		{payment_type: 1, pay_within_days: 0, payment_fees: {'Convenience Fee': '3'}, display_active: true},
		{payment_type: 1, pay_within_days: 15, payment_fees: {'Convenience Fee': '3', 'Late fee': '2' }, display_active: true},
		{payment_type: 1, pay_within_days: 30, payment_fees: {'Convenience Fee': '3', 'Late fee': '4' }, display_active: true},
		{payment_type: 2, pay_within_days: 0, payment_fees: {'Convenience Fee': '3'}, display_active: true},
		{payment_type: 2, pay_within_days: 15, payment_fees: {'Convenience Fee': '3', 'Late fee': '2' }, display_active: true},
		{payment_type: 2, pay_within_days: 30, payment_fees: {'Convenience Fee': '3', 'Late fee': '4' }, display_active: true}
	])
end

def create_transporter
	yield
	Transporter.create!([
		{email: "trans1@gmail.com", password: "hello123", name: "Trans1", ph_no: "1234567890", company_name: "Trans One pvt. ltd.", approved: true, ph_no_verified: true, otp_sent_at: Time.zone.now, otp_verified_at: Time.zone.now },
		{email: "trans2@gmail.com", password: "hello123", name: "Trans2", ph_no: "0987654321", company_name: "Trans Two pvt. ltd.", approved: true, ph_no_verified: true, otp_sent_at: Time.zone.now, otp_verified_at: Time.zone.now },
		{email: "trans3@gmail.com", password: "hello123", name: "Trans3", ph_no: "852456", company_name: "Trans Three pvt. ltd.", approved: true, ph_no_verified: true, otp_sent_at: Time.zone.now, otp_verified_at: Time.zone.now }
	])
end

def create_customer
	yield
	Customer.create!([
		{email: "cust1@gmail.com", password: "hello123", name: "Cust1", ph_no: "1234567890", company_name: "Cust One pvt. ltd.", approved: true, ph_no_verified: true, otp_sent_at: Time.zone.now, otp_verified_at: Time.zone.now },
		{email: "cust2@gmail.com", password: "hello123", name: "Cust2", ph_no: "0987654321", company_name: "Cust Two pvt. ltd.", approved: true, ph_no_verified: true, otp_sent_at: Time.zone.now, otp_verified_at: Time.zone.now },
		{email: "cust3@gmail.com", password: "hello123", name: "Cust3", ph_no: "852456", company_name: "Cust Three pvt. ltd.", approved: true, ph_no_verified: true, otp_sent_at: Time.zone.now, otp_verified_at: Time.zone.now }
	])
end

def vehicle
	yield
	get_all_transporters
	Vehicle.create!([
		{transporter_id: @transporters[0].id, vehicle_type_id: 1, number_plate: "TN 58 N 4006", vehicle_status: 2, chassis_number: "5GZCZ43D13S8127JH8", fitness: 1},
		{transporter_id: @transporters[0].id, vehicle_type_id: 2, number_plate: "KL 15 3431", vehicle_status: 2, chassis_number: "565IUGHCZ43D1H12715", fitness: 1},
		{transporter_id: @transporters[0].id, vehicle_type_id: 3, number_plate: "DL 11 CAA 1111", vehicle_status: 1, chassis_number: "KJ98CZ433453S812715", fitness: 1},
		{transporter_id: @transporters[1].id, vehicle_type_id: 1, number_plate: "MI 60 AG 3333", vehicle_status: 2, chassis_number: "HNIN9Z43D13S812JK8", fitness: 1},
		{transporter_id: @transporters[1].id, vehicle_type_id: 4, number_plate: "MH 14 BT 2433", vehicle_status: 1, chassis_number: "L943D1KH812LK89", fitness: 1},
		{transporter_id: @transporters[1].id, vehicle_type_id: 2, number_plate: "GA 03 X 0109", vehicle_status: 2, chassis_number: "ZLJ98983S8127IJH8", fitness: 1},
	])
end

def city
	yield
	City.create!([
		{name: "Mumbai, Maharashtra, India", short_name: "MBI", coordinate: [19.075984, 72.877656]},
		{name: "Delhi, India", short_name: "DLH", coordinate: [28.704059, 77.102490]},
		{name: "Jaipur, Rajasthan, India", short_name: "JIP", coordinate: [26.912434, 75.787271]},
		{name: "Hyderabad, Telangana, India", short_name: "HDB", coordinate: [17.385044, 78.486671]},
		{name: "Nagpur, Maharashtra, India", short_name: "NGP", coordinate: [21.145800, 79.088155]},
		{name: "Rajasthan, India", short_name: "RJT", coordinate: [27.023804, 74.217933]},
		{name: "Vapi, Gujarat, India", short_name: "VP", coordinate: [20.3893155,72.91062020000004]}
	])
end

def transporter_routes
	yield
	get_all_transporters
	get_all_cities
	TransporterRouter.create!([
		{source_city_id: @cities[0].id, destination_city_id: @cities[1].id, transporter_id: @transporters[0].id},
		{source_city_id: @cities[2].id, destination_city_id: @cities[3].id, transporter_id: @transporters[0].id},
		{source_city_id: @cities[1].id, destination_city_id: @cities[3].id, transporter_id: @transporters[0].id},
		{source_city_id: @cities[1].id, destination_city_id: @cities[5].id, transporter_id: @transporters[0].id},
		{source_city_id: @cities[4].id, destination_city_id: @cities[3].id, transporter_id: @transporters[0].id},
		{source_city_id: @cities[3].id, destination_city_id: @cities[0].id, transporter_id: @transporters[1].id},
		{source_city_id: @cities[0].id, destination_city_id: @cities[2].id, transporter_id: @transporters[1].id},
		{source_city_id: @cities[0].id, destination_city_id: @cities[4].id, transporter_id: @transporters[1].id},
		{source_city_id: @cities[5].id, destination_city_id: @cities[6].id, transporter_id: @transporters[1].id}
	])
end

def get_all_transporters
	@transporters = Transporter.all.to_a
end

def get_all_cities
	@cities = City.all.to_a
end



create_admin{ puts "-> creating admin..." }
create_transporter{ puts "-> creating transporters..." }
create_customer{ puts "-> creating transporters..." }
load_type{ puts "-> setting load_type..."}
weight_unit{ puts "-> setting weight_unit..."}
volume_unit{ puts "-> setting volume_unit..."}
dimention_unit{ puts "-> setting dimention_unit..."}
vehicle_load_type{ puts "-> setting vehicle_load_type..." }
vehicle_type{ puts "-> setting vehicle_type..." }
payment_plan{ puts "-> setting Payment plan..."}
vehicle{ puts "-> add vehicles..." }
city{ puts "-> add cities..." }
transporter_routes{ puts "-> add transporter routes..." }