# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160909130147) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "",         null: false
    t.string   "encrypted_password",     default: "",         null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,          null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "type",                   default: "SubAdmin"
    t.index ["email"], name: "index_admins_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree
  end

  create_table "bank_account_details", force: :cascade do |t|
    t.string   "account_holder_name"
    t.string   "account_number"
    t.string   "bank_name"
    t.string   "ifsc_code"
    t.string   "branch_name"
    t.string   "accountable_type"
    t.integer  "accountable_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.index ["accountable_type", "accountable_id"], name: "index_bank_acc_details_on_acc_type_and_acc_id", using: :btree
    t.index ["deleted_at"], name: "index_bank_account_details_on_deleted_at", using: :btree
    t.index ["slug"], name: "index_bank_account_details_on_slug", unique: true, using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.string   "short_name"
    t.point    "coordinate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.index ["slug"], name: "index_cities_on_slug", unique: true, using: :btree
  end

  create_table "customers", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "name"
    t.string   "ph_no"
    t.string   "company_name"
    t.string   "address_detail"
    t.decimal  "rating_score"
    t.boolean  "is_blocked"
    t.boolean  "approved",               default: false, null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.boolean  "ph_no_verified",         default: false
    t.string   "otp_secret_code"
    t.datetime "otp_sent_at"
    t.datetime "otp_verified_at"
    t.index ["approved"], name: "index_customers_on_approved", using: :btree
    t.index ["deleted_at"], name: "index_customers_on_deleted_at", using: :btree
    t.index ["email"], name: "index_customers_on_email", using: :btree
    t.index ["ph_no"], name: "index_customers_on_ph_no", using: :btree
    t.index ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true, using: :btree
    t.index ["slug"], name: "index_customers_on_slug", unique: true, using: :btree
  end

  create_table "customers_payments", force: :cascade do |t|
    t.integer  "quote_id"
    t.integer  "payment_plan_id"
    t.decimal  "amount"
    t.datetime "paying_date"
    t.string   "pay_via"
    t.string   "ref_no"
    t.integer  "status"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "customer_id"
    t.index ["customer_id"], name: "index_customers_payments_on_customer_id", using: :btree
    t.index ["payment_plan_id"], name: "index_customers_payments_on_payment_plan_id", using: :btree
    t.index ["quote_id"], name: "index_customers_payments_on_quote_id", using: :btree
  end

  create_table "dimension_units", force: :cascade do |t|
    t.string   "name"
    t.string   "short_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_dimension_units_on_deleted_at", using: :btree
    t.index ["slug"], name: "index_dimension_units_on_slug", unique: true, using: :btree
  end

  create_table "favorite_loads", force: :cascade do |t|
    t.integer  "transporter_id"
    t.integer  "load_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["load_id"], name: "index_favorite_loads_on_load_id", using: :btree
    t.index ["transporter_id"], name: "index_favorite_loads_on_transporter_id", using: :btree
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree
  end

  create_table "inquiries", force: :cascade do |t|
    t.string   "email"
    t.string   "phone_number"
    t.text     "message"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "load_types", force: :cascade do |t|
    t.string   "name"
    t.boolean  "display_active", default: true
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_load_types_on_deleted_at", using: :btree
    t.index ["slug"], name: "index_load_types_on_slug", unique: true, using: :btree
  end

  create_table "load_vehicles", force: :cascade do |t|
    t.integer  "load_id"
    t.integer  "vehicle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["load_id"], name: "index_load_vehicles_on_load_id", using: :btree
    t.index ["vehicle_id"], name: "index_load_vehicles_on_vehicle_id", using: :btree
  end

  create_table "loads", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "load_type_id"
    t.integer  "vehicle_load_type_id"
    t.integer  "vehicle_type_id"
    t.integer  "vehicle_quantity"
    t.hstore   "weight",               default: {"unit"=>nil, "value"=>nil}
    t.hstore   "volume",               default: {"unit"=>nil, "value"=>nil}
    t.hstore   "dimension",            default: {"dept"=>nil, "unit"=>nil, "height"=>nil, "length"=>nil}
    t.datetime "loading_date"
    t.boolean  "strict_date"
    t.string   "comment"
    t.boolean  "display_comment"
    t.integer  "load_status"
    t.string   "trip_distance"
    t.string   "from_address"
    t.string   "to_address"
    t.string   "load_docket_id"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                                              null: false
    t.datetime "updated_at",                                                                              null: false
    t.integer  "step_status"
    t.datetime "posted_on"
    t.datetime "load_canceled_at"
    t.integer  "load_canceled_by"
    t.string   "slug"
    t.string   "loading_address"
    t.string   "destination_address"
    t.point    "from_coordinate"
    t.point    "to_coordinate"
    t.string   "posted_by"
    t.index ["customer_id"], name: "index_loads_on_customer_id", using: :btree
    t.index ["deleted_at"], name: "index_loads_on_deleted_at", using: :btree
    t.index ["load_type_id"], name: "index_loads_on_load_type_id", using: :btree
    t.index ["slug"], name: "index_loads_on_slug", unique: true, using: :btree
    t.index ["vehicle_load_type_id"], name: "index_loads_on_vehicle_load_type_id", using: :btree
    t.index ["vehicle_type_id"], name: "index_loads_on_vehicle_type_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "load_id"
    t.integer  "quote_id"
    t.integer  "transporter_id"
    t.datetime "dispatched_at"
    t.datetime "completed_at"
    t.integer  "status"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "canceled_by"
    t.string   "action_as"
    t.datetime "canceled_at"
    t.index ["load_id"], name: "index_orders_on_load_id", using: :btree
    t.index ["quote_id"], name: "index_orders_on_quote_id", using: :btree
    t.index ["transporter_id"], name: "index_orders_on_transporter_id", using: :btree
  end

  create_table "payment_plans", force: :cascade do |t|
    t.integer  "payment_type"
    t.integer  "pay_within_days"
    t.hstore   "payment_fees"
    t.boolean  "display_active"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "quotes", force: :cascade do |t|
    t.integer  "transporter_id"
    t.integer  "load_id"
    t.integer  "offered_vehicle_load_type_id"
    t.integer  "offered_vehicle_type_id"
    t.integer  "offered_vehicle_quantity"
    t.datetime "offered_date"
    t.decimal  "offered_price"
    t.integer  "status"
    t.datetime "quotes_on"
    t.integer  "quotes_by"
    t.datetime "deleted_at"
    t.string   "slug"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["deleted_at"], name: "index_quotes_on_deleted_at", using: :btree
    t.index ["load_id"], name: "index_quotes_on_load_id", using: :btree
    t.index ["offered_vehicle_load_type_id"], name: "index_quotes_on_offered_vehicle_load_type_id", using: :btree
    t.index ["offered_vehicle_type_id"], name: "index_quotes_on_offered_vehicle_type_id", using: :btree
    t.index ["slug"], name: "index_quotes_on_slug", unique: true, using: :btree
    t.index ["transporter_id"], name: "index_quotes_on_transporter_id", using: :btree
  end

  create_table "routeboxer_histories", force: :cascade do |t|
    t.integer  "transporter_id"
    t.point    "load_origin_point"
    t.point    "load_destination_point"
    t.point    "matched_origin_point"
    t.point    "matched_destination_point"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["transporter_id"], name: "index_routeboxer_histories_on_transporter_id", using: :btree
  end

  create_table "transporter_routers", force: :cascade do |t|
    t.integer  "source_city_id"
    t.integer  "destination_city_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "transporter_id"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_transporter_routers_on_deleted_at", using: :btree
    t.index ["transporter_id"], name: "index_transporter_routers_on_transporter_id", using: :btree
  end

  create_table "transporters", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "name"
    t.string   "ph_no"
    t.string   "company_name"
    t.string   "address_detail"
    t.decimal  "rating_score"
    t.boolean  "is_blocked"
    t.boolean  "approved",               default: false, null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.boolean  "ph_no_verified",         default: false
    t.string   "otp_secret_code"
    t.datetime "otp_sent_at"
    t.datetime "otp_verified_at"
    t.index ["approved"], name: "index_transporters_on_approved", using: :btree
    t.index ["deleted_at"], name: "index_transporters_on_deleted_at", using: :btree
    t.index ["email"], name: "index_transporters_on_email", using: :btree
    t.index ["ph_no"], name: "index_transporters_on_ph_no", using: :btree
    t.index ["reset_password_token"], name: "index_transporters_on_reset_password_token", unique: true, using: :btree
    t.index ["slug"], name: "index_transporters_on_slug", unique: true, using: :btree
  end

  create_table "transporters_payments", force: :cascade do |t|
    t.integer  "quote_id"
    t.decimal  "amount"
    t.datetime "paying_date"
    t.string   "pay_via"
    t.string   "ref_no"
    t.integer  "status"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "transporter_id"
    t.index ["quote_id"], name: "index_transporters_payments_on_quote_id", using: :btree
    t.index ["transporter_id"], name: "index_transporters_payments_on_transporter_id", using: :btree
  end

  create_table "trip_trackings", force: :cascade do |t|
    t.integer  "trip_id"
    t.string   "location"
    t.point    "coordinate"
    t.datetime "location_time_stamp"
    t.decimal  "speed"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "packet_type"
    t.integer  "tracker_id"
    t.string   "information_serial"
    t.integer  "gpsinfolength"
    t.integer  "gpssatellites"
    t.string   "status"
    t.boolean  "gps_real_time"
    t.boolean  "gps_positioned"
    t.integer  "course"
    t.integer  "mcc"
    t.integer  "mnc"
    t.integer  "lac"
    t.integer  "cell_id"
    t.string   "lbslength"
    t.string   "voltage"
    t.string   "gsmsignal"
    t.string   "gas_electricity"
    t.string   "gps_tracking"
    t.string   "terminal_type"
    t.string   "charging"
    t.string   "acc"
    t.string   "defence"
    t.string   "alarm_type"
    t.string   "language"
    t.text     "dataString"
    t.string   "in_poi"
    t.index ["trip_id"], name: "index_trip_trackings_on_trip_id", using: :btree
  end

  create_table "trips", force: :cascade do |t|
    t.integer  "load_id"
    t.integer  "vehicle_id"
    t.integer  "status",     default: 0
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["load_id"], name: "index_trips_on_load_id", using: :btree
    t.index ["vehicle_id"], name: "index_trips_on_vehicle_id", using: :btree
  end

  create_table "user_notifications", force: :cascade do |t|
    t.integer  "web_notification_id"
    t.string   "notifiable_user_type"
    t.integer  "notifiable_user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["notifiable_user_type", "notifiable_user_id"], name: "index_u_ntf_on_notifiable_user_type_and_notifiable_user_id", using: :btree
    t.index ["web_notification_id"], name: "index_user_notifications_on_web_notification_id", using: :btree
  end

  create_table "vehicle_load_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_vehicle_load_types_on_deleted_at", using: :btree
    t.index ["slug"], name: "index_vehicle_load_types_on_slug", unique: true, using: :btree
  end

  create_table "vehicle_types", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_vehicle_types_on_deleted_at", using: :btree
    t.index ["slug"], name: "index_vehicle_types_on_slug", unique: true, using: :btree
  end

  create_table "vehicles", force: :cascade do |t|
    t.integer  "transporter_id"
    t.integer  "vehicle_type_id"
    t.string   "number_plate"
    t.integer  "vehicle_status"
    t.string   "chassis_number"
    t.integer  "fitness"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["transporter_id"], name: "index_vehicles_on_transporter_id", using: :btree
    t.index ["vehicle_type_id"], name: "index_vehicles_on_vehicle_type_id", using: :btree
  end

  create_table "volume_units", force: :cascade do |t|
    t.string   "name"
    t.string   "short_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_volume_units_on_deleted_at", using: :btree
    t.index ["slug"], name: "index_volume_units_on_slug", unique: true, using: :btree
  end

  create_table "web_notifications", force: :cascade do |t|
    t.string   "title"
    t.json     "body"
    t.boolean  "is_read",         default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "notifiable_type"
    t.integer  "notifiable_id"
    t.string   "event_type"
    t.index ["notifiable_type", "notifiable_id"], name: "index_web_notifications_on_notifiable_type_and_notifiable_id", using: :btree
  end

  create_table "weight_units", force: :cascade do |t|
    t.string   "name"
    t.string   "short_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_weight_units_on_deleted_at", using: :btree
    t.index ["slug"], name: "index_weight_units_on_slug", unique: true, using: :btree
  end

  add_foreign_key "customers_payments", "customers"
  add_foreign_key "customers_payments", "payment_plans"
  add_foreign_key "customers_payments", "quotes"
  add_foreign_key "favorite_loads", "loads"
  add_foreign_key "favorite_loads", "transporters"
  add_foreign_key "load_vehicles", "loads"
  add_foreign_key "load_vehicles", "vehicles"
  add_foreign_key "loads", "customers"
  add_foreign_key "loads", "load_types"
  add_foreign_key "loads", "vehicle_load_types"
  add_foreign_key "loads", "vehicle_types"
  add_foreign_key "orders", "loads"
  add_foreign_key "orders", "quotes"
  add_foreign_key "orders", "transporters"
  add_foreign_key "quotes", "loads"
  add_foreign_key "quotes", "transporters"
  add_foreign_key "quotes", "vehicle_load_types", column: "offered_vehicle_load_type_id"
  add_foreign_key "quotes", "vehicle_types", column: "offered_vehicle_type_id"
  add_foreign_key "routeboxer_histories", "transporters"
  add_foreign_key "transporter_routers", "transporters"
  add_foreign_key "transporters_payments", "quotes"
  add_foreign_key "transporters_payments", "transporters"
  add_foreign_key "trip_trackings", "trips"
  add_foreign_key "user_notifications", "web_notifications"
  add_foreign_key "vehicles", "transporters"
  add_foreign_key "vehicles", "vehicle_types"
end
