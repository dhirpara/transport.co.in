Rails.application.routes.draw do
  resources :inquiries
  # mount ActionCable.server => '/cable'
  get 'transporters/dashboard'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :admins
  devise_scope :admin do
    authenticated :admin do
      root 'admins#dashboard', as: :authenticated_admin_root
    end
  end

  namespace :admins do
    resources :customers do
      member do
        post 'approve_customer'
        get 'show_customer', as: :show_customer_in_load
      end
    end
    resources :transporters do
      member do
        post 'approve_transporter'
      end

      post 'create_vehicle_from_order' => "vehicles#create_vehicle_from_order"
    end
    resources :quotes do
      member do
        get 'edit_quote'
        patch 'update_quote'
        put 'cancel_quote'
      end
    end
    resources :loads do
      member do
        put 'cancel'
        put 'approve'
        get 'see_quotes'
      end
      resources :quotes, only: [] do
        member do
          put 'accept_quote'
        end
      end
      resources :trackers, only: [:index, :new, :create, :show], shallow: true
      # get 'new_from_order' => "trackers#new_from_order"
    end
    resources :payment_plans
    resources :weight_units
    resources :dimension_units
    resources :volume_units
    resources :vehicle_load_types
    resources :vehicle_types
    resources :load_types
    resources :cities
    resources :orders do
      member do
        put 'cancel'
        post 'create_vehicle'
        get 'assign_vehicle_form'
        post 'assign_vehicle'
        get 'get_address_form'
        patch 'set_address'
        patch 'dispatch_order'
        patch 'marke_as_reached'
      end

      collection do
        get 'active_orders'
        get 'processed_orders'
      end

      resources 'payments', only: [] do
        member do
          get 'customer_payment_form'
          patch 'receive_customer_payment'
        end
      end
      get 'transporter_payment_form' => 'payments#transporter_payment_form'
      post 'pay_transporter_payment' => 'payments#pay_transporter_payment'
    end
    # get 'active_orders' => 'orders#active_orders'
    # get 'active_orders' => 'orders#active_orders'
    get 'new_order_track_detail/trip/:id' => 'orders#new_order_track_detail', as: :new_order_track_detail
    post 'create_order_track_detail/trip/:id' => 'orders#create_order_track_detail', as: :create_order_track_detail
    get 'track_order/:id' => 'orders#track_order', as: :track_order

  end
  resources :admins, as: :sub_admins, :path => 'sub_admins'
  get 'admins/:id/web_notification_show', to: "admins#web_notification_show", as: :web_notification_show
  get 'get_notifyable_transporter/:load_id', to: "admins#get_notifyable_transporter", as: :get_notifyable_transporter
  get 'all_transporter_routes', to: "admins#all_transporter_routes"
  post 'send_notification_for_new_load', to: "admins#send_notification_for_new_load"
  # resources :admin  do
  #   member do
  #     post 'approve_customer/:customer_id' => 'admin#approve_customer', as: :approve_customer
  #   end
  # end

  namespace :customers do
    namespace :loads do
      resources :builds, only: [:create, :show, :update]
    end
    resources :loads do
      member do
        put 'cancel'
        put "accept_quote/:quote_id", :as => "accept_quote", :action => "accept_quote"
        get 'order'
        get 'confirmed_load'
        get 'in_transit'
        get 'track'
        get 'track_details'
        get 'quotes'
        get 'cancel_load'
        put 'dispatch_order'
        put 'marke_as_reached'
        get 'load_detail'
        get 'get_address_form'
        patch 'set_address'
        get 'track_shipment'
        get 'find_address'
      end
    end
    resources :payments, path: "quote/:quote_id/payments"
    get "payments/get_all_plans", to: "payments#get_all_plans"
    get "payments/get_form", to: "payments#get_form"
   # get "quote/:quote_id/payments/new", to: "payments#new", as: :quote_payment
  end
  devise_for :customers, controllers: {
        registrations: 'customers/registrations',
        sessions: 'customers/sessions'
  }
  devise_scope :customer do
    authenticated :customer do
      root 'customers/loads#index', as: :authenticated_customer_root
    end
  end


  resources :customers, only: :show do
    member do
      get 'change_password'
      put 'update_password'
      get 'send_otp'
      get 'otp_authentication'
      post 'verify_phone'
    end

    collection do
      get 'web_notifications'
    end
    resources :bank_account_details
  end

  devise_for :transporters, controllers: {
        registrations: 'transporters/registrations',
        sessions: 'transporters/sessions'
  }
  devise_scope :transporter do
    authenticated :transporter do
      root 'transporters/quotes#index', as: :authenticated_transporter_root
    end
  end

  namespace :transporters do
    resources :vehicles
    resources :transporter_routers, except: :new
    resources :quotes do
      member do
        get 'draft'
      end
    end
    post 'load/:id/add_to_favorite', as: :add_to_favorite, to: "quotes#add_to_favorite"

    resources :orders, only: [:show, :index] do
      member do
        get 'vehicle_list'
        post 'assign_vehicle'
        delete 'discard_vehicle'
        # put 'dispatch_order'
        put 'cancel_order'
        get 'track'
        get 'track_details'
        get 'payment_info'
        get 'cancel'
        get 'order_details'
        get 'order_statistic'
      end

      # get '/dispatch' => :ready_to_dispatch_load, as: :ready_to_dispatch_load
      # get '/list_vehicle' => :list_vehicle, as: :list_vehicle
      # post '/assign_vehicle' => :assign_vehicle, as: :assign_vehicle
      # delete '/discard_vehicle' => :discard_vehicle_assignmnet, as: :discard_vehicle_assignmnet
      # post '/dispatch_load' => :dispatch_load, as: :dispatch_load
      # get '/in_transit' => :in_transit, as: :in_transit
      # get '/cancel_order' => :cancel_order, as: :cancel_order
    end

    # controller :tracking do
    #   scope 'load/:id/' do
    #     get 'load_tracking' => :load_tracking, as: :load_tracking
    #     get 'track_details' => :track_details, as: :track_details
    #   end
    # end

    # resources :orders, only: [] do
    #   member do
    #     get 'payment_info'
    #   end
    # end
  end

  resources :transporters, only: :show do
    member do
      get 'change_password'
      put 'update_password'
      get 'send_otp'
      get 'otp_authentication'
      post 'verify_phone'
    end
    collection do
      get 'web_notifications'
    end
    resources :bank_account_details
  end

  post 'authenticate_customer_for_account_existence' => 'landing#authenticate_customer_for_account_existence'
  root 'landing#transport_landing'
  # Default path if no routes match
  # match '*path' => redirect('/'), via: :get
end
