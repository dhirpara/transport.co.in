# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.

%w( landing
		admin
		admin_dashboard
		admins/cities
		admins/dimention_units
		admins/load_types
		admins/quotes
		admins/payment_plans
		admins/transporters
		admins/vehicle_load_types
		admins/vehicle_types
		admins/volume_units
		customers admins/loads
		admins/trackers admins/orders
		customers/sessions
		customers/payments
		customers/loads
		customers/loads/builds
		transporters
		transporters/materialize
		transporters/style
		transporters/vehicles
		transporters/quotes
		transporters/transporter_routers
		transporters/orders
		transporters/tracking
		transporters/orders
		bank_account_details ).each do |controller|
  Rails.application.config.assets.precompile += ["#{controller}.js", "#{controller}.css"]
end
