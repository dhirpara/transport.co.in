module Exceptions
	class ResourceNotFound < StandardError; end
end