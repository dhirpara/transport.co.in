require 'rexml/rexml'
module SmsService::Gateway
	class BulksmsduniyaAdapter < BaseService
		include ERB::Util

		def send(receiver, message, sender, settings)
			if settings.api_type == "XML"
				send_with_xml(receiver, message, settings)
			end
		end

		private

		def send_with_xml(receiver, message, settings)
			template = SmsService::Templates::Bulksmsduniya.generate_template(receiver, message, settings)
			encode_template = url_encode(template)
			url = URI(settings.adapter_url + encode_template + "&xmloutput=1")
			http = Net::HTTP.new(url.host, url.port)
			request = Net::HTTP::Post.new(url)
			response = http.request(request)
			parse_response(response)
		end

		def parse_response(res)
			begin
				response = Nokogiri::XML(res.read_body)
				hash_response = Hash.from_xml(response.to_s)
				return Success.new(hash_response, "Message sent.")
			rescue REXML::ParseException => err
				return Error.new(REXML::ParseException, res.read_body || err)
			end
		end


	end
end