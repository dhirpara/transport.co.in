module SmsService::SMS

	GATEWAY_ADAPTERS = {
    bulksmsduniya: SmsService::Gateway::BulksmsduniyaAdapter.new
  }

  SETTINGS_ADAPTERS = {
    bulksmsduniya: SmsService::AdapterSettings::BulksmsduniyaAdapter.new
	}

  module_function

	def send(receiver, message, sender: :bulksmsduniya, sms_adapter: :bulksmsduniya)
		puts "sending SMS >>>>>>>>"
		adapter = GATEWAY_ADAPTERS[sms_adapter]
		adapter_settings =   SETTINGS_ADAPTERS[sms_adapter]
		response = adapter.send(receiver, message, sender, adapter_settings)
		puts "RESPONSE>>>>>>>> #{response.inspect} <<<<<<<<<<<<"
		response
	end

end