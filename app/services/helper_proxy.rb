class HelperProxy

	def self.events_helper
		include ApplicationHelper
		@helper_proxy ||= new
	end

	def get_notifiable_user_type(type, action)
		event_action = admin_support_event.merge(customer_support_event)
		[event_action.dig(type.to_sym, action.to_sym, :notifiable_user).presence || %w(admins),
			event_action.dig(type.to_sym, action.to_sym, :notifiable_association).presence || {}]
		# event_action_settings.dig(type.to_sym, action.to_sym, :nitifiable_user).presence || %w(admins)
	end

	def get_notification_path(type, action, object, user_type=nil)
		# event_action = admin_support_event.merge(customer_support_event(nil, object))
		send("#{user_type}_support_event", nil, object).dig(type.to_sym, action.to_sym, :url).presence || []
	end

end