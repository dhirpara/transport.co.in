
module OtpService
	class Otp < BaseService

		def initialize(options = {})
			@model_name = options[:unique_for]
			@model_attr = options[:unique_attr]
			@otp_code = nil
		end

		def generate(style = :numeric, size = 6)
			validate_arg(style, size)
				.on_success{
		     	return token = if @model_name.present? && @model_attr.present?
						generate_unique_otp(style, size)
				  else
				  	generate_random_otp(style, size)
				  end
				}
		end

		private

		def validate_arg(style, size)
			if !valid_arg.include?(style.to_sym)
				Error.new(ArgumentError, "Invalid style argument.It must be #{valid_arg}.")
			elsif size.class != Fixnum
				Error.new(ArgumentError, "Invalid size argument.It must be numeric.")
			else
				Success.new(true, "Valid parameters.")
			end
		end

		def valid_arg
			[:numeric, :alpha, :alphanumeric]
		end

		def generate_unique_otp(type, size)
			begin
				token = loop do
		     	token = generator(type, size)
		      break token unless @model_name.to_s.camelize.constantize.exists?(@model_attr => token )
		    end
			Success.new(token, "Unique OTP generated for #{@model_name} model.")
			rescue NameError => error
				Error.new(error.class, "Model #{@model_name} not exist.")
			rescue ActiveRecord::StatementInvalid => error
				Error.new(error.class, "Column #{@model_attr} not exist in #{@model_name} model")
			rescue StandardError => error
				Error.new(error.class, error.message)
			end
		end

		def generate_random_otp(type, size)
	    	Success.new(generator(type, size), "Random OTP generated.")
		end

		def generator(type, size)
			begin
				code_array = case type
				when :numeric
					[('0'..'9')].map { |i| i.to_a }.flatten
				when :alpha
					[('a'..'z'), ('A'..'Z')].map { |i| i.to_a }.flatten
				when :alphanumeric
					[('a'..'z'), ('A'..'Z'), (0..9)].map { |i| i.to_a }.flatten
				end
				(0...6).map { code_array[rand(code_array.length)] }.join
			end
		end

	end
end