module Response
	class Base
		attr_reader :data, :message

		def initialize(data = nil, message = nil)
			@data = data
			@message = message
		end

	 	def success?
	 		raise NotImplementedError
		end

		def on_success(&block)
	 		raise NotImplementedError
		end

		def on_error(&block)
	 		raise NotImplementedError
		end
	end
end