class Success < Response::Base

	def success?
		true
	end

	def on_success(&block)
    block.call(@data, @message)
    self
  end

  def on_error(&block)
    self
  end

end