module JsonService::Parser

	module_function

	def parse_with_associations(object)
		object.as_json( include: get_associations(object.class.name) )
	end

	def get_associations(klass)
		klass.constantize.reflections.keys.reject { |key| not_needed_associations_list.include?(key) }.map(&:to_sym)
	end

	def not_needed_associations_list
		%w(web_notifications slugs generated_notifications user_notifications web_notifications)
	end

end