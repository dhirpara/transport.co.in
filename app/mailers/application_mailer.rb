class ApplicationMailer < ActionMailer::Base
  default from: 'jignesh.tps@gmail.com'
  layout 'mailer'

  def dummy
		mail(to: "techplusqa@gmail.com", subject: "Testing mail")
  end
end
