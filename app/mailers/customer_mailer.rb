class CustomerMailer < ApplicationMailer
	default from: "info@transport.co.in"

	def mail_on_load_action(subject, args)
		set_notification(args)
    @url = HelperProxy.events_helper.get_notification_path(args[:type], args[:action], @object, "customer")
		mail(to: args[:user].email, subject: subject)
	end
	[:posted_load, :edited_load, :canceled_load, :create_quote_assoc, :edited_quote_assoc, :accepted_quote_assoc].each { |type| alias_method type, :mail_on_load_action }

	private

	def set_notification(args)
		@object = args[:object]
		@load = @object.try(:load)
	end

end
