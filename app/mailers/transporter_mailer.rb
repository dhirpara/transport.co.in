class TransporterMailer < ApplicationMailer
	default from: "info@transport.co.in"

	def alert_mail_on_load_action(subject, args)
		set_notification(args)
		@user = args[:user]
    @url = HelperProxy.events_helper.get_notification_path(args[:type], args[:action], @object, "transporter")
		mail(to: args[:user].email, subject: subject)
	end
	[:posted_load, :matched_load_assoc, :edited_load, :canceled_load_assoc, :create_quote, :edited_quote, :accepted_quote, :accepted_quote_assoc].each { |type| alias_method type, :alert_mail_on_load_action }

	private

	def set_notification(args)
		@object = args[:object]
		@load = @object.try(:load)
	end

end
