class AdminMailer < ApplicationMailer
	default from: "info@transport.co.in"

	def mail_to_admin(subject, args)
		set_notification(args)
		mail(to: "info@transport.co.in", subject: subject)
	end
	[:create_inquiry, :create_customer, :create_transporter, :create_load, :posted_load, :edited_load, :canceled_load, :create_quote, :edited_quote, :accepted_quote].each { |type| alias_method type, :mail_to_admin }

	private

	def set_notification(args)
		@object = args[:object]
		@load = @object.try(:load)
	end

end
