class SendNotificationJob < ApplicationJob
  queue_as :default
  NOTIFICATION_TYPE = ["sms", "web", "email"]
  USER_TYPE = ["admins", "customers", "transporters"]

  def perform(args)
    args[:notified_via].select! { |type| NOTIFICATION_TYPE.include?(type) }
    @notified_to, @association_notified_to = HelperProxy.events_helper.get_notifiable_user_type(args[:type], args[:action])
  	puts "======notified_to : #{@notified_to}==========="
    puts "======association_notified_to : #{@association_notified_to}==========="
    args[:notified_via].to_a.each do |type|
      puts ""
      puts ">>>>>>>>>>>>>>>>> #{args[:type]} : #{args[:action]} : #{type} Notification <<<<<<<<<<<<<<<<<<<<"
      send("send_#{type}_notification", args)
    end
  end

  private

  def send_web_notification(args)
  	create_notification(args, "web") { |data, user_type, user|
      # data.merge!(template: send("#{user_type}_render_template")).except!(:notified_via, :object) # merge template and remove unnecessary value
      extra_args = data.merge(userType: user_type, template: send("#{user_type}_render_template", user) ).except(:notified_via, :object, :send_to) # merge template and remove unnecessary value
      # ActionCable.server.broadcast("web_notification_#{to}", extra_args)
      ActionCable.server.broadcast("web_notification_#{user_type}_#{user.id}", extra_args)
      puts"   -> Send to : web_notification_#{user_type}_#{user.id} :  #{user.email}"
    }
  end

  def send_sms_notification(args)
    sms = SmsService::SMS
    @notified_to.reject! {|u| u == "admins"}
    @notified_to.each do |user_type|
      singular_user_type = user_type.singularize
      unless args[:action] =~ /matched/
        if args[:object].respond_to?(singular_user_type)
          # mailer_name = "#{args[:action]}_#{args[:type].downcase}"
          # mail_args = args.merge(user_type: user_type, user: data[:object].send(singular_user_type)).except(:notified_via) # merge template and remove unnecessary value
          # (user_type+"Mailer").classify.constantize.send(mailer_name, mail_args).deliver
          user = args[:object].send(singular_user_type)
          url = HelperProxy.events_helper.get_notification_path(args[:type], args[:action], args[:object], singular_user_type)
          message = ApplicationController.render(partial: "/#{user_type}/sms_templates", locals: { data: args, url: url, assoc: false, user: user } )
          res = sms.send(user.ph_no, message)
          puts "-> send SMS to[#{user_type}][#{user.id}] : #{user.ph_no} : #{res}"
        end
        if @association_notified_to.keys.include?(singular_user_type)
          url = HelperProxy.events_helper.get_notification_path(args[:type], args[:action], args[:object], singular_user_type)
          if @association_notified_to[singular_user_type].inject(args[:object], :send).respond_to?('collect') &&
              @association_notified_to[singular_user_type].inject(args[:object], :send).collect(&:"#{singular_user_type}").count > 0
            @association_notified_to[singular_user_type].inject(args[:object], :send).collect(&:"#{singular_user_type}").each do |assoc|
              message = ApplicationController.render(partial: "/#{user_type}/sms_templates", locals: { data: args, url: url, assoc: true, user: assoc } )
              res = sms.send(assoc.ph_no, message)
              puts "-> assoc_send SMS to[#{user_type}][#{assoc.id}] : #{res}"
            end
          elsif !@association_notified_to[singular_user_type].inject(args[:object], :send).respond_to?('collect')
            user = @association_notified_to[singular_user_type].inject(args[:object], :send).send(singular_user_type)
            message = ApplicationController.render(partial: "/#{user_type}/sms_templates", locals: { data: args, url: url, assoc: true, user: user } )
            res = sms.send(user.ph_no, message)
            puts "-> assoc_send SMS to[#{user_type}][#{user.id}] : #{res}"
          end
        end
      else
        args[:send_to].each do |transporter|
          message = ApplicationController.render(partial: "/#{user_type}/sms_templates", locals: { data: args, url: url, assoc: true, user: transporter } )
          res = sms.send(transporter.ph_no, message)
          puts "-> assoc_send SMS to[#{user_type}][#{transporter.id}] : #{res}"
        end
      end
    end
  end

  def send_email_notification(args)
    # create_notification(args, "email") { |data, user_type, user, notification, assoc|
    #   # data.merge!(template: send("#{user_type}_render_template")).except!(:notified_via, :object) # merge template and remove unnecessary value
    #   extra_args = data.merge(user_type: user_type, user: user, notification: notification ).except(:notified_via, :object) # merge template and remove unnecessary value
    #   mailer_name = "#{notification.event_type}_#{notification.notifiable_type.downcase}" + "#{assoc ? '_assoc' : ''}"
    #   (user_type+"Mailer").classify.constantize.send(mailer_name, extra_args).deliver
    #   puts"======Send Email to : #{user_type} : #{user.id} :  assoc :#{assoc}============"
    # }

    # puts "======notified_to : #{@notified_to}==========="
    # puts "======association_notified_to : #{@association_notified_to}==========="
    @notified_to.each do |user_type|
      singular_user_type = user_type.singularize
      unless args[:action] =~ /matched/
        if args[:object].respond_to?(singular_user_type) || singular_user_type == "admin"
          # mailer_name = "#{args[:action]}_#{args[:type].downcase}"
          # mail_args = args.merge(user_type: user_type, user: data[:object].send(singular_user_type)).except(:notified_via) # merge template and remove unnecessary value
          # (user_type+"Mailer").classify.constantize.send(mailer_name, mail_args).deliver
          user = user_type == "admins" ? Admin.first : args[:object].send(singular_user_type)
          mail_to(user, args) # set association object true as last arg
        end
        if @association_notified_to.keys.include?(singular_user_type)
          if @association_notified_to[singular_user_type].inject(args[:object], :send).respond_to?('collect') &&
              @association_notified_to[singular_user_type].inject(args[:object], :send).collect(&:"#{singular_user_type}").count > 0
            @association_notified_to[singular_user_type].inject(args[:object], :send).collect(&:"#{singular_user_type}").each do |assoc|
              mail_to(assoc, args, true) # set association object true as last arg
            end
          elsif !@association_notified_to[singular_user_type].inject(args[:object], :send).respond_to?('collect')
              mail_to(@association_notified_to[singular_user_type].inject(args[:object], :send).send(singular_user_type), args, true) # set association object true as last arg
            # yield args, singular_user_type, @association_notified_to[singular_user_type].inject(args[:object], :send).send(singular_user_type), notification, true
          end
        end
      else
       # send notification to matched transporter
        args[:send_to].each do |transporter|
          mail_to(transporter, args, true) # set association object true as last arg
        end
      end
    end
  end

  def create_notification(data, type)
    # @notified_to, @association_notified_to = HelperProxy.events_helper.get_notifiable_user_type(data[:type], data[:action])
    # puts "======notified_to : #{@notified_to}==========="
    # puts "======association_notified_to : #{@association_notified_to}==========="
      @notified_to.each do |user_type|
        singular_user_type = user_type.singularize
        unless data[:action] =~ /matched/
          if data[:object].respond_to?(singular_user_type) || singular_user_type == "admin"
            puts "--> title : notification.#{type}.#{user_type}.#{data[:type]}.#{data[:action]}.title"
          	notification = data[:object].generated_notifications.create(
          			title: I18n.t("notification.#{type}.#{user_type}.#{data[:type]}.#{data[:action]}.title", customer_id: data[:object].try(:customer_id), load_id: data[:object].try(:load_id) ),
          			body: JsonService::Parser.parse_with_associations(data[:object]),
                event_type: data[:action]
          	)
            notification.send(user_type) << get_notifiable_users_from(object: data[:object], type: user_type)
            puts "--> create notification(#{notification.id}) for : #{user_type}"
          end
          # yield Actioncable for broadcasting notification
          case user_type
          when "admins"
            if type == "email"
              yield data, singular_user_type, Admin.first, notification
            else
              Admin.all.each do |admin|
                yield data, singular_user_type, admin, notification
              end
            end
          when "customers", "transporters"
            if data[:object].respond_to?(singular_user_type)
              yield data, singular_user_type, data[:object].send(singular_user_type), notification, false
            end
          end

          if @association_notified_to.keys.include?(singular_user_type)
            puts "--> ASSOCIATION title : notification.#{type}.#{user_type}.#{data[:type]}.alert.#{data[:action]}.title==========="
            notification = data[:object].generated_notifications.create(
                title: I18n.t("notification.#{type}.#{user_type}.#{data[:type]}.alert.#{data[:action]}.title", load_id: data[:object].try(:load_id)),
                body: JsonService::Parser.parse_with_associations(data[:object]),
                event_type: data[:action]
            )
            puts "--> create notification(#{notification.id}) for : #{user_type}"
            if @association_notified_to[singular_user_type].inject(data[:object], :send).respond_to?('collect') &&
                @association_notified_to[singular_user_type].inject(data[:object], :send).collect(&:"#{singular_user_type}").count > 0
              notification.send(user_type) << @association_notified_to[singular_user_type].inject(data[:object], :send).collect(&:"#{singular_user_type}")
              @association_notified_to[singular_user_type].inject(data[:object], :send).collect(&:"#{singular_user_type}").each do |assoc|
                yield data, singular_user_type, assoc, notification
              end
            elsif !@association_notified_to[singular_user_type].inject(data[:object], :send).respond_to?('collect')
              notification.send(user_type) << @association_notified_to[singular_user_type].inject(data[:object], :send).send(singular_user_type)
              yield data, singular_user_type, @association_notified_to[singular_user_type].inject(data[:object], :send).send(singular_user_type), notification
            end
          end
        else
          # send notification to matched transporter
          notification = data[:object].generated_notifications.create(
              title: I18n.t("notification.#{type}.#{user_type}.#{data[:type]}.alert.#{data[:action]}.title", from_city: data[:from_city], to_city: data[:to_city] ),
              body: JsonService::Parser.parse_with_associations(data[:object]).merge(from_city: data[:from_city], to_city: data[:to_city]),
              event_type: data[:action]
          )
          puts "--> create notification(#{notification.id}) for : #{user_type}"
          notification.send(user_type) << data[:send_to]
          data[:send_to].each do |transporter|
            yield data, singular_user_type, transporter, notification
          end
        end
      end

  end

  def get_notifiable_users_from(args)
    case args[:type]
    when "admins"
      Admin.all
    when "customers", "transporters"
      args[:object].send(args[:type].singularize)
    end
  end

  def admin_render_template(admin)
  	ApplicationController.render(partial: '/shared/admin_web_notification_bedge', locals: { current_admin: admin } )
  end

  def customer_render_template(customer)
    ApplicationController.render(partial: '/shared/customer_web_notification_bedge', locals: { current_customer: customer } )
  end

  def transporter_render_template(transporter)
    ApplicationController.render(partial: '/shared/transporter_web_notification_bedge', locals: { current_transporter: transporter } )
  end

  private

  def mail_to(user, data, assoc=false)
    mailer_name = "#{data[:action]}_#{data[:type].downcase}"+"#{assoc ? '_assoc' : ''}"
    mail_args = data.merge(user: user).except(:notified_via) # merge template and remove unnecessary value
    subject = I18n.t("notification.email.#{user.class.name.downcase.pluralize}.#{data[:type]}#{assoc ? '.alert' : ''}.#{data[:action]}.title", load_id: data[:object].try(:load_id), customer_id: data[:object].try(:customer_id), from_city: data[:from_city], to_city: data[:to_city])
    (user.class.name+"Mailer").classify.constantize.send(mailer_name, *[subject, mail_args]).deliver
    puts "--> title : #{assoc ? 'ASSOCIATION : ' : ''}notification.email.#{user.class.name.downcase.pluralize}.#{data[:type]}#{assoc ? '.alert' : ''}.#{data[:action]}.title"
    puts"   -> Send Email to : #{user.class.name} : #{user.id} : #{user.email}"
  end
end
