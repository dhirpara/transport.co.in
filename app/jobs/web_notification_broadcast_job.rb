class WebNotificationBroadcastJob < ApplicationJob
  queue_as :default

  def perform(*args)
  	create_notification(args.last[:data])
  	args.last.merge!(template: render_template)
    ActionCable.server.broadcast(*args)
  end

  private

  def create_notification(data)
  	data[:object].web_notifications.create(title: data[:title], body: data[:body])
  end

  def render_template
  	ApplicationController.render(partial: 'shared/web_notification_bedge')
  end

end
