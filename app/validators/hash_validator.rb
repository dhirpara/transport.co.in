class HashValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    # exit
    if options[:present].present?
      options[:present].each do |f|
        record.errors[attribute] << (options[:message] || "#{f.to_s} should be present.") unless value[f.to_s].present?
      end
    end
    if options[:numeric].present?
      options[:numeric].each do |f|
        record.errors[attribute] << (options[:message] || "#{f.to_s} should be positive numeric only.") unless is_number?(value[f.to_s])
      end
    end
    return record
  end

  private

  def is_number?(string)
    true if Float(string).positive? rescue false
  end
end