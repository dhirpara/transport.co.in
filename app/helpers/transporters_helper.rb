module TransportersHelper

	def quoted_load?(load, quotes)
		quotes.collect {|q| break [q] if q.load_id == load.id }.compact.blank? ? false : true
	end

	def load_quote(load, quotes)
		quotes.collect {|q| break [q] if q.load_id == load.id }.compact.first
	end

end
