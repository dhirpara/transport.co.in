module ApplicationHelper
	def title(page_title)
    content_for(:title) { "::" + page_title.to_s }
  end

  def user_login?
  	return admin_signed_in? # && #{user}_signed_in?
  end

  def current_resource
  	current_admin || current_customer || current_transporter
  end
  alias_method :current_user, :current_resource

  def show_errors(object, field_name)
	  if object.errors.any?
	    if !object.errors.messages[field_name].blank?
	       error_message = object.errors.messages[field_name].join(", ")
	       field_name == :coordinate ? error_message = "City" + ' ' +  error_message : error_message = field_name.to_s.titleize + ' ' +  error_message
	    end
	  end
	end

  def offered_date_for(load)
    if load.strict_date?
      [[load.loading_date.day, load.loading_date]]
    else
      date_range = ((load.loading_date-2.day).to_datetime..(load.loading_date+2.day).to_datetime)
      date_range.map { |date| [date.day, date] }
    end
  end

  def link_to_remove_fields(name,f)
    f.hidden_field(:_destroy) + link_to_function(name,"remove_fields(this)" ,class: "btn custom-btn custom_btn custom-add-bordered-btn m-none")
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
  end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")",class: "btn custom-btn custom_btn custom-add-bordered-btn m-none")
  end

  def linkable?(notification)
    event_action_settings(notification).dig(notification.notifiable_type.to_sym, notification.event_type.to_sym, :path).presence || "javascript:;"
  end

  def event_action_settings(notification_object=nil)
    # Event which is not defined here should be notifield to default admin and path should be nothing
    case true
    when admin_signed_in?
      admin_support_event(notification_object)
    when customer_signed_in?
      customer_support_event(notification_object)
    when transporter_signed_in?
      transporter_support_event(notification_object)
    end
  end

  def admin_support_event(notification_object=nil)
    {
      Load: {
        create: {
          path: !notification_object.nil? ? edit_admins_load_path(notification_object.notifiable_id) : nil,
          notifiable_user: %w(admins),
        },
        posted: {
          path: !notification_object.nil? ? admins_loads_path(load_status: 'posted') : nil,
          notifiable_user: %w(admins customers) # TODO : for matching transporters
        },
        edited: {
          path: !notification_object.nil? ? admins_loads_path(load_status: 'posted') : nil,
          notifiable_user: %w(admins customers) # TODO : for matching transporters
        },
        canceled: {
          path: nil,
          notifiable_user: %w(admins customers transporters),
          notifiable_association: {"transporter"=> %w(submitted_quotes)}
        },
        matched: {
          path: !notification_object.nil? ? new_transporters_quote_path(notification_object.notifiable_id) : nil,
          notifiable_user: %w(transporters)
        },
        edit_matched: {
          path: !notification_object.nil? ? new_transporters_quote_path(notification_object.notifiable_id) : nil,
          notifiable_user: %w(transporters)
        }
      }
    }
  end

  def customer_support_event(notification_object=nil, object=nil)
    {
      Quote: {
        create: {
          path: !notification_object.nil? && notification_object.notifiable.is_a?(Quote) ? quotes_customers_load_path(notification_object.notifiable.load) : nil,
          url: !object.nil? && object.is_a?(Quote) ? Rails.application.routes.url_helpers.quotes_customers_load_url(object.load, host: Rails.application.config.action_mailer.default_url_options.values.join(":")).to_s : nil,
          notifiable_user: %w(admins customers transporters),
          notifiable_association: {"customer"=> %w(load)}
        },
        edited: {
          path: !notification_object.nil? && notification_object.notifiable.is_a?(Quote) ? quotes_customers_load_path(notification_object.notifiable.load) : nil,
          url: !object.nil? && object.is_a?(Quote) ? Rails.application.routes.url_helpers.quotes_customers_load_url(object.load, host: Rails.application.config.action_mailer.default_url_options.values.join(":")).to_s : nil,
          notifiable_user: %w(admins customers transporters),
          notifiable_association: {"customer"=> %w(load)}
        },
        accepted: {
          path: !notification_object.nil? && notification_object.notifiable.is_a?(Quote) ? load_detail_customers_load_path(notification_object.notifiable.load) : nil,
          url: !object.nil? && object.is_a?(Quote) ? Rails.application.routes.url_helpers.load_detail_customers_load_url(object.load, host: Rails.application.config.action_mailer.default_url_options.values.join(":")).to_s : nil,
          notifiable_user: %w(admins customers transporters),
          notifiable_association: {"customer"=> %w(load), "transporter"=> %w(load submitted_quotes)}
        }
      }
    }
  end

  def transporter_support_event(notification_object=nil, object=nil)
    {
      Quote: {
        accepted: {
          path: !notification_object.nil? && notification_object.event_type == "accepted" && notification_object.notifiable.transporter == current_transporter ? vehicle_list_transporters_order_path(notification_object.notifiable.order) : nil,
          url: !object.nil? && object.is_a?(Quote) && object.accepted? ? Rails.application.routes.url_helpers.vehicle_list_transporters_order_url(object.order, host: Rails.application.config.action_mailer.default_url_options.values.join(":")).to_s : nil,
        }
      },
      Load: {
        matched: {
          path: !notification_object.nil? ? new_transporters_quote_path(load_id: notification_object.notifiable_id) : nil,
          url: !object.nil? && object.is_a?(Load) ? Rails.application.routes.url_helpers.new_transporters_quote_url(load_id: object.id, host: Rails.application.config.action_mailer.default_url_options.values.join(":")).to_s : nil,
          notifiable_user: %w(transporters)
        }
      }
    }
  end
end
