// This is a manifest file that'll be compiled into admin.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require admins/jquery.validate
//= require turbolinks
//= require admins/bootstrap.min
//= require admins/bootstrap-datepicker
//= require admins/icheck
//= require admins/script
//= require admins/retina.min
//= require cable

function remove_fields(link) {
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".bank_account_details").hide();
}

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  $(link).parent().before(content.replace(regexp, new_id));
}
 setTimeout(function(){
    $('.alert').remove();
  }, 5000);
 
$(function(){
  $("#new_admin").validate({
    rules:{
      inputemail:{required:true, email:true},
      pwd:{required:true},
    },
    messages: {
      "admin[email]": {
          required: "Please Enter Email-ID"
      },
      "admin[password]": {
          required: "Please Enter Password"
      }
    }
  });
});