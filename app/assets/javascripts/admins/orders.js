$(document).on('turbolinks:load', function() {
	if($("#vehicle_list")){
    $("#vehicle_list").on("change", function(){
      $.get("/admins/track_order/"+$(this).val(), function(){
        if($("#travelled_distance").length > 0){
          transportApp.getDistance(transportApp.trackingCoordinateArray(), function(distanceInMeter){
            distanceInKm = (distanceInMeter/1000).toFixed(2) + " km"
            $("#travelled_distance").html(distanceInKm)
          })
        }
      })
    })
  }

  $(".clickable-row").on("click", function() {
    window.location = $(this).data("href");
  });

})

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

var rad = function(x) {
  return x * Math.PI / 180;
};

function TransportApp(arguments){
  default_options = {
        autocomplete : false
    };
  arguments = $.extend({}, default_options, arguments);

  if($("#travelled_distance").length > 0){
    this.getDistance(this.trackingCoordinateArray(), function(distanceInMeter){
         distanceInKm = (distanceInMeter/1000).toFixed(2) + " km"
         $("#travelled_distance").html(distanceInKm)
       })
   }
  switch (true) {
		case arguments.autocomplete == true:
			this.setAutocomplete(arguments.selector)
			break;
	}
}

TransportApp.prototype.setAutocomplete = function(selector) {
	_this = this
	var input = (document.getElementById(selector));
	var options = {
  	types: ['(cities)'],
  	componentRestrictions: {country: "in"}
 	};
 	google.maps.event.addDomListener(input, 'keydown', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
    }
  })
	this.autocomplete = new google.maps.places.Autocomplete(input, options);
  this.autocomplete.addListener('place_changed', function() {
    var place = _this.autocomplete.getPlace();
    if (!place.geometry) {
      window.alert("Autocomplete's returned place contains no geometry");
      return;
    }
    $("#trip_tracking_coordinate").val(place.geometry.location.lat() + ',' +place.geometry.location.lng());
  })
}

TransportApp.prototype.getDistance = function(pointWithWayPoint, callback) {
  var total = 0;
  for(i = 0; i < pointWithWayPoint.length - 1; i++ ){
    if(true){
      var R = 6378137; // Earth’s mean radius in meter
      var p1 = pointWithWayPoint[i]
      var p2 = pointWithWayPoint[i+1]
      var dLat = rad(p2.x - p1.x);
      var dLong = rad(p2.y - p1.y);
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1.x)) * Math.cos(rad(p2.x)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;
      total += parseFloat(d);
    }
    else{
     total = 0;
    }
  }
  callback(total) // returns the distance in meter
}

TransportApp.prototype.trackingCoordinateArray = function() {
  var wayPoint = new Array()
  origin = JSON.parse($("#tracking_from_coordinate").val())
  wayPoint[0] = origin
  if($(".tracking_coordinate").length > 0){
    for (i = 1; i <= $(".tracking_coordinate").length; i++) {
      wayPoint[i] = JSON.parse($(".tracking_coordinate")[i-1].value)
    }
  }
  destination = JSON.parse($("#tracking_to_coordinate").val())
  wayPoint.push(destination)
  return wayPoint
}