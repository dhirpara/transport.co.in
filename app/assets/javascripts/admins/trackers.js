$(document).on('turbolinks:load', function() {
	if($("#vehicle_list")){
    $("#vehicle_list").on("change", function(){
      $.get("/admins/trackers/"+$(this).val(), function(){
        transportApp.getDistance(transportApp.trackingCoordinateArray().origin, transportApp.trackingCoordinateArray().current, function(distanceInMeter){
         distanceInKm = (distanceInMeter/1000).toFixed(2) + " km"
         $("#travelled_distance").html(distanceInKm)
      })
      })
    })
  }
})

var rad = function(x) {
  return x * Math.PI / 180;
};

function TransportApp(arguments){
  switch (true) {
  	case arguments.googleMap == true:
			this.from_addr = (document.getElementById('from_address'));
		  this.dest_addr = (document.getElementById('to_address'));

		  this.initMap()
		  this.getAddressInfo()
		  this.setMarkersOnMap()
		  this.setInfoWindowOnMarker()
		  // this.drawRoute(function(distance){
		  // 	if($("#travelled_distance").length > 0){
		  // 		$("#travelled_distance").html(distance)
		  // 	}
		  // })
      this.drawRoute()
      this.getDistance(this.trackingCoordinateArray().origin, this.trackingCoordinateArray().current, function(distanceInMeter){
         distanceInKm = (distanceInMeter/1000).toFixed(2) + " km"
         $("#travelled_distance").html(distanceInKm)
      })
		  break;
		case arguments.autocomplete == true:
			this.setAutocomplete(arguments.selector)
	  	break;
	}
}

TransportApp.prototype.initMap = function() {
	var myOptions = {
      center: new google.maps.LatLng(20.8907819, 73.7273332),
      zoom: 5,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
  this.googleMap = new google.maps.Map(document.getElementById("map"), myOptions);
}

TransportApp.prototype.getAddressInfo = function() {
	fromPoints = JSON.parse($("#from_coordinate").val())
	toPoints = JSON.parse($("#to_coordinate").val())
	this.addressInfo = [{title: this.from_addr.innerHTML, lat: fromPoints.x, lng: fromPoints.y},{title: this.dest_addr.innerHTML, lat: toPoints.x, lng: toPoints.y}]
};

TransportApp.prototype.setMarkersOnMap = function() {
	_this = this
	var labels = "AB"
	this.markers = []
	this.latLngs = []
	var latlngbounds = new google.maps.LatLngBounds();
	this.addressInfo.forEach(function(data, i){
    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
    _this.latLngs.push(myLatlng);
    var marker = new google.maps.Marker({
        position: myLatlng,
        label: labels[i % labels.length],
        map: _this.googleMap,
        title: data.title
    });
    latlngbounds.extend(marker.position);
    _this.markers.push(marker)
	})
  this.googleMap.setCenter(latlngbounds.getCenter());
  this.googleMap.fitBounds(latlngbounds);
}

TransportApp.prototype.setInfoWindowOnMarker = function() {
	_this = this
  var infoWindow = new google.maps.InfoWindow();
	this.markers.forEach(function(marker, i){
		(function (marker, data) {
      google.maps.event.addListener(marker, "click", function (e) {
          infoWindow.setContent(data.title);
          infoWindow.open(map, marker);
      });
    })(marker, _this.addressInfo[i]);
	})
}

TransportApp.prototype.drawRoute = function(callback) {
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;
  _this = this
  directionsDisplay.setMap(this.googleMap);
  	this.markers.forEach(function(marker){ marker.setVisible(false) });
  	directionsService.route({
        origin: this.latLngs[0],
        destination: this.latLngs[1],
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    }, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
    			directionsDisplay.setDirections(response);
          // callback(_this.getDistance(response))
        }
        else{
        	alert('Directions request failed due to ' + status);
        }
    });
}

// TransportApp.prototype.getDistance = function(response) {
//   // console.log(response)
// 	return response.routes[0].legs[0].distance.text;
// }

TransportApp.prototype.setAutocomplete = function(selector) {
	_this = this
	var input = (document.getElementById(selector));
	var options = {
  	types: ['(cities)'],
  	componentRestrictions: {country: "in"}
 	};
 	google.maps.event.addDomListener(input, 'keydown', function(e) {
    if (e.keyCode == 13) {
        e.preventDefault();
    }
  })
	this.autocomplete = new google.maps.places.Autocomplete(input, options);
	this.autocomplete.addListener('place_changed', function() {
		var place = _this.autocomplete.getPlace();
		if (!place.geometry) {
	    window.alert("Autocomplete's returned place contains no geometry");
	    return;
		}
		$("#trip_tracking_coordinate").val(place.geometry.location.lat() + ',' +place.geometry.location.lng());
	})
}

TransportApp.prototype.getDistance = function(p1, p2, callback) {
    if(p1 && p2){
      var R = 6378137; // Earth’s mean radius in meter
      var dLat = rad(p2.lat - p1.lat);
      var dLong = rad(p2.lng - p1.lng);
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(p1.lat)) * Math.cos(rad(p2.lat)) *
        Math.sin(dLong / 2) * Math.sin(dLong / 2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;
      callback(d); // returns the distance in meter
    }
    else{
     callback(0)
    }
}

TransportApp.prototype.trackingCoordinateArray = function() {
  origin = JSON.parse($("#tracking_from_coordinate").val())
  if($("#tracking_coordinate #tracking_current_coordinate").length > 0){
    current = JSON.parse($("#tracking_coordinate #tracking_current_coordinate").val())
    return { origin: { lat: origin.x, lng: origin.y }, current: { lat: current.x, lng: current.y } }
  }
  else{
    return { origin: { lat: origin.x, lng: origin.y } }
  }
}