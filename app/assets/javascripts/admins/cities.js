$(document).on('turbolinks:load', function() {
  if(typeof google != 'undefined'){
    initMap()
  }
})
function initMap() {
  var input = (document.getElementById('city_name'));
  google.maps.event.addDomListener(input, 'keydown', function(e) { 
      if (e.keyCode == 13) { 
          e.preventDefault(); 
      }
    })
    
  var options = {
    	types: ['(cities)'],
    	componentRestrictions: {country: "in"}
   	};
  	
  var autocomplete = new google.maps.places.Autocomplete(input, options);

    autocomplete.addListener('place_changed', function() {
    	var place = autocomplete.getPlace();
    	if (!place.geometry) {
	      window.alert("Autocomplete's returned place contains no geometry");
	      return;
    	}
    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }
    $("#city_name").val(place.address_components[0].long_name);
    $("#city_coordinate").val(place.geometry.location.lat() + ',' +place.geometry.location.lng());
  	});
	}


  function check_coordinate(){
    $( "#city_name" ).on('keyup', function(){
      if($("#city_name").val() == ''){
        $("#city_coordinate").val('');
      }
    });
  }