$(function(){
  $("#load_form").submit(function(e){
    if(!$.isEmptyObject(points) && Object.keys(points).length == 2){
      calcDistance(points, function(distance){
        $("#load_trip_distance").val(distance);
      });
    }
  });
});

$(document).on('turbolinks:load', function() {
  if(typeof google != 'undefined'){
    initMap();
  }

  $("#approve_load").on("click", function(e){
    // return false
    e.preventDefault();
    this.disabled = true;
    $.get("/get_notifyable_transporter/"+$(this).data("load-id"), function(transportRoute){
      console.log("transportRoute :", transportRoute);
      transportApp.loadId = $(e.target).data("load-id");
      transportApp.transporterIds = transportRoute.fromRouteBoxerHistroy;
      if(transportRoute.needToApplyRouteboxer.length > 0){
        transportApp.sendNotificationAfterApplyRouteboxer(transportRoute.needToApplyRouteboxer);
      }
      else{
       transportApp.sendNotification();
      }
    });
    console.log("submitting");
  });
});

var autocomplete = [];
var place = [];
var points = {};
var latlngSet = ["sourceLat", "sourceLng", "destLat", "destLng"];
var distanceEle = document.getElementById("load_trip_distance");
function initMap() {
  var from_addr = (document.getElementById('load_from_address'));
  var dest_addr = (document.getElementById('load_to_address'));
  var options = {
    	componentRestrictions: {country: "in"}
   	};
    if(from_addr && dest_addr){
      [from_addr, dest_addr].forEach(function(addr, i){
        google.maps.event.addDomListener(addr, 'keydown', function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }
          });
      });

      if($(from_addr).val() && $(dest_addr).val()){
        loadOriginLong($(from_addr).val());
        loadDestinationLong($(dest_addr).val());
      }
      else if($(from_addr).val()){
        loadOriginLong($(from_addr).val());
      }
      else if($(dest_addr).val()){
        loadDestinationLong($(dest_addr).val());
      }
      originPlaceChangedEvent(from_addr, options);
      destinationPlaceChangedEvent(dest_addr, options);
    }
}

function calcDistance (points, callback) {
  callback((google.maps.geometry.spherical.computeDistanceBetween(points.origin, points.destination)/ 1000).toFixed(2));
}

function empty(array){
  return array.length === 0;
}

function loadOriginLong(originAdd){
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': originAdd}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK){
      points.origin = results[0].geometry.location;
    }
  });

}

function loadDestinationLong(destinationAdd){
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': destinationAdd}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK){
      points.destination = results[0].geometry.location;
    }
  });
}

function originPlaceChangedEvent(from_addr, options){
  origin_autocomplete = new google.maps.places.Autocomplete(from_addr, options);
  origin_autocomplete.addListener('place_changed', function() {
    var place = this.getPlace();
    points.origin = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
  });
}

function destinationPlaceChangedEvent(dest_addr, options){
  destination_autocomplete = new google.maps.places.Autocomplete(dest_addr, options);
  destination_autocomplete.addListener('place_changed', function() {
    var place = this.getPlace();
    points.destination = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
  });
}
/* js for drop-down menu
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
};


// New JS
function TransportApp(rdistance){
  if($("#from_coordinate").length > 0){
    originPointAry = $("#from_coordinate").val().split(",");
    destinationPointAry = $("#to_coordinate").val().split(",");
    this.rdistance = rdistance || 20;
    this.transporterList = [];
    this.originLatLng = new google.maps.LatLng({lat: parseFloat(originPointAry[0]), lng: parseFloat(originPointAry[1])});
    this.destinationLatLng = new google.maps.LatLng({lat: parseFloat(destinationPointAry[0]), lng: parseFloat(destinationPointAry[1])});
  }
}

TransportApp.prototype.sendNotificationAfterApplyRouteboxer = function(routes) {
  _this = this;
  // this.getTransporterRoutes(function(routes){
  // })
  console.log(routes);
  _this.allTransporterRoutes = routes;
  _this.setBoxesForRoutes();
};

// TransportApp.prototype.getTransporterRoutes = function(callback) {
//   $.get("/all_transporter_routes",{}, function(data){
//     callback(data)
//   })
// };

TransportApp.prototype.setBoxesForRoutes = function() {
  var directionsService = new google.maps.DirectionsService();
  var routeBoxer = new RouteBoxer();
  _this = this;
  var routeBoxCreateCount = this.allTransporterRoutes.length;
  this.allTransporterRoutes.forEach(function(route){
    originPoints = new google.maps.LatLng({lat: route.origin.x, lng: route.origin.y});
    destinationPoints = new google.maps.LatLng({lat: route.destination.x, lng: route.destination.y});
    directionsService.route({
      origin: {lat: originPoints.lat(), lng: originPoints.lng()},
      destination: {lat: destinationPoints.lat(), lng: destinationPoints.lng()},
      travelMode: 'DRIVING'
    }, function(response, status) {
      if (status === 'OK') {
        var path = response.routes[0].overview_path;
        route.boxes = routeBoxer.box(path, 20);
        route.boxRectangles = [];
        route.boxes.forEach(function(box, i){
          route.boxRectangles[i] = new google.maps.Rectangle({
            bounds: box
          });
        });
      } else {
        window.alert('Directions request failed due to ' + status);
      }
      routeBoxCreateCount--;
      if(routeBoxCreateCount === 0){
        _this.getTransporterList();
      }
    });
  });
};

TransportApp.prototype.getTransporterList = function() {
  _this = this;
  this.allTransporterRoutes.forEach(function(route){
    route.boxRectangles.some(function(boxRectangle){
      if(boxRectangle.getBounds().contains(_this.originLatLng)){
        route.boxRectangles.some(function(boxRectangle){
          if(boxRectangle.getBounds().contains(_this.destinationLatLng)){
            _this.transporterList.push(route.transRouteId);
            return true;
          }
        });
        return true;
      }
    });
  });
  _this.sendNotification();
};

// TODO : Notify matching transporter : Load : posted
TransportApp.prototype.sendNotification = function() {
  // $.post("/send_notification_for_new_load", {transporter_routes_id: this.transporterList})
  // App.common.sendNotificationToTransporter(this.transporterList)
  // Get city name
  geocoder = new google.maps.Geocoder();
  _this = this;
  geocoder.geocode( { 'location': _this.originLatLng}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK){
      var originResult = results;
      geocoder.geocode( { 'location': _this.destinationLatLng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK){
          var destinationResult = results;

          getCityName(originResult, destinationResult);
          postNotification();
        }
      });
    }
  });
};

getCityName = function(origin, destination){
  if(origin && destination){
    var i;
    var b;
    for (i=0; i<origin[0].address_components.length; i++) {
      for (b=0;b<origin[0].address_components[i].types.length;b++) {
        if (origin[0].address_components[i].types[b] == "administrative_area_level_2") {
          transportApp.sourceCity= origin[0].address_components[i].short_name;
          break;
        }
      }
    }
    for (i=0; i<destination[0].address_components.length; i++) {
      for (b=0;b<destination[0].address_components[i].types.length;b++) {
        if (destination[0].address_components[i].types[b] == "administrative_area_level_2") {
          transportApp.destinationCity= destination[0].address_components[i].short_name;
          break;
        }
      }
    }
  }
};

postNotification = function(){
  $.post("/send_notification_for_new_load",{
    transporter_routes_ids: transportApp.transporterList,
    transporter_ids: transportApp.transporterIds,
    load_id: transportApp.loadId,
    load_origin_city: transportApp.sourceCity,
    load_destination_city: transportApp.destinationCity,
    load_origin_coordinate: [transportApp.originLatLng.lat(), transportApp.originLatLng.lng()],
    load_destination_coordinate: [transportApp.destinationLatLng.lat(), transportApp.destinationLatLng.lng()]
  });
  $("form.edit_load").submit();
};