// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
// require jquery
//= require jquery_ujs
//= require jquery.validate
//= require jquery.canvasjs.min
//= require turbolinks
//= require jquery.loadTemplate.min
//= require main-js
//= require init
//= require cable
//= require setup
// require materialize.min


$(window).scroll(function() {
  var scroll = $(window).scrollTop();

  if (scroll >= 100) {
    $(".go-top").addClass("show-btn");
  } else {
    $(".go-top").removeClass("show-btn");
  }
});
// $(document).on('ready page:load',function () {
//   $('select').material_select();
//   $('.modal-trigger').leanModal({
//     dismissible: true, // Modal can be dismissed by clicking outside of the modal
//     opacity: .87, // Opacity of modal background
//     in_duration: 300, // Transition in duration
//     out_duration: 200 // Transition out duration
//   });
//   $('a[href=#top]').click(function () {
//     $('body').animate({
//       scrollTop: 0
//     },1500);
//   });
// });
$(document).ready(function() {

  // console.log('helolo');
  // $('.modal-trigger').leanModal({
  //   dismissible: true, // Modal can be dismissed by clicking outside of the modal
  //   opacity: .5, // Opacity of modal background
  //   in_duration: 300, // Transition in duration
  //   out_duration: 200, // Transition out duration
  //   ready: function() {
  //         //alert('Ready');
  //     }, // Callback for Modal open
  //   complete: function() {
  //         //alert('Closed');
  //     } // Callback for Modal close
  // });
  // $(document).on('click','.swap_v',function(){
  //   var source_text = $('#load_from_address').val();
  //   var destination_text = $('#load_to_address').val();

  //   $('#load_to_address').val(source_text);
  //   $('#load_from_address').val(destination_text);
  // });
  if (typeof from_address !== typeof undefined) {
    if (from_address && to_address) {
      [from_address, to_address].forEach(function(address, i) {
        google.maps.event.addDomListener(address, 'keydown', function(e) {
          if (e.keyCode == 13) {
            e.preventDefault();
          }
        })
      })
    }
  }
  // $("#landing_form").validate({
  //   rules: {
  //     from_address: "required",
  //     to_address: "required",
  //     phone: "required",
  //   },
  // //For custom messages
  //   messages: {
  //     from_address: "Please enter from address ",
  //     to_address: "Please enter destination address",
  //     phone: "Please enter valid phone number"
  //   },
  //   errorElement: 'div',
  //   errorPlacement: function(error, element) {
  //     var placement = $(element).data('error');
  //     if (placement) {
  //       $(placement).append(error)
  //     } else {
  //       error.insertAfter(element);
  //     }
  //   }
  // });
  // $("#new_customer").validate({
  //    rules: {
  //      "customer[login]" : "required",
  //      "customer[password]" : "required",
  //      "customer[ph_no]" : "required",
  //      "customer[password_confirmation]": "required",
  //      // "customer[email]":"required"
  //    },
  //    //For custom messages
  //    messages: {
  //      "customer[login]": "Please enter valid email",
  //      "customer[password]": "Please enter password",
  //      "customer[ph_no]": "Please enter valid phone number",
  //      "customer[password_confirmation]": "Please enter password confirmation",
  //      // "customer[email]":"Please enter valid email"
  //    },
  //    errorElement: 'div',
  //    errorPlacement: function(error, element) {
  //      var placement = $(element).data('error');
  //      if (placement) {
  //          $(placement).append(error)
  //      } else {
  //          error.insertAfter(element);
  //      }
  //    }
  //  });

  // $("#load_form").validate({
  //    rules: {
  //      "load[load_type_id]": "required",
  //      "load[weight][value]":"required",
  //      "load[vehicle_type_id]":"required",
  //      "load[vehicle_quantity]":"required",
  //      "load[from_address]":"required",
  //      "load[to_address]":"required"
  //    },
  //     messages: {
  //      // "load[load_type_id]": "please select valid load type",
  //      "load[from_address]": "Please select from address",
  //      "load[to_address]": "Please select destination address",
  //      "load[vehicle_quantity]":"Please enter valid vehicle quantity",
  //      "load[weight][value]":"Please enter valid weight value",
  //      "load[load_type_id]": "Please select valid load type"
  //    },
  //    errorElement: 'div',
  //    errorPlacement: function(error, element) {
  //      var placement = $(element).data('error');
  //      if (placement) {
  //          $(placement).append(error)
  //      } else {
  //          error.insertAfter(element);
  //      }
  //    }
  //   });
  //  $("#new_transporter").validate({
  //    rules: {
  //      "transporter[login]" : "required",
  //      "transporter[password]" : "required",
  //      "transporter[ph_no]" : "required",
  //      "transporter[password_confirmation]": "required",
  //      "transporter[email]":"required"
  //    },
  //    //For custom messages
  //    messages: {
  //      "transporter[login]": "Please enter valid email",
  //      "transporter[password]": "Please enter password",
  //      "transporter[ph_no]": "Please enter valid phone number",
  //      "transporter[password_confirmation]": "Please enter password confirmation",
  //      "transporter[email]":"Please enter valid email"
  //    },
  //    errorElement: 'div',
  //    errorPlacement: function(error, element) {
  //      var placement = $(element).data('error');
  //      if (placement) {
  //          $(placement).append(error)
  //      } else {
  //          error.insertAfter(element);
  //      }
  //    }
  //  });
});
// $(document).on('turbolinks:load', function() {
//   console.log('truck')
//   $('#assign_truck').on('click',function(){
//       console.log('in assign truck')
//     });
// });
// $(".waves-effect").click(function() {
//   // assumes element with id='button'
//   $(".side-nav").hide();
//   $('#sidenav-overlay').hide();
// });

// $(".button-collapse").click(function() {
//   // assumes element with id='button'
//   $(".side-nav").show();
//   $('#sidenav-overlay').show();
// });

$(document).on("turbolinks:load", function() {
  $('select').material_select();
  $(document).on("click", ".close-popup", function() {
    $('#modal-quotes').closeModal();
  });
});
