$(document).on('turbolinks:load', function() {
  if(typeof google != 'undefined'){
    initMap()
    if($("#travelled_distance")){
      getDistance();
    }
  }

  if($("#vehicle_list")){
    $("#vehicle_list").on("change", function(){
      $.get("track_details", { trip: $(this).val() }, function(){
        getDistance();
      })
    })
  }
})

function initMap() {
  var from_addr = (document.getElementById('from_address'));
  var dest_addr = (document.getElementById('to_address'));
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;

  map = new google.maps.Map(document.getElementById('map'), {
  mapTypeControl: false,
  zoom: 13
  });

  directionsDisplay.setMap(map);
  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29)
  });
  var options = {
      componentRestrictions: {country: "in"}
    };
    if(from_addr && dest_addr){
      [from_addr, dest_addr].forEach(function(addr, i){
        google.maps.event.addDomListener(addr, 'keydown', function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }
          })
      })
      marker.setVisible(false);
      loadOriginLong($(from_addr).html(), function(id){
        origin_place_id = id.place_id
        loadDestinationLong($(dest_addr).html(), function(id){
          destination_place_id = id.place_id
          route(origin_place_id,destination_place_id,directionsService, directionsDisplay,marker,infowindow,from_addr,dest_addr, function(){})
        })
      })
    }
}

function loadOriginLong(originAdd, callback){
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': originAdd}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK){
      callback(results[0])
    }
  });
}

function loadDestinationLong(destinationAdd, callback){
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': destinationAdd}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK){
      callback(results[0])
    }
  });
}

function route(origin_place_id, destination_place_id,directionsService, directionsDisplay,marker,infowindow,origin_addr,destination_addr, callback) {
  if (!origin_place_id || !destination_place_id) {
    return;
  }
  travel_mode = google.maps.TravelMode.DRIVING;

  directionsService.route({
    origin: {'placeId': origin_place_id},
    destination: {'placeId': destination_place_id},
    travelMode: travel_mode
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      if(marker){
        infowindow.close();
        marker.setVisible(false);
      }
      directionsDisplay.setDirections(response);
      var distanceInput = document.getElementById("distanceKm");
      distanceInput = response.routes[0].legs[0].distance.value / 1000;
      callback(distanceInput)
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

function getDistance(){
  var from_addr = (document.getElementById('from_address'));
  var current_addr = ($("#tracking_list span:last")[0]);
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;

  if(!current_addr){
    $("#travelled_distance").html("0 kms")
  }
  loadOriginLong($(from_addr).html(), function(id){
    origin_place_id = id.place_id
    loadDestinationLong($(current_addr).html(), function(id){
      destination_place_id = id.place_id
      route(origin_place_id,destination_place_id,directionsService, directionsDisplay, undefined, undefined,from_addr,current_addr, function(distanceKm){
        $("#travelled_distance").html(distanceKm + " kms")
      })
    })
  })
}

function trackVehicleList() {
    document.getElementById("myDropdown").classList.toggle("show");
}