App.common = App.cable.subscriptions.create "CommonChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    console.log "CommonChannel received : ", data
    ele = $("#"+data.userType+"_web_notification_count .badge-notify#"+data.userType+"-"+data.userId)
    if ele.length > 0 && parseInt(ele.html()) <= 0
      ele.removeClass("hidden")
      ele.removeAttr("style")
    ele.html(parseInt(ele.html()) + 1)


  # TODO : Notify matching transporter : Load : posted
  sendNotificationToTransporter: (transporterList) ->
  	console.log("transporterList :", transporterList)
  	@perform 'notify_transporter', transporterList