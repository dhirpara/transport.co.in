App.loads = App.cable.subscriptions.create "LoadsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    console.log "LoadChannel received:", data
    switch data.action
    	when "create" then $("#admin_web_notification_count").html(data.data)

  create: (loadId) ->
  	console.log "LoadChannel create", loadId
  	@perform 'create', loadId: loadId
