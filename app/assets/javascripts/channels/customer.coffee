App.customer = App.cable.subscriptions.create "CustomerChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    console.log "CustomerChannel received : ", data
    ele = $("#"+data.userType+"_web_notification_count .badge-notify#"+data.userType+"-"+data.userId)
    if ele.length > 0 && parseInt(ele.html()) <= 0
      ele.removeClass("hidden")
      ele.removeAttr("style")
    ele.html(parseInt(ele.html()) + 1)
