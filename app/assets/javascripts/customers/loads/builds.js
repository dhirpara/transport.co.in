
$(function(){
	$("#load_form").submit(function(e){
		if(!$.isEmptyObject(points) && Object.keys(points).length == 2){
			calcDistance(points, function(distance){
				$("#load_trip_distance").val(distance);
			});
		}
	});
});

$(document).on('turbolinks:load', function() {
	if(typeof google != 'undefined'){
		initMap();
	}
	$(".tab").on('click', function (e) {
		// alert("hi");
		var num = $(this).find("#load").attr('data');
		document.getElementById('vehicle_load').value = num;
	});

  if($('select').length > 0){
    $('select').material_select();
  }
  if($('.datepicker').length > 0){
  	$('.datepicker').pickadate({
  		selectMonths: true, // Creates a dropdown to control month
  		selectYears: 15 // Creates a dropdown of 15 years to control year
  	});
  }

  $(document).on('click','.swap_v',function(){
    var source_text = $('#load_from_address').val();
    var destination_text = $('#load_to_address').val();
    var source_coordinate = $('#load_from_coordinate').val();
    var destination_coordinate = $('#load_to_coordinate').val();

    $('#load_to_address').val(source_text);
    $('#load_from_address').val(destination_text);
    $('#load_from_coordinate').val(destination_coordinate);
    $('#load_to_coordinate').val(source_coordinate);
		initMap();
  });
});

var autocomplete = [];
var map = null ;
var origin_place_id = null;
var destination_place_id = null;
var place = [];
var points = {};
var infowindow = null;
var marker = null;
var routeBoxer = null;
var boxpolys = null;
var rdistance = 20; // km
var latlngSet = ["sourceLat", "sourceLng", "destLat", "destLng"];
var distanceEle = document.getElementById("load_trip_distance");
var allPlaces = [];
function initMap() {
	var from_addr = (document.getElementById('load_from_address'));
	var dest_addr = (document.getElementById('load_to_address'));
	var directionsService = new google.maps.DirectionsService();
	var directionsDisplay = new google.maps.DirectionsRenderer();
	
			if($(from_addr).val() && $(dest_addr).val()){
					map = new google.maps.Map(document.getElementById('map'), {
					mapTypeControl: false,
					zoom: 13
					});
			}   
			else{
					map = new google.maps.Map(document.getElementById('map'), {
					mapTypeControl: false,
					center: {lat: 23.022505, lng: 72.57136209999999},
					zoom: 13
				});
			}
	directionsDisplay.setMap(map);
	var infowindow = new google.maps.InfoWindow();
	marker = new google.maps.Marker({
		map: map,
		anchorPoint: new google.maps.Point(0, -29)
	});
	var options = {
			componentRestrictions: {country: "in"}
		};
		if(from_addr && dest_addr){
			[from_addr, dest_addr].forEach(function(addr, i){
				google.maps.event.addDomListener(addr, 'keydown', function(e) {
						if (e.keyCode == 13) {
								e.preventDefault();
						}
					});
			});

			if($(from_addr).val() && $(dest_addr).val()){  
				marker.setVisible(false);
				loadOriginLong($(from_addr).val(), function(id){
					origin_place_id = id.place_id;
					loadDestinationLong($(dest_addr).val(), function(id){
						destination_place_id = id.place_id;
						route(origin_place_id,destination_place_id,directionsService, directionsDisplay,marker,infowindow,from_addr,dest_addr);
					});
				});
			}
			else if($(from_addr).val()){
				loadOriginLong($(from_addr).val(), function(id){
					origin_place_id = id.place_id;
					set_position($(from_addr).val(),id,marker,infowindow);
				});
			}
			else if($(dest_addr).val()){
				loadDestinationLong($(dest_addr).val(), function(id){
						destination_place_id = id.place_id;
						set_position($(dest_addr).val(),id,marker,infowindow);
					});
			}
			originPlaceChangedEvent(from_addr,dest_addr, options,directionsService,directionsDisplay,marker,infowindow);
			destinationPlaceChangedEvent(from_addr,dest_addr, options,directionsService,directionsDisplay,marker, infowindow);
		}
}

function calcDistance (points, callback) {
	callback((google.maps.geometry.spherical.computeDistanceBetween(points.origin, points.destination)/ 1000).toFixed(2));
}

function empty(array){
	return array.length === 0;
}

function loadOriginLong(originAdd, callback){
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': originAdd}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK){
			points.origin = results[0].geometry.location;
			callback(results[0]);
		}
	});
}

function loadDestinationLong(destinationAdd, callback){
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode( { 'address': destinationAdd}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK){
			points.destination = results[0].geometry.location;
			callback(results[0]);
		}
	});
}

function originPlaceChangedEvent(from_addr,dest_addr, options,directionsService,directionsDisplay,marker,infowindow) {
	origin_autocomplete = new google.maps.places.Autocomplete(from_addr, options);
	origin_autocomplete.bindTo('bounds', map);
	origin_autocomplete.addListener('place_changed', function() {
		var place = this.getPlace();
		// expandViewportToFitPlace(map, place);
		origin_place_id = place.place_id;
		$("#load_from_coordinate").val([place.geometry.location.lat(), place.geometry.location.lng()]);
		marker.setVisible(false);
		infowindow.close();
		set_position($(from_addr).val(),place,marker,infowindow);
		route(origin_place_id, destination_place_id,directionsService, directionsDisplay,marker,infowindow,from_addr,dest_addr);
		points.origin = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
	});  
}

function destinationPlaceChangedEvent(from_addr,dest_addr, options,directionsService,directionsDisplay,marker, infowindow) {
	destination_autocomplete = new google.maps.places.Autocomplete(dest_addr, options);
	destination_autocomplete.bindTo('bounds', map);
	destination_autocomplete.addListener('place_changed', function() {
		var place = this.getPlace();
		// expandViewportToFitPlace(map, place);
		destination_place_id = place.place_id;
		$("#load_to_coordinate").val([place.geometry.location.lat(), place.geometry.location.lng()]);
		marker.setVisible(false);
		infowindow.close();
		set_position($(dest_addr).val(),place,marker,infowindow);
		route(origin_place_id, destination_place_id,directionsService, directionsDisplay,marker,infowindow,from_addr,dest_addr);
		points.destination = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
	});
}

function destinationPlaceChangedEvent(from_addr,dest_addr, options,directionsService,directionsDisplay,marker, infowindow) {
  destination_autocomplete = new google.maps.places.Autocomplete(dest_addr, options);
  destination_autocomplete.bindTo('bounds', map);
  destination_autocomplete.addListener('place_changed', function() {
    var place = this.getPlace();
    // expandViewportToFitPlace(map, place);
    destination_place_id = place.place_id;
    $("#load_to_coordinate").val([place.geometry.location.lat(), place.geometry.location.lng()]);
    marker.setVisible(false);
    infowindow.close();
    set_position($(dest_addr).val(),place,marker,infowindow);
    route(origin_place_id, destination_place_id,directionsService, directionsDisplay,marker,infowindow,from_addr,dest_addr);
    points.destination = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
    if(document.getElementById('load_from_address').value !== "" && document.getElementById('load_to_address').value !== ""){
      calculateAndDisplayRoute(directionsService, directionsDisplay);
    }
  });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
  directionsService.route({
    origin: document.getElementById('load_from_address').value,
    destination: document.getElementById('load_to_address').value,
    travelMode: 'DRIVING'
  }, function(response, status) {
    if (status === 'OK') {
      directionsDisplay.setDirections(response);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}


function route(origin_place_id, destination_place_id,directionsService, directionsDisplay,marker,infowindow,origin_addr,destination_addr) {
  if (!origin_place_id || !destination_place_id) {
    return;
  }
  travel_mode = google.maps.TravelMode.DRIVING;
  routeBoxer = new RouteBoxer();

  directionsService.route({
    origin: {'placeId': origin_place_id},
    destination: {'placeId': destination_place_id},
    travelMode: travel_mode
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      infowindow.close();
      marker.setVisible(false);
      directionsDisplay.setDirections(response);

      var route = response.routes[0];
      var summaryPanel = document.getElementById("directions_panel");
       // Box the overview path of the first route
      var path = response.routes[0].overview_path;
      var boxes = routeBoxer.box(path, rdistance);
      response.routes[0].legs[0].start_address = $(origin_addr).val();
      response.routes[0].legs[0].end_address = $(destination_addr).val();
      // clearBoxes();
      // drawBoxes(boxes);
      // findPlaces(boxes);
      // for (var i = 0; i < boxes.length; i++) {
      //   var bounds = boxes[i];
      //   console.log(bounds)
      // }
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

function set_position(place_name,place,marker,infowindow){
    marker.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
    marker.setPosition(place.geometry.location);
    marker.setVisible(true);
    infowindow.setContent('<div>' + place_name + '</div>');
    infowindow.open(map, marker);
}

// function expandViewportToFitPlace(map, place) {
//           if (place.geometry.viewport) {
//             map.fitBounds(place.geometry.viewport);
//           } else {
//             map.setCenter(place.geometry.location);
//             map.setZoom(17);
//           }
//         }

//  ***********Draw boxex on map***********************************//
function drawBoxes(boxes) {
  console.log(boxes);
  window.boxpolys = new Array(boxes.length);
  for (var i = 0; i < boxes.length; i++) {
    boxpolys[i] = new google.maps.Rectangle({
      bounds: boxes[i],
      fillOpacity: 0,
      strokeOpacity: 1.0,
      strokeColor: '#000000',
      strokeWeight: 1,
      map: map
    });
    console.log(boxpolys[i]);
  }
  for (i = 0; i < boxpolys.length; i++) {
    // console.log(points)
    if(boxpolys[i].getBounds().contains(new google.maps.LatLng({lat: 23.022505, lng: 72.571362})) ||
      boxpolys[i].getBounds().contains(new google.maps.LatLng({lat: 21.17024, lng: 72.831061}))){
      boxpolys[i].setOptions({strokeColor: '#FF0000'});
    }
    console.log(boxpolys[i].getBounds().contains(new google.maps.LatLng({lat: 23.022505, lng: 72.571362})));
  }
}

// Clear boxes currently on the map
function clearBoxes() {
  if (boxpolys !== null) {
    for (var i = 0; i < boxpolys.length; i++) {
      boxpolys[i].setMap(null);
    }
  }
  boxpolys = null;
}

function findPlaces(boxes) {
 var data = "";
 for (var i = 0; i < boxes.length; i++) {
	//form the query string that will be sent via ajax
	if (data !== "") {
	 	data += "&";
		}
	data += "boxes[]=" + boxes[i].getNorthEast().lat() + ":" + boxes[i].getNorthEast().lng() + "-" + boxes[i].getSouthWest().lat() + ":" + boxes[i].getSouthWest().lng();
 	}
 	console.log(data);
}
function performSearch(bound) {
   console.log("in performSearch");
   var request = {
     bounds: bound,
     keyword: 'bars'
   };
   console.log(bound);
   currentBound = bound;
   new google.maps.places.PlacesService(map).radarSearch(request, callback);
 }

 function callback(results, status) {
   if (status !== google.maps.places.PlacesServiceStatus.OK) {
     console.error(status);
     return;
   }

   for (var i = 0, result; result == results[i]; i++) {
     // Go through each result from the search and if the place exist already in our list of places then done push it in to the array
     if (!placeExists(result.id)) {
       allPlaces.push(result);
     }
   }
   console.log(allPlaces);
 }

// var LoadBuildJs = {
// 	init: function(){
// 		console.log("LoadBuild init called");
// 		$("[data-load=submitted]").on('click', function(e){
// 			console.log("submite for create");
// 			App.loads.create($(this).data("load-id"));
// 		});
// 	}
// };
