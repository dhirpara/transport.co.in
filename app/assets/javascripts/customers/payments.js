$(document).on('turbolinks:load', function() {
	if($('ul.payment').length > 0){
		$('ul.payment').on('click', 'a', function(){
			var type = this.getAttribute("href").slice(1)
			payments.paymentType = $(this).html()
			if(payments.installments[type] == undefined)
				payments.initInstallment(type)
			payments.updatePaymentForm(type)
		})
	}
	if($('.date').length > 0){
		$(document).on("change", ".date", function(){
			var type = $(this).parents('form').parent()[0].id
			var num = parseInt($(this).parents(".installment_content")[0].id.slice(-1))
			var installmentLength = Object.keys(payments.installments[type]).length
			payments.installments[type][num].date = new Date(this.value)
			payments.calcExtraFees(payments.installments[type][num].date, num, type)
			for (i = num+1; i <= installmentLength; i++) {
				console.log(payments.installments[type][i].date.getTime() <= payments.installments[type][num].date.getTime())
				if(payments.installments[type][i].date.getTime() <= payments.installments[type][num].date.getTime()){
					var date = new Date(payments.installments[type][i-1].date)
					payments.installments[type][i].date = new Date(date.setDate(date.getDate() + 1));
					payments.calcExtraFees(payments.installments[type][i].date, i, type)
				}
			}
			payments.updatePaymentForm(type)
		})
	}

	if($(".remain_payment_txt")){
		$(document).on("change", ".remain_payment_txt", function(){
			var type = $(this).parents('form').parent()[0].id
			var num = parseInt($(this).parents(".installment_content")[0].id.slice(-1))
			payments.installments[type][num].payable = parseFloat($(this).val())
			payments.totalRemainingPayment = payments.installments[type][num-1].remain - payments.installments[type][num].payable;
			payments.calcExtraFees(payments.installments[type][num].date, num, type)
			payments.installments[type][num].remain = payments.totalRemainingPayment
			for(var count in payments.installments[type]){
				if(count > num && !payments.installments[type][num].remain) console.log(count)
				if(count > num && !payments.installments[type][num].remain) delete payments.installments[type][count]
			}
			if(payments.totalRemainingPayment > 0){
				payments.generateInstallment(type, {from: num+1})
			}
			payments.updatePaymentForm(type)
		})
	}
})


function Payment(arg){
	this.op = parseFloat(arg.offerPrice);
	this.installments = {};
	this.paymentType = $(".tabs.payment").find('a.active').html()

	this.getPaymentPlans(function(){
		this.initInstallment("fullpay")
		this.updatePaymentForm("fullpay")
	}.bind(this))
}

Payment.prototype.getPaymentPlans = function (callback){
	$.get('/customers/payments/get_all_plans', function(plans){
		this.paymentPlans = plans.paymentPlans
		callback()
	}.bind(this))
};

Payment.prototype.initInstallment = function (type, args){
	var i = 1;
	this.installments[type]={}
	var payableEle = $("#"+type+" #installment_content_"+i+" #payable_amount")
	var payableDateEle = $("#"+type+" #installment_content_"+i+" #payment_date")
	this.totalRemainingPayment = this.op;
	this.installments[type][i] = {};
	this.installments[type][i].payable = parseFloat(payableEle.val());
	this.installments[type][i].date = new Date(payableDateEle.val());
	this.totalRemainingPayment -= this.installments[type][i].payable;
	this.calcExtraFees(this.installments[type][i].date, 1, type)
	this.installments[type][i].remain = this.totalRemainingPayment
	console.log("installment :", i, "Payable:", this.installments[type][i].payable, "remain :", this.totalRemainingPayment)


	while(this.totalRemainingPayment > 0){
		if(!this.installments[type].hasOwnProperty(i)){
			var payableEle = $("#"+type+" #installment_content_"+i+" #payable_amount")
			var payableDateEle = $("#"+type+" #installment_content_"+i+" #payment_date")
			this.installments[type][i] = {}
			if(args && args.events.paymentChanged && args.events.target[0] == payableEle[0]){
				this.installments[type][i].payable = parseFloat(payableEle.val());
			}
			else{
				this.installments[type][i].payable = this.totalRemainingPayment
			}
			var date = new Date(this.installments[type][i-1].date)
			if(payableDateEle.length > 0){
				this.installments[type][i].date = new Date(payableDateEle.val());
			}
			else{
				this.installments[type][i].date = new Date(date.setDate(date.getDate() + 1));
			}
			this.totalRemainingPayment -= this.installments[type][i].payable
			this.calcExtraFees(this.installments[type][i].date, i, type)
			this.installments[type][i].remain = this.totalRemainingPayment
		}
		if(i!=1) console.log("installment :", i, "Payable:", this.installments[type][i].payable, "remain :", this.totalRemainingPayment)
		i++;
	}
}

Payment.prototype.generateInstallment = function (type, args){
	var date = new Date(this.installments[type][args.from-1].date)
	this.installments[type][args.from] = {};
	this.installments[type][args.from].payable = this.totalRemainingPayment;
	this.installments[type][args.from].date = new Date(date.setDate(date.getDate() + 1));
	this.totalRemainingPayment -= this.installments[type][args.from].payable;
	this.calcExtraFees(this.installments[type][args.from].date, args.from, type)
	this.installments[type][args.from].remain = this.totalRemainingPayment
};

Payment.prototype.calcExtraFees = function (date, installmentcount, type){

	var currentInstallment = this.installments[type][installmentcount]
	var dateDiffInDay = getDateDiff(date, new Date())
	console.log("date diff : ", dateDiffInDay)

	var currentPlanFlag = false;
	switch(this.paymentType.toLowerCase()){
		case "full payment":
			this.paymentPlans.fullPayment.some(function(plan){
				if(plan.pay_within_days >= dateDiffInDay){
					currentInstallment.currentPlan = plan;
					currentPlanFlag = true;
					return true;
				}
			})
			if(!currentPlanFlag) currentInstallment.currentPlan = this.paymentPlans.fullPayment[this.paymentPlans.fullPayment.length - 1];
			break;
		case "part payment":
			this.paymentPlans.partPayment.some(function(plan){
				if(plan.pay_within_days >= dateDiffInDay){
					currentInstallment.currentPlan = plan;
					currentPlanFlag = true;
					return true;
				}
			})
			if(!currentPlanFlag) currentInstallment.currentPlan = this.paymentPlans.partPayment[this.paymentPlans.partPayment.length - 1];
			break;
	}(this)
	currentInstallment.paymentPlanId = currentInstallment.currentPlan.id
	var value;
	var extra_fee = [];
	var total_value = [];
	currentInstallment.total = currentInstallment.payable;

	$.each(currentInstallment.currentPlan.payment_fees, function(k,v){
		value = currentInstallment.payable * v / 100;
		currentInstallment.total += parseFloat(value);
		extra_fee.push({title : k +"("+ v +"%)",value : value})
		currentInstallment[k] = value
	})
	total_value.push({total_title: "Total amount to be paid :", total_value: currentInstallment.total})
	this.installments[type][installmentcount] = currentInstallment
	this.installments[type][installmentcount].extraFeeTemplateValue = extra_fee
	this.installments[type][installmentcount].totalTemplateValue = total_value

};

Payment.prototype.updatePaymentForm = function (type){
	$("#"+type+" #new_installment").html('')
	for (var count in this.installments[type]) {
		if($("#"+type+" #installment_content_"+count).length > 0){
			var PaymentPlanIdEle = $("#"+type+" #installment_content_"+count+" #payment_plan_id");
			var extraFeesEle = $("#"+type+" #installment_content_"+count+" #extra_fees");
		}
		else{
			$("#"+type+" #installment_content_2").clone()
				.prop("id", "installment_content_"+count).appendTo('#new_installment');
			var PaymentPlanIdEle = $("#"+type+" #installment_content_"+count+" #payment_plan_id");
			var extraFeesEle = $("#"+type+" #installment_content_"+count+" #extra_fees");
		}
		var payableEle = $("#"+type+" #installment_content_"+count+" #payable_amount")
		var payableDateEle = $("#"+type+" #installment_content_"+count+" #payment_date")
		payableEle.val(this.installments[type][count].payable)
		payableDateEle.val(formetedDate(this.installments[type][count].date))

		extraFeesEle.loadTemplate($("#extra_fee_template"),this.installments[type][count].extraFeeTemplateValue)
		extraFeesEle.loadTemplate($("#total_value_template"),this.installments[type][count].totalTemplateValue, {append: true})
		PaymentPlanIdEle.val(this.installments[type][count].paymentPlanId)
	}
}

// Payment.prototype.remaining_payment = function (){
// 	var total = 0;
// 	$.each(this.installments, function(key, value){
// 		total += value.payable
// 	})
// 	return (this.op - total)
// };

// Payment.prototype.next_installment_count = function(){
// 	return Object.keys(this.installments).length + 1
// };

// Payment.prototype.is_remain = function(){
// 	return this.remaining_payment() > 0
// };

// Payment.prototype.dateChange = function(){
// 	// console.log(this)
// };

formetedDate = function(date){
	date = new Date(date)
	day = date.getDate(date)
	month = date.getMonth() + 1
	year = date.getFullYear()
	return year+"-"+pad(month)+"-"+pad(day)
}

pad = function(n){
	return n<10 ? '0'+n : n
}

getDateDiff = function(date1, date2){
	var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
	var firstDate = new Date(date1);
	var secondDate = new Date(date2);

	return diffDays = Math.ceil((firstDate.getTime() - secondDate.getTime())/(oneDay));
}

// function get_remaining_amount_form(payments){
// 	$("#installment_content").append(generate_ele("div", {id: "installment_content_"+Object.keys(payments.installment).length}))
// 	// $("#content").clone().appendTo("#installment_content_"+Object.keys(payments.installment).length)

// }
