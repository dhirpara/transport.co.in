$(document).on('turbolinks:load', function() {
  if(typeof google != 'undefined' && document.getElementById('from_address')){
    // initMap()
  }

  if($("#edit_load")){
    initAutocomplete()
  }

  if($("#vehicle_list")){
    $("#vehicle_list").on("change", function(){
      $.get("track_details", { trip: $(this).val() })
    })
  }
})

function initAutocomplete(){
  if($("#load_loading_address").length > 0){
    var input = (document.getElementById('load_loading_address'));
  }
  else{
    var input = (document.getElementById('load_destination_address'));
  }

  console.log(input)

  var options = {
      componentRestrictions: {country: "in"}
    };
  if(input){
    google.maps.event.addDomListener(input, 'keydown', function(e) {
      if (e.keyCode == 13) {
          e.preventDefault();
      }
    })

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        window.alert("Autocomplete's returned place contains no geometry");
        return;
      }
    })
  }
}

function initMap() {
  var from_addr = (document.getElementById('from_address'));
  var dest_addr = (document.getElementById('to_address'));
  var directionsService = new google.maps.DirectionsService;
  var directionsDisplay = new google.maps.DirectionsRenderer;

  map = new google.maps.Map(document.getElementById('map'), {
  mapTypeControl: false,
  zoom: 13
  });

  directionsDisplay.setMap(map);
  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    map: map,
    anchorPoint: new google.maps.Point(0, -29)
  });
    if(from_addr && dest_addr){
      [from_addr, dest_addr].forEach(function(addr, i){
        google.maps.event.addDomListener(addr, 'keydown', function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
            }
          })
      })
      marker.setVisible(false);
      loadOriginLong($(from_addr).html(), function(id){
        origin_place_id = id.place_id
        loadDestinationLong($(dest_addr).html(), function(id){
          destination_place_id = id.place_id
          route(origin_place_id,destination_place_id,directionsService, directionsDisplay,marker,infowindow,from_addr,dest_addr, function(){})
        })
      })
    }
}

function loadOriginLong(originAdd, callback){
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': originAdd}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK){
      callback(results[0])
    }
  });
}

function loadDestinationLong(destinationAdd, callback){
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': destinationAdd}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK){
      callback(results[0])
    }
  });
}

function route(origin_place_id, destination_place_id,directionsService, directionsDisplay,marker,infowindow,origin_addr,destination_addr, callback) {
  if (!origin_place_id || !destination_place_id) {
    return;
  }
  travel_mode = google.maps.TravelMode.DRIVING;

  directionsService.route({
    origin: {'placeId': origin_place_id},
    destination: {'placeId': destination_place_id},
    travelMode: travel_mode
  }, function(response, status) {
    if (status === google.maps.DirectionsStatus.OK) {
      if(marker){
        infowindow.close();
        marker.setVisible(false);
      }
      directionsDisplay.setDirections(response);
      var distanceInput = document.getElementById("distanceKm");
      distanceInput = response.routes[0].legs[0].distance.value / 1000;
      callback(distanceInput)
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

function trackVehicleList() {
    document.getElementById("myDropdown").classList.toggle("show");
}