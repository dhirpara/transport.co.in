class TransportersController < ApplicationController
  before_action :authenticate_transporter!, except: [:send_otp, :otp_authentication, :verify_phone, :resend_send_otp]
	before_action :set_transporter, except: [:dashboard, :web_notifications]
  before_action :check_otp_authentication, only: [:send_otp, :otp_authentication, :verify_phone, :resend_send_otp]
  def dashboard
  end

  def show
		@bank_details = current_transporter.bank_account_details
		if request.path != transporter_path(current_transporter)
      return redirect_to current_transporter, :status => :moved_permanently
    end
    @resource_name = "transporter"
  end

  def change_password
  end

  def update_password
    if @transporter.update_with_password(transporter_params)
      bypass_sign_in @transporter
      redirect_to authenticated_transporter_root_path, notice: "Successfully updated password"
    else
    	flash.now[:alert] = @transporter.errors.full_messages.join(", ")
      render "change_password"
    end
  end

  def send_otp
    otp_res = generate_otp
    if otp_res.success?
      message = "Your OTP is #{otp_res.data}"
      SmsService::SMS.send(@transporter.ph_no, message)
        .on_success{
          @transporter.set_otp(otp_res.data)
          flash[:notice] = t("otp_service.send_otp")
        }
        .on_error{
          flash[:notice] = "SMS server problem."
        }
    else
      flash[:alert] = t("otp_service.errors.generate")
    end
    redirect_to otp_authentication_transporter_path
  end

  def otp_authentication
  end

  def verify_phone
    if @transporter.authenticate_otp(params[:otp_code])
      @transporter.phone_verified!
      redirect_to new_transporter_session_path, notice: t("authenticaction.otp.verified")
    else
      flash.now[:alert] = "OTP is wrong. Re-enter or regenerate again."
      render 'otp_authentication'
    end
  end

  def web_notifications
    @web_notifications = current_transporter.web_notifications.sort_by(&:created_at).reverse
  end

  private

  def transporter_params
  	params.require(:transporter).permit(:current_password, :password, :password_confirmation)
  end

  def set_transporter
  	@transporter = Transporter.friendly.find(params[:id])
  end

  def check_otp_authentication
    if @transporter.ph_no_verified?
      redirect_to new_transporter_session_path, notice: t("authenticaction.otp.allready_verified")
    end
  end

  def generate_otp
    OtpService::Otp.new(unique_for: :transporter, unique_attr: :otp_secret_code).generate
  end

end
