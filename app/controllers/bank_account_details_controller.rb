class BankAccountDetailsController < ApplicationController
	before_action :set_resource
	before_action :set_bank_account_detail, only: [:edit, :update, :destroy]

	def index
		@bank_details = @resource.bank_account_details
	end

	def new
		@bank_detail = @resource.bank_account_details.new
	end

	def create
		@bank_detail = @resource.bank_account_details.new(bank_detail_params)
		if @bank_detail.save
			@bank_details = @resource.bank_account_details
			flash[:success] = "Bank detail created successfully!"
			# redirect_to send("#{@resource_name}_path", @resource), notice: "Bank detail created successfully!"
		else
			flash[:danger] = "Bank detail not save because of following error : #{@bank_detail.errors.full_messages.join(', ')}"
		end
	end

	def edit
	end

	def update
		if @bank_detail.update_attributes(bank_detail_params)
			@bank_details = @resource.bank_account_details
			flash[:success] = "Bank detail update successfully!"
			# redirect_to send("#{@resource_name}_path", @resource), notice: "Bank detail update successfully!"
		else
			flash[:danger] = "Bank detail not update successfully because of following error : #{@bank_detail.errors.full_messages.join(', ')}"
			# render "edit"
		end
	end

	def destroy
		if @bank_detail.destroy
	    flash[:notice] = "Your bank account deleted successfully!"
		else
	    flash[:alert] = "Your bank account not deleted because of : #{@bank_detail.errors.full_errors.join(', ')}"
		end
			redirect_to :back
	end

	private

	def set_resource
		logger.info"====current_resource=====#{current_resource.inspect}========="
		case current_resource.class.name
		when "Customer"
			@resource = Customer.friendly.find(params[:customer_id])
		when "Transporter"
			@resource = Transporter.friendly.find(params[:transporter_id])
		else
			raise Exceptions::ResourceNotFound
		end
		@resource_name = @resource.class.name.downcase
		logger.info"====resource=====#{@resource.inspect}========="
	end

	def set_bank_account_detail
		@bank_detail = @resource.bank_account_details.friendly.find(params[:id])
	end

	def bank_detail_params
		params.require(:bank_account_detail).permit(:bank_name, :branch_name, :account_holder_name, :account_number, :ifsc_code)
	end
end
