class Customers::SessionsController < Devise::SessionsController
# before_action :configure_permitted_parameters, only: [:create]

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    customer = Customer.find_by_login(params[:customer][:login])
    if customer.nil? || customer.ph_no_verified?
      self.resource = warden.authenticate!(auth_options)
      set_flash_message!(:notice, :signed_in)
      sign_in(resource_name, resource)
      yield resource if block_given?
      respond_with resource, location: after_sign_in_path_for(resource)
    else
      redirect_to send_otp_customer_path(customer), method: :post
    end
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  protected

  def after_sign_in_path_for(resource)
    if params[:from].present? && params[:to].present?
      @load = current_customer.loads.new
      @load.save!(validate: false)
      customers_loads_build_path(:load_detail, load_id: @load.id, from: params[:from], to: params[:to])
    else
      super
    end
  end

  # def auth_options
  #   { scope: resource_name, recall: "#{controller_path}#new" }
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
