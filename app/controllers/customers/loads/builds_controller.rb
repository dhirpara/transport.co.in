class Customers::Loads::BuildsController < ApplicationController
	before_action :authenticate_customer!
	include Wicked::Wizard
	before_action :set_load, only: [:show, :update]
  before_action :authenticate_step, only: [:show]
	steps :load_detail, :vehicle_detail, :other_detail, :confirm_detail

	def create
    @load = current_customer.loads.new
		@load.save!(validate: false)
    redirect_to wizard_path(:load_detail, load_id: @load.id)
	end

	def show
    # exit
    @vehicle_load_types = VehicleLoadType.all
    @load.finish_process if step == "wicked_finish"
    render_wizard
  end

  def update
    # exit
    if @load.update_attributes(load_params)
      @load.send("#{next_step}!") # Update load step
    	redirect_to wizard_path(next_step, load_id: @load.id)
    else
      @vehicle_load_types = VehicleLoadType.all
    	render_wizard @load
    end
  end

  private

  def load_params
  	params.require(:load).permit(:load_type_id, :vehicle_load_type_id, :vehicle_type_id, :vehicle_quantity, :loading_date, :strict_date, :comment, :from_address, :to_address, :from_coordinate, :to_coordinate, :trip_distance, :terms_of_service, weight: [:value, :unit], volume: [:value, :unit], dimension: [:length, :height, :dept, :unit]).merge(form_step: step)
  end

  def set_load
    @load ||= Load.friendly.find(params[:load_id])
  end

  def authenticate_step
    if @load.drafted? && !authenticated_step?
      authenticated_step_path
    end
  end

  def authenticated_step?
    return true if current_step_index.nil?
    current_step_index > @load.step_status_before_type_cast ? false : true
  end

  def authenticated_step_path
    redirect_to wizard_path(@load.step_status.to_sym, load_id: @load.id)
  end
end
