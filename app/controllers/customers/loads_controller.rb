class Customers::LoadsController < ApplicationController
  before_action :authenticate_customer!
	before_action :set_load, except: [:index]
	before_action :set_quote, only: [:accept_quote]
	before_action :set_quotes, only: [:show]

	def index
		@loads = current_customer.accessible_loads(params[:status])
		@new_load = current_customer.loads.new
	end

	def show
	end

	def order
	end

	def get_address_form
	end

	def set_address
		if @load.update_attributes(address_params)
			redirect_to authenticated_customer_root_path(status: "active"), notice: "Address added."
		else
			render 'get_address_form'
		end
	end

	def cancel
		if @load.canceled_by(:customer)
			flash[:success] = "Load canceled successfully!"
		else
			flash[:danger] = "Load not canceled due to #{@load.errors.full_messages.split(",").join(", ")}"
		end
		redirect_to :back
	end

	def accept_quote
		redirect_to new_customers_payment_path(@quote)
	end


	# def confirmed_load
	# end

	# def in_transit
	# 	return redirect_to confirmed_load_customers_load_path(@load) unless @load.in_transit?
	# 	render 'confirmed_load'
	# end

	def track
		@trips = @load.trips
	end

	def track_details
		@trip = Trip.find(params[:trip])
	end

	def quotes
		load_id = params[:id].upcase
		@load =  Load.find_by(load_docket_id: load_id)
		@quotes = @load.submitted_quotes
		respond_to :js
	end

	def cancel_load
		load_id = params[:id].upcase
		@load =  Load.find_by(load_docket_id: load_id)
		respond_to :js
	end

	def dispatch_order
		return redirect_to :back, notice: "Vehicle not assign." if @load.assign_vehicles.blank?
		@load.confirmed_order.dispatch
		redirect_to order_customers_load_path(@load), notice: "Order dispatched."
	end

	def marke_as_reached
		@load.confirmed_order.reached
		redirect_to authenticated_customer_root_path(status: "processed"), notice: "Your order is reached at your destination."
	end

	def load_detail
		load_id = params[:id].upcase
		@load =  Load.find_by(load_docket_id: load_id)
		respond_to :js
	end

	def track_shipment
		load_id = params[:id].upcase
		@load =  Load.find_by(load_docket_id: load_id)
		@trips = @load.trips
		respond_to :js
	end

	def find_address
		load_id = params[:id].upcase
		@load =  Load.find_by(load_docket_id: load_id)
		respond_to :js
	end
  private

  def load_params
  	params.require(:load).permit(:load_type_id, :vehicle_load_type_id, :vehicle_type_id, :vehicle_quantity, :loading_date, :strict_date, :comment, :from_address, :to_address, :trip_distance, :terms_of_service, weight: [:value, :unit], volume: [:value, :unit], dimension: [:length, :height, :dept, :unit]).merge(form_step: step)
  end

  def address_params
  	params.require(:load).permit(:loading_address, :destination_address)
  end

  def set_load
    @load ||= Load.includes(:accepted_quote, trips: :vehicle).friendly.find(params[:id])
  end

	def set_quote
 		@quote ||= Quote.friendly.find(params[:quote_id])
	end

 	def set_quotes
 		@quotes ||= @load.submitted_quotes.includes(:transporter)
	end

end
