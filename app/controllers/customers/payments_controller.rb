class Customers::PaymentsController < ApplicationController
  before_action :authenticate_customer!
  before_action :get_quote, only: [:new, :get_form, :create]

	def new
		return redirect_to authenticated_customer_root_path if @load.paid?
		@load = @quote.load
		@payment = @quote.customers_payments.new
	end

	def get_form
		logger.info"================#{@quote.inspect}============="
		@payment = @quote.customers_payments.new
		respond_to do |format|
			format.js
		end
	end

	def create
		success = begin
			CustomersPayment.transaction do
				params[:customers_payment].each do |customers_payment|
		 			@payment = @quote.customers_payments.new(payment_params(customers_payment.merge(customer_id: current_customer.id)))
					@payment.save!
				end
			end
			true
		rescue => e
			false
		end
		logger.info"=============================#{success}=========="
		if success
			@load.create_order_with(@quote)
			flash[:success] = "Payment success!"
			redirect_to authenticated_customer_root_path
		else
			flash.now[:error] = "Payment not success."
			render 'new'
		end

	end

	def get_all_plans
		render json: { paymentPlans: { fullPayment: PaymentPlan.send("Full Payment").active.as_json(except: [:created_at, :updated_at, :display_active]), partPayment: PaymentPlan.send("Part Payment").active.as_json(except: [:created_at, :updated_at, :display_active]) } }
	end

	private

	def get_quote
		@quote ||= Quote.friendly.find(params[:quote_id])
		@load ||= @quote.load
	end

	def payment_params(customers_payment)
		customers_payment.permit(:quote_id, :payment_plan_id, :amount, :paying_date, :customer_id)
	end

end
