class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  layout :set_layout
  before_action :authenticate_resource!
  before_action :configure_permitted_parameters, if: :devise_controller?
  rescue_from ActiveRecord::RecordNotFound, with: :known_error

  include ApplicationHelper

  private

  def authenticate_resource!
    # if current_admin || controller_name == "admin"
    #   authenticate_admin!
    # elsif current_customer
    #   authenticate_customer!
    # end
  end

  def set_layout
  	if admin_signed_in? || !request.path["admin"].nil?
  		"admin"
  	end
  end

  def known_error(exception)
    flash[:danger] = "Record not found."
    redirect_to "/"
  end


  
  protected

  def configure_permitted_parameters
    added_attrs = [:ph_no, :email, :password, :password_confirmation, :remember_me]
    added_update_attrs = [:ph_no, :email, :name, :company_name, :address_detail]
    devise_parameter_sanitizer.permit(:sign_up, keys: added_attrs)
    devise_parameter_sanitizer.permit :account_update, keys: added_update_attrs
  end

end
