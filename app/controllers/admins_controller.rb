class AdminsController < ApplicationController
  before_action :authenticate_admin!
  def dashboard

  end

  def index
  	@sub_admins = SubAdmin.all
    @sub_admin = SubAdmin.new
  end

  def new
  	@sub_admin = SubAdmin.new
  end

  def create
  	@sub_admin = SubAdmin.new(sub_admin_params)
  	if @sub_admin.save
  		redirect_to sub_admins_path, notice: "Sub Admin created successfully!"
  	else
      @sub_admins = SubAdmin.all
  		render 'index'
    end
  end

  def get_notifyable_transporter
    load = Load.find(params[:load_id])
    routebox_history = RouteboxerHistory.where("load_origin_point ~= point (?) AND load_destination_point ~= point (?)", load.from_coordinate, load.to_coordinate)
    transporterRoutes = { fromRouteBoxerHistroy: routebox_history.map { |rh| rh.transporter_id } }
    logger.info"======================#{transporterRoutes}======================"
    rh_last_record = RouteboxerHistory.all.sort_by(&:created_at).last
    rh_last_record.nil? ?
        transporterRoutes["needToApplyRouteboxer"] = TransporterRouter.includes(:source_city, :destination_city).collect { |route| {transRouteId: route.id, origin: route.source_city.coordinate, destination: route.destination_city.coordinate} }
        : transporterRoutes["needToApplyRouteboxer"] = TransporterRouter.where('updated_at >= (?)', rh_last_record.created_at).includes(:source_city, :destination_city).collect { |route| {transRouteId: route.id, origin: route.source_city.coordinate, destination: route.destination_city.coordinate} }
    logger.info"======================#{transporterRoutes}======================"
    render json: transporterRoutes
    # exit
  end

  # def all_transporter_routes
  #   render json: TransporterRouter.includes(:source_city, :destination_city).all.collect {|route| {transRouteId: route.id, origin: route.source_city.coordinate, destination: route.destination_city.coordinate} }
  # end

  def send_notification_for_new_load
    transporters = []
    logger.info "===========================#{params[:transporter_routes_ids].inspect}=================================="
    if params[:transporter_routes_ids].present?
      transporters = TransporterRouter.includes(:transporter, :source_city, :destination_city).find(params[:transporter_routes_ids]).map do |tr|
        RouteboxerHistory.create!(
          transporter_id: tr.transporter_id,
          load_origin_point: params[:load_origin_coordinate],
          load_destination_point: params[:load_destination_coordinate],
          matched_origin_point: tr.source_city.coordinate,
          matched_destination_point: tr.destination_city.coordinate
        )
        tr.transporter
      end.uniq
      transporters = TransporterRouter.includes(:transporter).find(params[:transporter_routes_ids]).map{|tr| tr.transporter}.uniq
    end
    transporters.concat(Transporter.find(params[:transporter_ids])).uniq! if params[:transporter_ids].present?
    if transporters.present?
      load = Load.find(params[:load_id])
      SendNotificationJob.perform_later(type: load.class.name, action: "matched", notified_via: ["web", "email", "sms"], object: load, send_to: transporters, from_city: params[:load_origin_city], to_city: params[:load_destination_city])
    end
    render json: "success"
  end

  def web_notification_show
    # return redirect_back(fallback_location: "/", notice: "You are not authorize to access ")
    @web_notifications = current_admin.web_notifications.sort_by(&:created_at).reverse
  end

  protected

  def sub_admin_params
  	params.require(:sub_admin).permit(:email, :password)
  end

end