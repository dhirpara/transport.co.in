class Transporters::TrackingController < ApplicationController
  before_action :authenticate_transporter!
	before_action :set_load

	def load_tracking
		@trips = @load.trips
	end

	def track_details
		@trip = Trip.find(params[:trip])
	end

	private

	def set_load
		@load = Load.includes(:trips).friendly.find(params[:id])
	end
	# def set_trip_data
	# 	@trip = Trip.includes(:load, :vehicle, :trip_trackings).find_by(id: params[:id])
	# 	@load = @trip.load
	# 	@vehicle = @trip.vehicle
	# end
end
