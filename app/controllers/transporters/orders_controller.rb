class Transporters::OrdersController < ApplicationController
  before_action :authenticate_transporter!
  before_action :get_order, except: :index
	# before_action :set_load

	def index
	  @orders = current_transporter.accessible_orders(params[:status])
	end

	def show
		respond_to :js
	end

	# def ready_to_dispatch_load
	# end

	def vehicle_list
		respond_to :js
	end

	def assign_vehicle
		@load.assign_vehicles.destroy_all
		@load.assign_vehicles << Vehicle.find(params[:assign_vehicle])
		redirect_to transporters_order_path(@order), notice: "Assign successfully!"
		rescue ActiveRecord::AssociationTypeMismatch, ActiveRecord::RecordNotFound
			flash.now[:notice] = "Please select vehicles."
			render "vehicle_list"
		rescue ActiveRecord::RecordInvalid => e
			flash.now[:notice] = e.message.split(":")[1]
			render "vehicle_list"
	end

	def discard_vehicle
		@load.assign_vehicles.destroy_all
		redirect_to transporters_order_path(@order), notice: "Assignment removed!"
	end

	# def dispatch_order
	# 	return redirect_to transporters_list_vehicle_path(@load), notice: "Assign Vehicle." if @load.assign_vehicles.blank?
	# 	@load.dispatch
	# 	redirect_to transporters_in_transit_path
	# end

	# def in_transit
	# 	return redirect_to transporters_ready_to_dispatch_load_path(@load) if @load.confirmed?
	# 	render 'ready_to_dispatch_load'
	# end

	def cancel_order
		if @load.confirmed_order.cancel_order()
			redirect_to authenticated_transporter_root_path, notice: "Order canceled."
		else
			redirect_to authenticated_transporter_root_path, notice: "Order not canceled."
		end
	end

	def track
		@trips = @load.trips
		respond_to :js
	end

	def track_details
		@trip = Trip.find(params[:trip])
	end

	def payment_info
	end

	def cancel
		@order = Order.find(params[:id])
		respond_to :js
	end

	def order_details
		@order = Order.find(params[:id])
		respond_to :js
	end

	def order_statistic
		@order = Order.find(params[:id])
		@trips = @load.trips
		@trip = @trips.first
		respond_to :js
	end
	private

	def get_order
		@order = Order.find(params[:id])
		@load = @order.load
		@quote = @order.quote
		@assign_vehicles = @load.assign_vehicles
	end

	def set_load
		@load ||= Load.includes(:accepted_quote, :assign_vehicles, trips: :vehicle).friendly.find(params[:id])
		@quote ||= @load.accepted_quote
	end

end
