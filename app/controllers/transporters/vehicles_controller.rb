class Transporters::VehiclesController < ApplicationController
  before_action :authenticate_transporter!
	before_action :set_resource
	before_action :set_vehicle, only: [:edit, :update, :destroy]
	before_action :set_vehicles, only: [:index, :create, :update]

	def index
		@vehicle = @resource.vehicles.new
	end

	# def new
	# 	@vehicle = @resource.vehicles.new
	# end

	def create
		@vehicle = @resource.vehicles.new(vehicle_params)
		if @vehicle.save
			redirect_to  transporters_vehicles_path, notice: "vehicle created successfully!"
		else
			render 'index'
		end
	end

	def edit
	end

	def update
    	flash[:success] = "Vehicle details updated successfully!" if @vehicle.update_attributes(vehicle_params)
		# if @vehicle.update_attributes(vehicle_params)
		# 	redirect_to  transporters_vehicles_path, notice: "vehicle update successfully!"
		# else
		# 	flash[:alert] = "Vehicle not update successfully because of following error : #{@vehicle.errors.full_errors.join(', ')}"
		# 	render "edit"
		# end
	end

	def destroy
		if @vehicle.destroy
	    flash[:notice] = "Your Vehicle details deleted successfully!"
		else
	    flash[:alert] = "Vehicle details not deleted because of : #{@vehicle.errors.full_errors.join(', ')}"
		end
		@vehicles = @resource.vehicles
		redirect_to :back
	end

	private

		def set_resource
			@resource = current_resource
		end

		def vehicle_params
			params.require(:vehicle).permit(:transporter_id,:vehicle_type_id,:number_plate,:vehicle_status,:chassis_number,:fitness)
		end

	 	def set_vehicle
	 		begin
	 			@vehicle = @resource.vehicles.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Vehicle is not found"
	 		end
	 	end

	 	def set_vehicles
	 		@vehicles = @resource.vehicles.all
	 	end
end
