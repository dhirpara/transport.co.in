class Transporters::TransporterRoutersController < ApplicationController
  before_action :authenticate_transporter!
  before_action :set_tranporter_route, only: [:edit,:update,:show,:destroy]
  before_action :set_cities, only: [:index, :create, :edit]
  before_action :set_transporter_routes, only: [:index, :create, :update]

  def index
    @transporter_router = current_transporter.transporter_routers.new
  end

  def create
    @transporter_router = current_transporter.transporter_routers.new(transporter_router_params)
    if @transporter_router.save
      redirect_to transporters_transporter_routers_path,notice: "Route created successfully!"
    else
      render 'index'
    end
  end

  def edit
  end

  def update
    flash[:success] = "Route updated successfully!" if @transporter_route.update_attributes(transporter_router_params)
  end

  def destroy
  	@transporter_route.destroy
  	redirect_to transporters_transporter_routers_path,notice: "Route deleted successfully!"
  end


private

	def set_tranporter_route
		@transporter_route = TransporterRouter.find(params[:id])
	end

	def transporter_router_params
		params.require(:transporter_router).permit(:source_city_id,:destination_city_id)
	end

  def set_cities
    @cities = City.all
  end

  def set_transporter_routes
    @transporter_routes = current_transporter.transporter_routers.all
  end
end
