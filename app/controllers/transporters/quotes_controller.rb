class Transporters::QuotesController < ApplicationController
  before_action :authenticate_transporter!
  before_action :get_quote, only: [:show, :draft, :edit, :update, :destroy]
  before_action :get_load, only: [:show, :edit, :draft]

  def index
  	@loads = Load.includes(:load_type, :vehicle_load_type, :vehicle_type, :quotes, :transporters).posted
    # if ["active", "processed"].include?(params[:status])
	   #  @orders = current_transporter.confirmed_loads(params[:status])
    # else
	   #  @loads = current_transporter.accessible_loads
    # end
  	@quotes = current_transporter.quotes
  end

  def new
    @load = Load.friendly.find(params[:load_id])
    @vehicle_load_types = VehicleLoadType.all
		@quote = current_transporter.quotes.new
    respond_to :js
  end

  def draft
  	# render 'new'
    @vehicle_load_types = VehicleLoadType.all
    respond_to :js
  end

  def create
    exit
    @quote = current_transporter.quotes.new(quote_params)
    if @quote.save(validate: !params[:draft].present?)
      flash[:success] = t("transporter.quote.save", action: params[:commit].present? ? "created" : "drafted" )
      # @quote.send("#{params[:commit].present? ? 'submitted' : 'drafted'}!")
      @quote.update_attribute(:status, params[:commit].present? ? :submitted : :drafted )
      redirect_to transporters_quotes_path
    # else
    #   render "new"
    end
  end

  def show
    respond_to :js
  end

  def edit
     @vehicle_load_types = VehicleLoadType.all
     respond_to :js
  end

  def update
  	if @quote.update_attributes(quote_params)
  		flash[:success] = t("transporter.quote.save", action: params[:commit].present? ? "updated" : "drafted" )
  		@quote.send("#{params[:commit].present? ? 'submitted' : 'drafted'}!")
  		redirect_to transporters_quotes_path
  	end
  end

  def destroy
    exit
  	if @quote.destroy
  		flash[:success] = t("transporter.quote.soft_delete")
  		redirect_to transporters_quotes_path
  	end
  end

  def add_to_favorite
    load = Load.includes(:favorite_loads).friendly.find(params[:id])
    favorite_load = load.favorite_loads.select{ |load| load.transporter_id == current_transporter.id }[0]
    if favorite_load.nil?
      load.transporters << current_transporter
    else
      favorite_load.destroy
    end
    # render json: :success
    redirect_back(fallback_location: "/")
  end

  private

  def get_quote
  	@quote ||= Quote.includes(:load).friendly.find(params[:id])
  end

  def get_load
  	@load ||= @quote.load
  end

  def quote_params
  	params.require(:quote).permit(:load_id, :offered_date, :offered_vehicle_load_type_id, :offered_vehicle_type_id, :offered_vehicle_quantity, :offered_price)
  end

end
