class LandingController < ApplicationController

	def transport_landing
	end

	def authenticate_customer_for_account_existence
		@customer = Customer.find_by_ph_no(params[:phone])
		if @customer
			redirect_to new_customer_session_path(from: params[:from_address], to: params[:to_address])
		else
			redirect_to new_customer_registration_path
		end
	end
end
