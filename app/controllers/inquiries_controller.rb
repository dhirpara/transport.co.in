class InquiriesController < ApplicationController

	def create
		@inquiry = Inquiry.new(inquiry_params)
		if @inquiry.save
			redirect_to root_path, notice: "Admin will contact you soon."
		end
	end

	private

	def inquiry_params
		params.require(:inquiry).permit(:email, :phone_number, :message)
	end

end
