class Admins::CitiesController < ApplicationController
	before_action :set_city, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:new, :create, :index]
  before_action :check_coordinate, only: [:update]
  def index
    @cities = City.all
    @city = City.new
  end

  def new
    @city = City.new
  end

  def create
    @city = City.new(city_params)
    if @city.save
      redirect_to admins_cities_path, notice: "City created successfully!"
    else
      @cities = City.all
      render 'index'
    end
  end

  def edit
  end

  def update
    if @city.update_attributes(city_params)
      redirect_to admins_cities_path, notice: "City updated successfully!"
    else
      @cities = City.all
      render 'index'
    end
  end

  def destroy
    @city.destroy
    @cities = City.all
    redirect_to admins_cities_path, notice: "City deleted successfully!"
  end

	private

    def city_params
			params.require(:city).permit(:name,:short_name,:coordinate)
		end

    def set_city
      begin
        @city = City.friendly.find(params[:id])
      rescue Exception => e
        redirect_to :back, notice: "City is not found"
      end
    end

   def check_coordinate
    unless params[:city][:coordinate].include? ","
      params[:city][:coordinate] = params[:city][:coordinate].gsub(' ', ",")
    end
   end
end