class Admins::TransportersController < ApplicationController
	before_action :set_transporter, only: [:show, :edit, :update, :destroy, :approve_transporter]
	before_action :authenticate_admin!
	
	def index
		@transporters = params[:approved].nil? ? Transporter.find_by_account_status : Transporter.find_by_account_status(params[:approved])
	end

	def new
		@transporter = Transporter.new
		@bank_account_detail = @transporter.bank_account_details.build
	end

	def create
		@transporter = Transporter.new(transporter_params)
		@transporter[:approved] = true
		@transporter[:ph_no_verified] = true
		if @transporter.save
			redirect_to admins_transporters_path,notice: "Transporter created successfully!"
		else
			render 'new'
		end
	end

	def edit
	end

	def update
		if @transporter.update_attributes(transporter_params)
			redirect_to admins_transporters_path, notice: "Transporter updated successfully!"
		else
			render 'edit'		
		end	
	end

	def show
	end

	def destroy
		@transporter.destroy
		redirect_to admins_transporters_path, notice: "Transporter deleted successfully!"
	end

	def approve_transporter
	    @transporter.approve_resource
	    redirect_to :back
  	end

	private
	 	def set_transporter
	 		begin
	 			@transporter = Transporter.friendly.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "transporter is not found"
	 		end
	 	end

	 	def transporter_params
			params.require(:transporter).permit(:email, :ph_no_verified, :approved ,:password,:password_confirmation,:name,:ph_no,:company_name,:address_detail,bank_account_details_attributes: [:id,:bank_name,:branch_name,:account_holder_name,:account_number,:ifsc_code,:_destroy])
		end
end