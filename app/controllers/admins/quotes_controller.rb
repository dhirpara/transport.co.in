class Admins::QuotesController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_load, only: [:accept_quote]
  before_action :set_quote, only: [:show, :edit,:update,:cancel_quote,:accept_quote]

	def index
		# @quotes =  Quote.includes(:transporter,:vehicle_load_type,:vehicle_type,load: [:customer,:load_type,:vehicle_type]).all
		@quotes =  Quote.includes(:transporter,:vehicle_load_type,:vehicle_type,load: [:customer,:load_type,:vehicle_type]).quotes_by_status(params[:status])
		# @accepted_quote = @load.quotes.where(status: "accepted" ).count
	end

	def show
		@load = @quote.load
	end

	def edit
	end

	def update
		if @quote.update_attributes(quote_params)
			redirect_to admins_quotes_path, notice: "Quote updated."
		else
			render 'edit'
  	end
	end

	def quotes_details
		@quotes = Quote.all
	end

	def edit_quote
		@quote = Quote.friendly.find(params[:id])
	end

	def update_quote
		@quote = Quote.friendly.find(params[:id])
		@quote.update_attributes(quote_params)
		redirect_to admins_quotes_quotes_details_path,notice: "Quote updated"
	end

	def cancel_quote
		@quote.destroy
		redirect_to admins_quotes_path, notice: "Quote canceled."
	end

	def accept_quote
		@quote.accepted!
		@load.confirmed!
		redirect_to see_quotes_admins_load_path(@load.id), notice: "Quote accepted."
	end

	private

	  def set_load
	  	@load ||= Load.friendly.find(params[:load_id])
	  end

	  def set_quote
	  	 @quote ||= Quote.friendly.find(params[:id])
	  end
	  
	  def quote_params
	  	params.require(:quote).permit(:offered_date, :offered_vehicle_load_type_id, :offered_vehicle_type_id, :offered_vehicle_quantity, :offered_price)
	  end
end
