class Admins::TrackersController < AdminsController
	before_action :set_load, except: [:show]
	before_action :set_trip, only: [:new, :create]

	def index
		@trips = @load.trips.includes(:vehicle)
	end

	def new
		@trip_tracking = @trip.trip_trackings.new
	end

	def create
		return redirect_to new_admins_load_tracker_path(@load, trip: @trip.id), notice: "Place not find." unless params[:trip_tracking][:coordinate].present?
		@trip_tracking = @trip.trip_trackings.new(trip_tracking_params)
		if @trip_tracking.save
			redirect_to admins_load_trackers_path(@load)
		else
			render 'new'
		end
	end

	def show
		@trip = Trip.find(params[:id])
		@load = @trip.load
	end

	private

	def set_load
		@load = Load.includes(:vehicle_load_type, :vehicle_type).friendly.find(params[:load_id])
	end

	def set_trip
		@trip = Trip.find(params[:trip])
	end

	def trip_tracking_params
		params.require(:trip_tracking).permit!
	end

end
