class Admins::DimensionUnitsController < ApplicationController
	before_action :set_dimension_unit, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_admin!
	def index
		@dimension_units = DimensionUnit.all
		@dimension_unit = DimensionUnit.new
	end

	def new
		@dimension_unit = DimensionUnit.new
	end

	def create
		@dimension_unit = DimensionUnit.new(dimention_params)
		if @dimension_unit.save
  		redirect_to admins_dimension_units_path, notice: "Dimension unit created successfully!"
  	else
  		@dimension_units = DimensionUnit.all
  		render 'index'
  	end
	end

	def edit
	end

	def update
		if @dimension_unit.update_attributes(dimention_params)
			redirect_to admins_dimension_units_path, notice: "Dimension unit updated successfully!"
		else
			@dimension_units = DimensionUnit.all
  		render 'index'
		end
  end

	def destroy
		@dimension_unit.destroy
		@dimension_units = DimensionUnit.all
		redirect_to admins_dimension_units_path, notice: "Dimension unit deleted successfully!"
	end

	private

		def dimention_params
			params.require(:dimension_unit).permit(:name,:short_name)
		end

	 	def set_dimension_unit
	 		begin
	 			@dimension_unit = DimensionUnit.friendly.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Dimension Unit is not found"
	 		end
	 	end

	end
