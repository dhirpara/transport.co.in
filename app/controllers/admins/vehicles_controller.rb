class Admins::VehiclesController < ApplicationController

	before_action :set_transporter, only: [:create_vehicle_from_order]

	def create_vehicle_from_order
		@vehicle = @transporter.vehicles.new(vehicle_params)
		@vehicle.save
	end

	private

	def vehicle_params
		params.require(:vehicle).permit(:vehicle_type_id, :number_plate, :vehicle_status, :chassis_number, :fitness)
	end

	def set_transporter
		@transporter = Transporter.friendly.find(params[:transporter_id])
	end

end
