class Admins::VolumeUnitsController < ApplicationController
	before_action :set_volume_unit, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_admin!
	def index
		@volume_units = VolumeUnit.all
		@volume_unit = VolumeUnit.new
	end

	def new
		@volume_unit = VolumeUnit.new
	end

	def create
		@volume_unit = VolumeUnit.new(volume_params)
		if @volume_unit.save
  		redirect_to admins_volume_units_path, notice: "Volume unit created successfully!"
  	else
  		@volume_units = VolumeUnit.all
  		render 'index'
  	end
	end

	def edit
	end

	def update
		if @volume_unit.update_attributes(volume_params)
			redirect_to admins_volume_units_path, notice: "Volume unit updated successfully!"
		else
			render 'edit'
		end
  end

	def destroy
		@volume_unit.destroy
		@volume_units = VolumeUnit.all
		redirect_to admins_volume_units_path, notice: "Volume unit deleted successfully!"
	end

	private

		def volume_params
			params.require(:volume_unit).permit(:name,:short_name)
		end

	 	def set_volume_unit
	 		begin
	 			@volume_unit = VolumeUnit.friendly.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Volume Unit is not found"
	 		end
	 	end

	end
