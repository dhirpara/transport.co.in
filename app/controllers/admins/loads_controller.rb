class Admins::LoadsController < AdminsController
	before_action :set_load, except: [:index, :new, :create,:drafted_load]

	def index
		@loads = Load.accessible_loads(params[:load_status])
	end

	def new
		@load = Load.new
	end

	def create
		@load = Load.new(load_params)
		if @load.save
			@load.post_load(:admin)
			flash[:success] = "Load created successfully!"
    	redirect_to admins_loads_path
		else
			render 'new'
		end
	end

	def update
		if @load.update_attributes(load_params)
    	if params[:approved_and_update].present?
    		@load.approved
    		flash[:success] = "Load updated and approved successfully!"
    	else
    		flash[:success] = "Load updated successfully!"
    	end
    	redirect_to admins_loads_path
		else
    	render 'edit'
		end
	end

	def show
		if request.path != admins_load_path(@load)
      return redirect_to [:admins, @load], :status => :moved_permanently
    end
	end

	def approve
		@load.update_attributes(load_params)
		@load.approved
		flash[:success] = "Load approved successfully!"
		redirect_to admins_loads_path
	end

	def cancel
		if @load.canceled_by(:admin)
			flash[:success] = "Load canceled successfully!"
		else
			flash[:danger] = "Load not canceled due to #{@load.errors.full_messages.split(",").join(", ")}"
		end
		redirect_to :back
	end

	def see_quotes
		@load=  Load.includes(quotes: [:transporter, :vehicle_load_type, :vehicle_type]).friendly.find(params[:id])
		@accepted_quote = @load.quotes.where(status: "accepted" ).count
	end

	private

	def set_load
		@load ||= Load.friendly.find(params[:id])
	end

	def load_params
		params.require(:load).permit(:customer_id, :load_type_id, :vehicle_load_type_id, :vehicle_type_id, :vehicle_quantity, :loading_date, :strict_date, :comment, :display_comment, :from_address, :to_address, :trip_distance, :terms_of_service, weight: [:value, :unit], volume: [:value, :unit], dimension: [:length, :height, :dept, :unit])
	end

end
