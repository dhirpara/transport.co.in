class Admins::PaymentPlansController < ApplicationController
	before_action :set_payment_plan, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!
  def index
  	@payment_plans = PaymentPlan.all
    @payment_plan = PaymentPlan.new
  end
  def new
  	@payment_plan = PaymentPlan.new
  end

  def create
    @payment_plan = PaymentPlan.new(payment_plan_params)
  	@extra_fee_field = params[:payment_plan][:payment_fees][:extra_fee_field]
  	@extra_fee_value = params[:payment_plan][:payment_fees][:extra_fee_value]
  	@payment_plan[:payment_fees] = @extra_fee_field.zip(@extra_fee_value).to_h.reject { |k,v| v.nil? || v.empty? }
  	if @payment_plan.save
  		redirect_to admins_payment_plans_path, notice: "Payment plan created successfully!"
  	else
      @payment_plans = PaymentPlan.all
  		render 'index'
  	end
  end

  def destroy
  	@payment_plan.destroy
  	@payment_plans = PaymentPlan.all
    redirect_to admins_payment_plans_path, notice: "Payment plan deleted successfully!"
  end
  def edit
  end

  def update
    @extra_fee_field = params[:payment_plan][:payment_fees][:extra_fee_field]
    @extra_fee_value = params[:payment_plan][:payment_fees][:extra_fee_value]
    params[:payment_plan][:payment_fees] = @extra_fee_field.zip(@extra_fee_value).to_h.reject { |k,v| v.nil? || v.empty? }
  	if @payment_plan.update_attributes(payment_plan_params)
  		redirect_to admins_payment_plans_path, notice: "Payment plan updated successfully!"
  	else
  		render 'edit'
  	end
  end
  private

  def payment_plan_params
  	params.require(:payment_plan).permit(:pay_within_days, :display_active,:payment_type).tap do |whitelisted|
    whitelisted[:payment_fees] = params[:payment_plan][:payment_fees].permit!
  	end
  end
  def set_payment_plan
	 		begin
	 			@payment_plan = PaymentPlan.find_by(id: params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Payment plan is not found"
	 		end
	 	end
 end
