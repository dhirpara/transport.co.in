class Admins::VehicleTypesController < ApplicationController
	before_action :set_vehicle_type, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_admin!
	def index
		@vehicle_types = VehicleType.all
		@vehicle_type = VehicleType.new
	end

	def new
		@vehicle_type = VehicleType.new
	end

	def create
		@vehicle_type = VehicleType.new(vehicle_type_params)
		if @vehicle_type.save
  		redirect_to admins_vehicle_types_path, notice: "Vehicle type created successfully!"
  	else
  		@vehicle_types = VehicleType.all
  		render 'index'
  	end
	end

	def edit
	end

	def update
		if @vehicle_type.update_attributes(vehicle_type_params)
			redirect_to admins_vehicle_types_path, notice: "Vehicle type updated successfully!"
		else
			render 'edit'
		end
  end

	def destroy
		@vehicle_type.destroy
		@vehicle_types = VehicleType.all
		redirect_to admins_vehicle_types_path, notice: "Vehicle type deleted successfully!"
	end

	private

		def vehicle_type_params
			params.require(:vehicle_type).permit(:name)
		end

	 	def set_vehicle_type
	 		begin
	 			@vehicle_type = VehicleType.friendly.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Vehicle Type is not found"
	 		end
	 	end
end
