class Admins::VehicleLoadTypesController < ApplicationController
	before_action :set_vehicle_load_type, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_admin!
	def index
		@vehicle_load_types = VehicleLoadType.all
		@vehicle_load_type = VehicleLoadType.new
	end

	def new
		@vehicle_load_type = VehicleLoadType.new
	end

	def create
		@vehicle_load_type = VehicleLoadType.new(vehicle_load_type_params)
		if @vehicle_load_type.save
  		redirect_to admins_vehicle_load_types_path, notice: "Vehicle load type created successfully!"
  	else
  		@vehicle_load_types = VehicleLoadType.all
  		render 'index'
  	end
	end

	def edit
	end

	def update
		if @vehicle_load_type.update_attributes(vehicle_load_type_params)
			redirect_to admins_vehicle_load_types_path, notice: "Vehicle load type updated successfully!"
		else
			render 'edit'
		end
  end

	def destroy
		@vehicle_load_type.destroy
		@vehicle_load_types = VehicleLoadType.all
		redirect_to admins_vehicle_load_types_path, notice: "Vehicle load type deleted successfully!"
	end

	private

		def vehicle_load_type_params
			params.require(:vehicle_load_type).permit(:name)
		end

	 	def set_vehicle_load_type
	 		begin
	 			@vehicle_load_type = VehicleLoadType.friendly.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Vehicle Load Type is not found"
	 		end
	 	end

	end
