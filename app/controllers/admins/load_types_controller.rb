class Admins::LoadTypesController < ApplicationController
	before_action :set_load_type, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_admin!
	def index
		@load_types = LoadType.all
		@load_type = LoadType.new
	end

	def new
		@load_type = LoadType.new
	end

	def create
		@load_type = LoadType.new(load_type_params)
		if @load_type.save
  		redirect_to admins_load_types_path, notice: "Load type created successfully!"
  	else
  		@load_types = LoadType.all
  		render 'index'
  	end
	end

	def edit
	end

	def update
		if @load_type.update_attributes(load_type_params)
			redirect_to admins_load_types_path, notice: "Load type updated successfully!"
		else
			@load_types = LoadType.all
  		render 'index'
		end
  end

	def destroy
		@load_type.destroy
		@load_types = LoadType.all
		redirect_to admins_load_types_path, notice: "Load type deleted successfully!"
	end

	private

		def load_type_params
			params.require(:load_type).permit(:name,:display_active)
		end

	 	def set_load_type
	 		begin
	 			@load_type = LoadType.friendly.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Load Type is not found"
	 		end
	 	end
end
