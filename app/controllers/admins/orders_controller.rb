class Admins::OrdersController < AdminsController
	before_action :set_order, except: [:active_orders, :processed_orders, :track_order, :new_order_track_detail, :create_order_track_detail]
	before_action :set_trip, only: [:new_order_track_detail, :create_order_track_detail, :track_order]

	def active_orders
		@orders = Order.includes(:load).get_active_by_status(params[:status])
	end

	def processed_orders
		@orders = Order.includes(:load).get_processed_by_status(params[:status])
	end

	def show
		@customer = @load.customer
		@trips = @load.trips.includes(:vehicle)
	end

	def assign_vehicle_form
		@assign_vehicles = @order.load.assign_vehicles
		@vehicle = @transporter.vehicles.new
	end

	def assign_vehicle
		@load.assign_vehicles.destroy_all
		@load.assign_vehicles << Vehicle.find(params[:assign_vehicle])
		flash[:notice] = "Assign successfully!"
		rescue ActiveRecord::AssociationTypeMismatch, ActiveRecord::RecordNotFound => @e
			flash[:notice] = "Please select vehicles."
		rescue ActiveRecord::RecordInvalid => @e
			flash[:notice] = @e.message.split(":")[1]
	end

	def get_address_form
	end

	def set_address
		if @load.update_attributes(address_params)
			redirect_back(fallback_location: "/", notice: "Address added.")
		end
	end

	def cancel
		return redirect_back(fallback_location: "/", notice: "Subject not found.") unless params[:action_as].present?
		if @order.cancel_order(params[:action_as], :admin)
			redirect_back(fallback_location: "/", notice: "Order canceled.")
		else
			redirect_back(fallback_location: "/", notice: "Order not canceled.")
		end
	end

	def new_order_track_detail
		@trip_tracking = @trip.trip_trackings.new
	end

	def create_order_track_detail
		@trip_tracking = @trip.trip_trackings.new(trip_tracking_params)
		@trip_tracking.save
	end

	def track_order
	end

	def dispatch_order
		return redirect_back(fallback_location: "/", notice: "Vehicle not assign.") if @load.assign_vehicles.blank?
		@order.dispatch
		redirect_back(fallback_location: "/", notice: "Order dispatched.")
	end

	def marke_as_reached
		@order.reached
		redirect_back(fallback_location: "/", notice: "Order mark as is reached.")
		# redirect_to authenticated_customer_root_path(status: "processed"), notice: "Your order is reached at your destination."
	end

	private

	def set_order
		@order = Order.includes(transporter: [:vehicles],load: [:customer], quote: [:admin_next_receivable_payment, :customers_payments]).find(params[:id])
		@load = @order.load
		@transporter = @order.transporter
	end

	def set_trip
		@trip = Trip.find(params[:id])
		@load = @trip.load
	end

	def address_params
  	params.require(:load).permit(:loading_address, :destination_address)
  end

  def trip_tracking_params
		params.require(:trip_tracking).permit!
	end

end
