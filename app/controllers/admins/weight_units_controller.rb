class Admins::WeightUnitsController < ApplicationController
	before_action :set_weight_unit, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_admin!
	def index
		@weight_units = WeightUnit.all
		@weight_unit = WeightUnit.new
	end

	def new
		@weight_unit = WeightUnit.new
	end

	def create
		@weight_unit = WeightUnit.new(weight_unit_params)
		if @weight_unit.save
  		redirect_to admins_weight_units_path, notice: "Weight unit created successfully!"
  	else
  		@weight_units = WeightUnit.all
  		render 'index'
  	end
	end

	def edit
	end

	def update
		if @weight_unit.update_attributes(weight_unit_params)
			redirect_to admins_weight_units_path, notice: "Weight unit updated successfully!"
		else
			render 'edit'
		end
  end

	def destroy
		@weight_unit.destroy
		@weight_units = WeightUnit.all
		redirect_to admins_weight_units_path, notice: "Weight unit deleted successfully!"
	end

	private
	 	def set_weight_unit
	 		begin
	 			@weight_unit = WeightUnit.friendly.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Weight Unit is not found"
	 		end
	 	end

	 	def weight_unit_params
			params.require(:weight_unit).permit(:name,:short_name)
		end
end