class Admins::CustomersController < ApplicationController
	before_action :set_customer, except: [:index, :new, :create]
	before_action :authenticate_admin!
	
	def index
		@customers = params[:approved].nil? ? Customer.find_by_account_status : Customer.find_by_account_status(params[:approved])
	end

	def new
		@customer = Customer.new
		bank_account_deatil = @customer.bank_account_details.build
	end

	def create
		@customer = Customer.new(customer_params)
		@customer[:approved] = true
		@customer[:ph_no_verified] = true
		if @customer.save
			redirect_to admins_customers_path,notice: "Customer created successfully!"
		else
			render 'new'
		end
	end

	def edit
	end

	def update
		if @customer.update_attributes(customer_params)
			redirect_to admins_customers_path, notice: "Customer updated successfully!"
		else
			render 'edit'		
		end	
	end

	def show
	end

	def destroy
		@customer.destroy
		redirect_to admins_customers_path, notice: "Customer deleted successfully!"
	end

	def approve_customer
    @customer.approve_resource
    redirect_to :back
  end

  def show_customer
  end

	private
	 	def set_customer
	 		begin
	 			@customer = Customer.friendly.find(params[:id])
	 		rescue Exception => e
	 			redirect_to :back, notice: "Customer is not found"
	 		end
	 	end

	 	def customer_params
			params.require(:customer).permit(:email,:password,:password_confirmation,:name,:ph_no,:company_name,:address_detail,bank_account_details_attributes: [:id,:bank_name,:branch_name,:account_holder_name,:account_number,:ifsc_code,:_destroy])
		end
end