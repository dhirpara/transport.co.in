class Admins::PaymentsController < AdminsController

	before_action :set_payment, only: [:customer_payment_form, :receive_customer_payment]
	before_action :set_order

	def customer_payment_form
	end

	def receive_customer_payment
		if @next_receivable_payment.update_attributes(customers_payment_params)
			@next_receivable_payment.paid!
			redirect_back(fallback_location: "/", notice: "Payment received successfully.")
		end
	end

	def transporter_payment_form
		@next_payable_payment = TransportersPayment.new
	end

	def pay_transporter_payment
		@next_payable_payment = @order.quote.transporters_payments.new(transporter_payment_params.merge(transporter_id: @order.transporter.id))
		if @next_payable_payment.save
			@next_payable_payment.paid!
			redirect_back(fallback_location: "/", notice: "Payment done successfully.")
		end
	end

	private

	def set_payment
		@next_receivable_payment = CustomersPayment.find(params[:id])
	end

	def set_order
		@order = Order.find(params[:order_id])
	end

	def customers_payment_params
		params.require(:customers_payment).permit(:pay_via, :ref_no)
	end

	def transporter_payment_params
		params.require(:transporters_payment).permit(:quote_id, :amount, :paying_date, :pay_via, :ref_no, :transporter_id)
	end

end
