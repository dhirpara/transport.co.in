class CustomersController < ApplicationController
  before_action :authenticate_customer!, except: [:send_otp, :otp_authentication, :verify_phone, :resend_send_otp]
  before_action :set_customer, except: [:dashboard, :initialize_load, :web_notifications]
  before_action :check_otp_authentication, only: [:send_otp, :otp_authentication, :verify_phone, :resend_send_otp]
  # include OtpService

  def dashboard
  end

  def show
		@bank_details = current_customer.bank_account_details
		if request.path != customer_path(current_customer)
      return redirect_to current_customer, :status => :moved_permanently
    end
    @resource_name = "customer"
  end

  def change_password
  end

  def update_password
    if @customer.update_with_password(customer_params)
      bypass_sign_in @customer
      redirect_to authenticated_customer_root_path, notice: "Successfully updated password"
    else
    	flash.now[:alert] = @customer.errors.full_messages.join(", ")
      render "change_password"
    end
  end

  def send_otp
    otp_res = generate_otp
    if otp_res.success?
      message = "Your OTP is #{otp_res.data}"
      SmsService::SMS.send(@customer.ph_no, message)
        .on_success{
          @customer.set_otp(otp_res.data)
          flash[:notice] = t("otp_service.send_otp")
        }
        .on_error{
          flash[:notice] = "SMS server problem."
        }
    else
      flash[:alert] = t("otp_service.errors.generate")
    end
    redirect_to otp_authentication_customer_path
  end

  def otp_authentication
  end

  def verify_phone
    if @customer.authenticate_otp(params[:otp_code])
      @customer.phone_verified!
      redirect_to new_customer_session_path, notice: t("authenticaction.otp.verified")
    else
      flash.now[:alert] = "OTP is wrong. Re-enter or regenerate again."
      render 'otp_authentication'
    end
  end

  def web_notifications
    @web_notifications = current_customer.web_notifications.sort_by(&:created_at).reverse
  end

  private

  def customer_params
  	params.require(:customer).permit(:current_password, :password, :password_confirmation)
  end

  def set_customer
  	@customer = Customer.friendly.find(params[:id])
  end

  def check_otp_authentication
    if @customer.ph_no_verified?
      redirect_to new_customer_session_path, notice: t("authenticaction.otp.allready_verified")
    end
  end

  def generate_otp
    OtpService::Otp.new(unique_for: :customer, unique_attr: :otp_secret_code).generate
  end
end

