# require 'active_support/concern'
module Devisable
	extend ActiveSupport::Concern
	include Devise::Models

	included do
		# Include default devise modules. Others available are:
  	# :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :authentication_keys => [:login]


  	attr_accessor :login
  	validates :ph_no, presence: true
  	# validates_uniqueness_of :ph_no, :email, allow_blank: true

  	# Class methods
  	def self.find_for_database_authentication(warden_conditions)
	    conditions = warden_conditions.dup
	    if login = conditions.delete(:login)
	      where(conditions.to_h).where(["ph_no = :value OR lower(email) = :value", { :value => login.downcase }]).first
	    elsif conditions.has_key?(:ph_no) || conditions.has_key?(:email)
	      conditions[:email].downcase! if conditions[:email]
	      where(conditions.to_h).first
	    end
	  end

	  def active_for_authentication?
	    super && (approved? && ph_no_verified?)
	  end

	  def inactive_message
	    if !ph_no_verified?
	      :ph_no_not_verified
	    elsif !approved?
	      :not_approved
	    else
	      super
	    end
	  end

	  def email_required?
	    false
	  end

	  def update_with_password(params, *options)
	    current_password = params.delete(:current_password)

	    if params[:password].blank?
	      params.delete(:password)
	      params.delete(:password_confirmation) if params[:password_confirmation].blank?
	    end

	    result = if valid_password?(current_password)
	      update_attributes(params, *options)
	    else
	      self.assign_attributes(params, *options)
	      self.valid?
	      self.errors.add(:current_password, current_password.blank? ? :blank : :invalid)
	      false
	    end

	    clean_up_passwords
	    result
	  end
  end



end