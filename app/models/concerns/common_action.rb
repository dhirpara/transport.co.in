module CommonAction
	extend ActiveSupport::Concern

	included do
		acts_as_paranoid
	  extend FriendlyId
	  friendly_id :name, use: [:slugged, :history]
  	has_many :bank_account_details, as: :accountable, dependent: :destroy
    has_many :generated_notifications, as: :notifiable, class_name: "WebNotification"
    has_many :user_notifications, as: :notifiable_user
    has_many :web_notifications, through: :user_notifications

    #TODO : Set email notification
    after_commit -> { SendNotificationJob.perform_later(type: self.class.name, action: "create", notified_via: ["web", "email"], object: self) }, on: :create

  	def self.find_by_login(login)
	    find_by_email(login) || find_by_ph_no(login)
	  end

    def should_generate_new_friendly_id?
      !name? || name_changed?
    end
  end


  def approve_resource
  	self.approved = true
  	self.save
  end

  def phone_verified!
    self.ph_no_verified = true
    self.save
  end

  def authenticate_otp(otp)
    otp_secret_code == otp && Time.zone.now <= otp_sent_at + 5.days
  end

  def set_otp(otp)
    self.otp_secret_code = otp
    self.otp_sent_at = Time.zone.now
    self.save
  end

  def admin?
    false
  end
  [:super_admin?, :sub_admin?].each { |type| alias_method type,  :admin? }

end