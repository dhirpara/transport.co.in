class TransporterRouter < ApplicationRecord
  acts_as_paranoid
  belongs_to :source_city,    class_name: "City"
  belongs_to :destination_city, class_name: "City"
  belongs_to :transporter

  validates :source_city_id, :destination_city_id, :transporter_id, presence: true
end
