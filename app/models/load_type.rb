class LoadType < ApplicationRecord
	extend FriendlyId
	friendly_id :name, use: :slugged

	has_many :load

	validates :name, uniqueness: true,presence: true
	acts_as_paranoid

	scope :active, -> { where(display_active: true) }

	def should_generate_new_friendly_id?
  	name_changed?
	end
end