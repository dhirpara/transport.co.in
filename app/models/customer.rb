class Customer < ApplicationRecord
  include Devisable
  include CommonAction

  has_many :loads, dependent: :destroy
  has_many :customers_payments, dependent: :destroy
  scope :find_by_account_status, -> (status = 'false') { ['true', 'false'].include?(status) ? where(:approved => ActiveRecord::Type::Boolean.new.cast(status)) :  nil  }
  accepts_nested_attributes_for :bank_account_details,:allow_destroy => true
  validates_associated :bank_account_details
  EMAIL_REGEX = /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/
  validates :email, uniqueness: true,:format => EMAIL_REGEX,:if => :email?
  validates :ph_no, uniqueness: true ,presence: true
  validates :password,:password_confirmation,presence:true,:on => :create

  def self.approved
    where(approved: true)
  end

  def accessible_loads(status)
    loads.includes(:load_type, :vehicle_load_type, :vehicle_type, :submitted_quotes, :accepted_quote, :confirmed_order).where(load_status: send("#{status}_loads"))
  end

  private

  def posted_loads
    [:waiting_for_approval, :posted]
  end

  def active_loads
    [:confirmed, :in_transit]
  end

  def processed_loads
    [:canceled, :completed]
  end

  def autenticate_methods
    [:posted_loads, :active_loads, :processed_loads ]
  end

  def method_missing(method_name, *args, &block)
    if autenticate_methods.exclude?(method_name.to_sym)
      send(autenticate_methods[0], *args, &block)
    else
      super
    end
  end

end
