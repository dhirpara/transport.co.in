class Quote < ApplicationRecord
  acts_as_paranoid
  extend FriendlyId
  friendly_id :transporter_name, use: [:slugged, :history]
  enum status: { drafted: 0, submitted: 1, withdrawed: 2, accepted: 3 }
  enum quotes_by: { admin: 0, customer: 1 }

  belongs_to :transporter
  belongs_to :load
  belongs_to :vehicle_load_type, foreign_key: "offered_vehicle_load_type_id"
  belongs_to :vehicle_type, foreign_key: "offered_vehicle_type_id"
  has_many :customers_payments, dependent: :destroy
  has_many :transporters_payments, dependent: :destroy
  has_one :admin_next_receivable_payment, -> { where('customers_payments.status = ?', 0) }, class_name: 'CustomersPayment'
  has_one :order, dependent: :destroy
  has_many :generated_notifications, as: :notifiable, class_name: "WebNotification"

  # after_commit -> { SendNotificationJob.perform_later(type: self.class.name, action: "create", notified_via: ["web"], object: self) },
  #   if: -> {exit if !status.nil?}#if: -> { status_previously_changed? && (load_status_previous_change <=> ["drafted", "submitted"]) == 0 }
  after_commit -> {
    puts "#{status_previous_change} : #{status_previously_changed?}"
    SendNotificationJob.perform_later(type: self.class.name, action: "create", notified_via: ["web", "email", "sms"], object: self) if status_previously_changed? && ((status_previous_change <=> [nil, "submitted"]) == 0 || (status_previous_change <=> ["drafted", "submitted"]) == 0)
    SendNotificationJob.perform_later(type: self.class.name, action: "accepted", notified_via: ["web", "email", "sms"], object: self) if status_previously_changed? && (status_previous_change <=> ["submitted", "accepted"]) == 0
    set_quote_info if submitted?
  }

  after_commit -> {
    SendNotificationJob.perform_later(type: self.class.name, action: "edited", notified_via: ["web", "email", "sms"], object: self) if !status_previously_changed? && submitted? && (!deleted? || !destroyed?) && previous_changes.except("slug", "updated_at", "quotes_on", "quotes_by").present?
  }, on: :update

  attr_accessor :submit_status

  validates :offered_date,:offered_vehicle_load_type_id,:offered_vehicle_type_id,:offered_vehicle_quantity,:offered_price,presence:true

  def self.quotes_by_status(status)
    statuses.keys.include?(status) ? where(status: status) : where(status: statuses.keys)
  end

  def transporter_name
  	self.transporter.name
  end

  def should_generate_new_friendly_id?
    !self.transporter.name? || self.transporter.name?
  end

  # def set_status(status)
  #   self.send("#{set_status}!")
  #   # TODO : Set SMS, email notification : Quote : create
  # end

  def set_quote_info
    if quotes_on.nil?
      self.quotes_on = Time.zone.now
      self.customer!
    end
  end

  def remain_receive_payments
    customers_payments.pending
  end

  def remain_paid_payments
    # customers_payments.pending
  end

  def receive_payments
    customers_payments.paid.map(&:amount).sum
  end

  def paid_payments
    transporters_payments.paid.map(&:amount).sum
  end

  def admin_next_payable_payment
    offered_price - transporters_payments.where(status: [:paid]).sum(:amount)
  end

end
