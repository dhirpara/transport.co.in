class TripTracking < ApplicationRecord
  attribute :coordinate, :point
  belongs_to :trip
end
