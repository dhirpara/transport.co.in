class PaymentPlan < ApplicationRecord
	store_accessor :payment_fees,:extra_fee_field,:extra_fee_value
	validates :pay_within_days, :numericality => {:only_integer => true},presence: true
	# validates :extra_fee_value,:numericality => {:only_integer => true}
	enum payment_type: { 'Full Payment': 1, 'Part Payment': 2 }

	has_many :customers_payments

	def self.active
		where(display_active: true)
	end
end
