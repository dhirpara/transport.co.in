class Trip < ApplicationRecord
	enum status: { yet_not_start: 0, running: 1, reached: 2 }

	belongs_to :load
	belongs_to :vehicle
	has_many :trip_trackings, dependent: :destroy

	# validates_uniqueness_of :load_id, scope: [:vehicle_id]

	def avg_speed
		speed_array = self.trip_trackings.map(&:speed)
		speed_array.present? ? speed_array.inject(:+)/speed_array.count : 0
	end

	def duration
		start_date = self.load.confirmed_order.dispatched_at
		last_date = self.trip_trackings.sort_by(&:id).map(&:location_time_stamp).last || Time.zone.now
		seconds = (last_date - start_date).to_i
		minutes = seconds / 60
		hours = minutes / 60
		hour = hours % 24
		days = hours / 24
		"#{days.to_s} #{'day'.pluralize(days)} #{hour.to_s} #{'hour'.pluralize(hour)}"
	end

end
