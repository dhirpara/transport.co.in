class VehicleType < ApplicationRecord
	extend FriendlyId
	friendly_id :name, use: :slugged

	has_many :load
	has_many :quote
	has_many :vehicles

	validates :name,presence: true, uniqueness: true
	acts_as_paranoid

	def should_generate_new_friendly_id?
  	name_changed?
	end
end