class RouteboxerHistory < ApplicationRecord
  attribute :load_origin_point, :point
  attribute :load_destination_point, :point
  attribute :matched_origin_point, :point
  attribute :matched_destination_point, :point
  belongs_to :transporter
end
