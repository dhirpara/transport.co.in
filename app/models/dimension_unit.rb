class DimensionUnit < ApplicationRecord
	extend FriendlyId
	acts_as_paranoid
	friendly_id :name, use: :slugged

	validates :name, uniqueness: true,presence: true

	def should_generate_new_friendly_id?
  	name_changed?
	end
end
