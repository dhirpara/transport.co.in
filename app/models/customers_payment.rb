class CustomersPayment < ApplicationRecord
	enum status: { due: 0, overdue: 1, paid: 2 }
  belongs_to :quote
  belongs_to :payment_plan
  belongs_to :customer
  before_create :set_status

  scope :pending, -> { where(status: [:due, :overdue]) }

  validates :quote_id, :payment_plan_id,:amount, :paying_date, presence: true

  def set_status
  	self.status = :due
  end

end
