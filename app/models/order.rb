class Order < ApplicationRecord
  enum status: { confirmed: 0, canceled: 1, in_transit: 2, completed: 3 }
  enum canceled_by: { admin: 0, transporter: 1, customer: 2 }
  ACTION_AS = ["transporter", "customer"]

  belongs_to :load
  belongs_to :quote
  belongs_to :transporter

  before_create :set_status
  before_save :set_status_wise_date

  validates :load_id, :quote_id, :transporter_id, presence: true
  validates_uniqueness_of :load_id, scope: [:quote_id, :transporter_id], conditions: -> { where.not(status: :canceled) }

  def self.get_active_by_status(status)
    status.nil? ? Order.where(status: [:confirmed, :in_transit]) : Order.where(status: status)
  end

  def self.get_processed_by_status(status)
    status.nil? ? Order.where(status: [:completed, :canceled]) : Order.where(status: status)
  end

  def cancel_order(as=nil, by=:transporter)
    return raise Exception.new("User type not found.") if as.present? && !ACTION_AS.include?(as)
    self.action_as =  as.nil? ? by : as
    self.canceled_by = by
    self.canceled!
    self.load.posted!
    self.load.accepted_quote.submitted!
  end

  def complete_order
    self.load.completed!
    self.completed!
  end

  def dispatch
    self.in_transit!
    self.load.in_transit!
    self.load.trip_vehicles << self.load.assign_vehicles
    self.load.trips.update_all(status: :running)
  end

  def reached
    self.completed!
    self.load.completed!
  end

  def active?
    self.confirmed? || self.in_transit?
  end

  def processed?
    self.completed? || self.canceled?
  end

  def remain_receive_payments
    quote.remain_receive_payments.map(&:amount).sum
  end

  def remain_paid_payments
    remaining_payable_amounts
  end

  def receive_payments
    quote.receive_payments
  end

  def paid_payments
    quote.paid_payments
  end

  def admin_next_receivable_payment
    quote.admin_next_receivable_payment
  end

  def admin_next_payable_payment
    remaining_payable_amounts
  end

  private

  def set_status
  	self.status = :confirmed
  end

  def set_status_wise_date
  	case self.status.try(:to_sym)
  	when :in_transit
			self.dispatched_at = Time.zone.now
  	when :completed
			self.completed_at = Time.zone.now
		when :canceled
      self.canceled_at = Time.zone.now
    end
  end

  def remaining_payable_amounts
    quote.offered_price - quote.transporters_payments.where(status: [:paid]).sum(:amount)
  end
end
