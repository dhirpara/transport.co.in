class LoadVehicle < ApplicationRecord
  belongs_to :load
  belongs_to :vehicle

  validates_uniqueness_of :load_id, scope: :vehicle_id, message: "has already assigned vehicle."
end
