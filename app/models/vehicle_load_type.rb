class VehicleLoadType < ApplicationRecord
	extend FriendlyId
	friendly_id :name, use: :slugged

	has_many :loads
	has_many :quotes

	validates :name,presence: true, uniqueness: true
	acts_as_paranoid

	def should_generate_new_friendly_id?
  	name_changed?
	end
end
