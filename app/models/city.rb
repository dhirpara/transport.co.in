class City < ApplicationRecord
	extend FriendlyId
	friendly_id :name, use: :slugged
  attribute :coordinate, :point
	validates :name,:short_name, presence: true
	validates :name, uniqueness: true
	validates :coordinate, :presence => { :message => " not found" },:if => :name?
	before_save :upcase_city_short_name
	# has_many :source_cities,  :class_name => 'TransporterRouter', :foreign_key => :source_city_id, dependent: :destroy
	# has_many :destination_cities, :class_name => 'TransporterRouter', :foregin_key => :destination_city_id, dependent: :destroy

	has_many :transporter_source_routes, class_name: "City", foreign_key: "source_city_id"
	has_many :transporter_destination_routes, class_name: "City", foreign_key: "destination_city_id"
  private
    def upcase_city_short_name
      self.short_name.upcase!
     end
end
