class WebNotification < ApplicationRecord
	belongs_to :notifiable, polymorphic: true
	# belongs_to :notifier, polymorphic: true
	has_many :user_notifications, dependent: :destroy
	has_many :admins, through: :user_notifications, source: :notifiable_user, source_type: "Admin"
	has_many :customers, through: :user_notifications, source: :notifiable_user, source_type: "Customer"
	has_many :transporters, through: :user_notifications, source: :notifiable_user, source_type: "Transporter"
end
