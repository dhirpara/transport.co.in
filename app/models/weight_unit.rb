class WeightUnit < ApplicationRecord
	extend FriendlyId
	acts_as_paranoid
	friendly_id :name, use: :slugged

	validates :name,:short_name,presence: true
	validates :name, uniqueness: true

	def should_generate_new_friendly_id?
  	name_changed?
	end
end
