class Inquiry < ApplicationRecord
	has_many :generated_notifications, as: :notifiable, class_name: "WebNotification"
	validates :email, :phone_number, :message, presence: true

	after_commit -> { SendNotificationJob.perform_later(type: self.class.name, action: "create", notified_via: ["web", "email"], object: self) }, on: :create
end
