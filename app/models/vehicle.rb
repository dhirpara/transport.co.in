class Vehicle < ApplicationRecord
  enum vehicle_status: { 'In Trip': 1, 'Free': 2 }
  enum fitness: { 'Valid': 1, 'Expired': 2 }

  belongs_to :transporter
  belongs_to :vehicle_type
  has_many :load_vehicles
  has_many :loads, through: :load_vehicles
  has_many :trips
  has_many :trip_load, through: :trips, source: "load"

  validates :number_plate, uniqueness: true, presence: true
  validates :chassis_number, uniqueness: true,presence: true

end
