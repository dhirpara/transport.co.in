class Transporter < ApplicationRecord
  include Devisable
  include CommonAction

  has_many :vehicles
  has_many :transporter_routers
  has_many :quotes, dependent: :destroy
  has_many :favorite_loads
  has_many :loads, through: :favorite_loads
  has_many :orders
  has_many :transporters_payments, dependent: :destroy
  has_many :routebox_histories

  EMAIL_REGEX = /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/

  validates :email, uniqueness: true,:format => EMAIL_REGEX,:if => :email?
  validates :ph_no, uniqueness: true,presence: true
  validates :password,:password_confirmation,presence:true, on: :create

  accepts_nested_attributes_for :bank_account_details , :allow_destroy => true
  scope :find_by_account_status, -> (status = 'false') { ['true', 'false'].include?(status) ? where(:approved => ActiveRecord::Type::Boolean.new.cast(status)) :  nil  }

  def accessible_orders(status)
    self.orders.includes(:quote, load: [:load_type, :vehicle_load_type, :vehicle_type]).where(status: send("#{status}_orders"))
  end

  private

  def active_orders
    [:confirmed, :in_transit]
  end

  def processed_orders
    [:completed, :canceled]
  end

  def autenticate_methods
    [:active_orders, :processed_orders ]
  end

  def method_missing(method_name, *args, &block)
    if autenticate_methods.exclude?(method_name.to_sym)
      send(autenticate_methods[0], *args, &block)
    else
      super
    end
  end

end
