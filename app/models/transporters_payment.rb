class TransportersPayment < ApplicationRecord
	enum status: { due: 0, overdue: 1, paid: 2 }
  belongs_to :quote
  belongs_to :transporter
end
