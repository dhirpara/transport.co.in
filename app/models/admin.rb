class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :registerable, :recoverable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :rememberable, :trackable, :validatable
  # has_many :web_notifications, as: :notifier
  has_many :user_notifications, as: :notifiable_user
  has_many :web_notifications, through: :user_notifications

  def admin?
  	is_a?(Admin)
  end

  def super_admin?
  	instance_of?(Admin)
  end

  def sub_admin?
  	instance_of?(SubAdmin)
  end

end
