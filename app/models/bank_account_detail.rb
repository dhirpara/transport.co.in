class BankAccountDetail < ApplicationRecord
  acts_as_paranoid
	extend FriendlyId

  friendly_id :account_number, use: [:slugged, :history]
  belongs_to :accountable, polymorphic: true

  validates :account_holder_name,:account_number,:bank_name,:ifsc_code,:branch_name,presence: true

  def should_generate_new_friendly_id?
    account_number? || super
  end
end
