class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
      # user ||= Customer.new # guest user (not logged in)
      if user.kind_of?(Customer)
        # can :manage, Load
        can :edit, Load, load_status: 2
        can :cancel, Load, load_status: [1, 2, 4]
        can :track, Load, load_status: [5, 6]
        can :manage, Load, load_status: [2]
      elsif user.kind_of?(Transporter)
        can :favorite, Load, load_status: 2
        can :cancel, Order, status: [0, 2]
      elsif user.kind_of?(Admin)
        can :manage, :all
      end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
