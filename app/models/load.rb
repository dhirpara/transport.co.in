class Load < ApplicationRecord
  acts_as_paranoid
  extend FriendlyId
  friendly_id :load_docket_id, use: [:slugged, :history]
  enum step_status: { load_detail: 0, vehicle_detail: 1, other_detail: 2, confirm_detail: 3, finish: 4 }
  enum load_status: { drafted: 0, waiting_for_approval: 1, posted: 2, canceled: 3, confirmed: 4, in_transit: 5, completed: 6 }
  enum load_canceled_by: { admin: 0, customer: 1 }
  attribute :from_coordinate, :point
  attribute :to_coordinate, :point

  belongs_to :customer
  belongs_to :load_type
  belongs_to :vehicle_load_type
  belongs_to :vehicle_type
  has_many :quotes, dependent: :destroy
  has_many :submitted_quotes, -> { where('quotes.status = ?', 1) }, class_name: 'Quote'
  has_many :favorite_loads, dependent: :destroy
  has_many :transporters, through: :favorite_loads
  has_one :accepted_quote, -> { where('quotes.status = ?', 3) }, class_name: 'Quote'
  has_many :load_vehicles, dependent: :destroy
  has_many :assign_vehicles, through: :load_vehicles, source: "vehicle"
  has_many :trips, dependent: :destroy
  has_many :trip_vehicles, through: :trips, source: "vehicle"
  has_many :orders, dependent: :destroy
  has_one :confirmed_order, -> { where.not('orders.status = ?', 1) }, class_name: 'Order'
  has_many :generated_notifications, as: :notifiable, class_name: "WebNotification"

  before_create :set_load_attr
  # after_update :send_to_admin, if: -> { load_posted_by_customer? }

  after_commit -> {
    SendNotificationJob.perform_later(type: self.class.name, action: "create", notified_via: ["web", "email"], object: self)
  }, on: :update, if: -> { load_status_previously_changed? && (load_status_previous_change <=> ["drafted", "waiting_for_approval"]) == 0 }

  # TODO : Set all notification for transporter : Load : posted : by admin
  after_commit -> {
    SendNotificationJob.perform_later(type: self.class.name, action: "posted", notified_via: ["web", "email", "sms"], object: self)
  }, on: :update, if: -> { load_status_previously_changed? && (load_status_previous_change <=> ["drafted", "posted"]) == 0 }

  after_commit -> { SendNotificationJob.perform_later(type: self.class.name, action: "posted", notified_via: ["web", "email", "sms"], object: self)
    }, on: :update, if: -> { load_status_previously_changed? && (load_status_previous_change <=> ["waiting_for_approval", "posted"]) == 0 }

  # TODO : Set all notification for transporter : Load : edited(verified)
  after_commit -> { SendNotificationJob.perform_later(type: self.class.name, action: "edited", notified_via: ["web", "email", "sms"], object: self)
    }, on: :update, if: -> { (load_status_previous_change <=> ["posted", "waiting_for_approval"]) == 0 && (step_status_previous_change <=> ["confirm_detail", "finish"]) == 0 }

  # TODO : Set all notification for transporter : Load : edited(verified) by admin
  after_commit -> { SendNotificationJob.perform_later(type: self.class.name, action: "edited", notified_via: ["web", "email", "sms"], object: self)
  }, on: :update, if: -> { !load_status_previously_changed? && posted? && (!deleted? || !destroyed?) && !form_step.present? && previous_changes.except("slug", "updated_at", "posted_on", "posted_by", "load_canceled_at", "load_canceled_by", "step_status").present? }

  after_commit -> { SendNotificationJob.perform_later(type: self.class.name, action: "canceled", notified_via: ["web", "email", "sms"], object: self)
    }, on: :update, if: -> { (load_status_previous_change <=> ["posted", "canceled"]) == 0 }

  # scope :find_by_load_status, -> (load_status) {['drafted'].include?(load_status) ? where("load_status=?",0) : where.not("load_status=?",0) }
  attr_accessor :form_step
  cattr_accessor :form_steps do
    %w(load_detail vehicle_detail other_detail confirm_detail)
  end

  validates :customer_id, presence: true
  DIMENSION_FIELD = [:length, :height, :dept, :unit]
  with_options if: -> { required_for_step?(:load_detail) } do |step|
  	step.validates :load_type_id, presence: true
  	step.validates :weight, hash: { present: [:value, :unit], numeric: [:value] }
    step.validates :volume, hash: { present: [:value, :unit], numeric: [:value] }, if: -> { volume["value"].present? }
  	step.validates :dimension, hash: { present: DIMENSION_FIELD, numeric: DIMENSION_FIELD.reject {|f| f == :unit } }, if: -> { dimension.except("unit").values.any? {|v| v.present?  } }
  end
  with_options if: -> { required_for_step?(:vehicle_detail) } do |step|
	  step.validates :vehicle_load_type_id, :vehicle_type_id, presence: true
	  step.validates :vehicle_quantity, numericality: { only_integer: true }
	  step.validates :loading_date, presence: true
	end
	with_options if: -> { required_for_step?(:other_detail) } do |step|
  	step.validates :terms_of_service, acceptance: true
  	step.validates :from_address, :to_address, presence: true
	end


  # validates :weight, :inclusion  => { :in => lambda { |a| a.class.billing_address_types.values } }
  # validates :weight, :inclusion  => { :in => lambda { |a| [WeightUnit.pluck(:id)] } }

  def should_generate_new_friendly_id?
    !load_docket_id? || load_docket_id_changed?
  end

  # Display if approved by admin
  def commentable
  	display_comment? ? comment : nil
  end

  # Set default attr to build load
  def set_load_attr
    self.step_status = :load_detail
    self.load_status = :drafted
    self.display_comment = false
  end

  # Fire step wise validation
  def required_for_step?(step)
	  return true if form_step.nil? || self.form_steps.index(step.to_s) <= self.form_steps.index(form_step.to_s)
	end

	# # Send load to admin to validate load details manully
	# def send_to_admin
	# 	waiting_for_approval!
 #    SendNotificationJob.perform_later("common", type: self.class.name, action: "create", notified_via: ["web"], object: self)
	# end

	# # Submit last step of load form
	# def load_posted_by_customer?
	# 	form_step == :confirm_detail && drafted?
	# end

  def finish_process
    # self.load_docket_id = generate_docket_id(self) if self.load_docket_id.nil?
    # self.step_status = :finish
    post_load
    waiting_for_approval!
  end

  def canceled_by(object = :admin)
    self.load_canceled_at = Time.zone.now
    self.load_canceled_by = object # canceled by
    self.canceled!
    # self.try(:confirmed_order).try(:canceled!)
  rescue
  end

  def approved
    post_load
    posted!
  end

  # post load if created/approved by admin
  def post_load(object = :customer)
    self.load_docket_id = generate_docket_id(self) if self.load_docket_id.nil?
    self.step_status = :finish
    self.display_comment = true if object == :admin
    self.posted_on = Time.zone.now if posted_on.nil?
    self.posted_by = object if posted_by.nil?
    self.posted! if object == :admin
  end

  def accepted?
    self.confirmed_order.present?
  end

  def paid?
    self.confirmed? && self.accepted_quote.customers_payments.present?
  end

  def create_order_with(quote)
    self.confirmed!
    quote.accepted!
    quote.transporter.orders.create!(load: self, quote: quote)
  end

  # def reached
  #   self.completed!
  #   self.confirmed_order.completed!
  # end

  def active?
    self.confirmed? || self.in_transit?
  end

  def processed?
    self.canceled? || self.completed?
  end

  def self.accessible_loads(status)
    includes(:customer, :load_type, :vehicle_type).where(load_status: send("#{status}_loads"))
  end

  private

  def generate_docket_id(load)
    alpha_array = [('A'..'Z')].map { |i| i.to_a }.flatten
    number_array = [('0'..'9')].map { |i| i.to_a }.flatten
    (0...5).map { alpha_array[rand(alpha_array.length)] }.join +
    (0...6).map { number_array[rand(number_array.length)] }.join
  end

  def self.drafted_loads
    [:drafted]
  end

  def self.posted_loads
    [:posted]
  end

  def self.waiting_for_approval_loads
    [:waiting_for_approval]
  end

  def self.active_loads
    [:confirmed, :in_transit]
  end

  def self.processed_loads
    [:canceled, :completed]
  end

  def self.autenticate_methods
    [:drafted_loads,:posted_loads, :waiting_for_approval_loads,:active_loads, :processed_loads ]
  end

  def self.method_missing(method_name, *args, &block)
    if autenticate_methods.exclude?(method_name.to_sym)
      send(autenticate_methods[2], *args, &block)
    else
      super
    end
  end

end
