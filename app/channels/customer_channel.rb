# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class CustomerChannel < ApplicationCable::Channel
  def subscribed
  	stop_all_streams
  	# stream_from "customer" if current_user.is_a?(Customer)
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
