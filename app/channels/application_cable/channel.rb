module ApplicationCable
  class Channel < ActionCable::Channel::Base

	  private

	  def create_notification
	  	WebNotification.create(title: "New Load added", body: "Load id is #{params[:loadId]}")
	  end
  end
end
