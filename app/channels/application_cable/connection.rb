module ApplicationCable
  class Connection < ActionCable::Connection::Base
  	identified_by :current_user

    def connect
      self.current_user = find_verified_user
      logger.add_tags 'ActionCable', current_user.email
    end

    protected
      def find_verified_user
      	scope = cookies.signed['scope']
        verified_user = scope.to_s.classify.constantize.find_by(id: cookies.signed['user.id']) if scope
        if verified_user && cookies.signed['user.expires_at'] > Time.now
          verified_user
        else
          reject_unauthorized_connection
        end
      end
  end
end
