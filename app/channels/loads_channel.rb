# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class LoadsChannel < ApplicationCable::Channel
  def subscribed
    stop_all_streams
  	# stream_from "loads"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def create(params)
  	create_notification()
  	WebNotificationBroadcastJob.perform_later("loads", action: "create", data: render_notification)
  end

  private

  def render_notification
  	ApplicationController.render(partial: 'shared/web_notification_bedge' )
  end
end
