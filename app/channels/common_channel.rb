# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class CommonChannel < ApplicationCable::Channel
  def subscribed
  	stop_all_streams
  	# stream_from "common"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

	# TODO : Notify matching transporter : Load : posted
  def notify_transporter(transporter_list)
  	transporters = TransporterRouter.includes(:transporter).find(params[:transporter_list]).map{|tr| tr.transporter}.uniq
    transporters.each{ |t| puts">>>>>>>Send notification to "+t.ph_no }
    SendNotificationJob.perform_later("common", type: self.class.name, action: "posted", notified_via: ["web"], object: self)
  end
end
