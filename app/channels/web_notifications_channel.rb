# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class WebNotificationsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "web_notification_admin_#{current_user.id}" if current_user.admin?
    stream_from "web_notification_customer_#{current_user.id}" if current_user.is_a?(Customer)
    stream_from "web_notification_transporter_#{current_user.id}" if current_user.is_a?(Transporter)
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
