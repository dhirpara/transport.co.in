# Preview all emails at http://localhost:3000/rails/mailers/admin_mailer
class AdminMailerPreview < ActionMailer::Preview

	def create_inquiry
		AdminMailer.create_inquiry(notification: WebNotification.first)
		# @notification = WebNotification.first
		# mail(to: "info@transport.co.in", subject: args[:notification].title)
	end

	def create_customer
		AdminMailer.create_customer(notification: WebNotification.find(367))
	end

	def create_transporter
		AdminMailer.create_transporter(notification: WebNotification.find(368))
	end

	def create_load
		AdminMailer.create_load(notification: WebNotification.find(369))
	end

	def posted_load
		AdminMailer.posted_load(notification: WebNotification.find(370))
	end

	def edited_load
		AdminMailer.edited_load(notification: WebNotification.find(375))
	end

end
