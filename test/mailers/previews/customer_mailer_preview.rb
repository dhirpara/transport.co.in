# Preview all emails at http://localhost:3000/rails/mailers/customer_mailer
class CustomerMailerPreview < ActionMailer::Preview

	def posted_load
		CustomerMailer.posted_load(notification: WebNotification.find(374), user: Customer.first)
	end

	def edited_load
		CustomerMailer.edited_load(notification: WebNotification.find(376), user: Customer.first)
	end

end
