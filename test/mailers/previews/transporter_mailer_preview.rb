# Preview all emails at http://localhost:3000/rails/mailers/transporter_mailer
class TransporterMailerPreview < ActionMailer::Preview

	def posted_load
		TransporterMailer.posted_load(notification: WebNotification.find(370), user: Transporter.first)
	end

	def edited_load
		TransporterMailer.edited_load(notification: WebNotification.find(375), user: Transporter.first)
	end

	def accepted_quote_assoc
		TransporterMailer.accepted_quote_assoc("Testing", {object: Quote.last, user: Transporter.first})
	end

end
