require 'test_helper'

class Admin::PaymentPlansControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get admin_payment_plans_new_url
    assert_response :success
  end

  test "should get create" do
    get admin_payment_plans_create_url
    assert_response :success
  end

end
