require 'test_helper'

class Customer::CustomersControllerTest < ActionDispatch::IntegrationTest
  test "should get dashboard" do
    get customer_customers_dashboard_url
    assert_response :success
  end

end
